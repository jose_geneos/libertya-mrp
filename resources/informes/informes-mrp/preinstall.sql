----------------------------------------------------------------------
---------- Nuevas Tablas y/o Vistas 
----------------------------------------------------------------------

CREATE VIEW RV_PP_COST_COLLECTOR_MATERIAL AS
SELECT om.documentno AS order_documentno, cc.m_product_id, cc.ad_orgtrx_id, cc.ad_org_id, cc.ad_user_id, cc.c_activity_id, cc.c_campaign_id, cc.c_doctypetarget_id, cc.c_doctype_id, cc.c_project_id, cc.c_uom_id, cc.created, cc.createdby, cc.dateacct, cc.description, cc.docaction, cc.docstatus, cc.durationreal, cc.isactive, cc.isbatchtime, cc.m_attributesetinstance_id, cc.m_locator_id, cc.m_warehouse_id, cc.movementdate, cc.movementqty, cc.pp_cost_collector_id, cc.pp_order_bomline_id, cc.pp_order_id, cc.pp_order_node_id, cc.pp_order_workflow_id, cc.posted, cc.processed, cc.processing, cc.qtyreject, cc.s_resource_id, cc.scrappedqty, cc.setuptimereal, cc.updated, cc.updatedby, cc.user1_id, cc.ad_client_id, cc.user2_id, cc.reversal_id, cc.costcollectortype, cc.issubcontracting, cc.documentno, cc.processedon, p.m_product_category_id, om.ad_workflow_id FROM ((pp_cost_collector cc JOIN pp_order om ON ((om.pp_order_id = cc.pp_order_id))) JOIN m_product p ON ((p.m_product_id = cc.m_product_id))) WHERE (((cc.costcollectortype)::text = ANY ((ARRAY['95'::character varying, '100'::character varying])::text[])) AND (om.docstatus = 'CO'::bpchar));

CREATE VIEW RV_PP_COST_COLLECTOR_COPRODUCT AS
SELECT om.documentno AS order_documentno, cc.m_product_id, cc.ad_orgtrx_id, cc.ad_org_id, cc.ad_user_id, cc.c_activity_id, cc.c_campaign_id, cc.c_doctypetarget_id, cc.c_doctype_id, cc.c_project_id, cc.c_uom_id, cc.created, cc.createdby, cc.dateacct, cc.description, cc.docaction, cc.docstatus, cc.durationreal, cc.isactive, cc.isbatchtime, cc.m_attributesetinstance_id, cc.m_locator_id, cc.m_warehouse_id, cc.movementdate, cc.movementqty, cc.pp_cost_collector_id, cc.pp_order_bomline_id, cc.pp_order_id, cc.pp_order_node_id, cc.pp_order_workflow_id, cc.posted, cc.processed, cc.processing, cc.qtyreject, cc.s_resource_id, cc.scrappedqty, cc.setuptimereal, cc.updated, cc.updatedby, cc.user1_id, cc.ad_client_id, cc.user2_id, cc.reversal_id, cc.costcollectortype, cc.issubcontracting, cc.documentno, cc.processedon, p.m_product_category_id, om.ad_workflow_id FROM ((pp_cost_collector cc JOIN pp_order om ON ((om.pp_order_id = cc.pp_order_id))) JOIN m_product p ON ((p.m_product_id = cc.m_product_id))) WHERE (((cc.costcollectortype)::text = ANY ((ARRAY['85'::character varying, '105'::character varying])::text[])) AND (om.docstatus = 'CO'::bpchar));

----------------------------------------------------------------------
---------- Nuevas columnas en tablas y/o vistas 
----------------------------------------------------------------------

----------------------------------------------------------------------
---------- Modificación de tablas y/o vistas
----------------------------------------------------------------------

