----------------------------------------------------------------------
---------- Nuevas Tablas y/o Vistas 
----------------------------------------------------------------------

----------------------------------------------------------------------
---------- Nuevas columnas en tablas y/o vistas 
----------------------------------------------------------------------

ALTER TABLE PP_Cost_Collector ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_MRP ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_BOM ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_BOMLine ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_Cost ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_Node ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_Node_Asset ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_NodeNext ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_Node_Product ADD COLUMN AD_ComponentObjectUID character varying(100);
ALTER TABLE PP_Order_Workflow ADD COLUMN AD_ComponentObjectUID character varying(100);
----------------------------------------------------------------------
---------- Modificación de tablas y/o vistas
----------------------------------------------------------------------

