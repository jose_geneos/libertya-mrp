----------------------------------------------------------------------
---------- Nuevas Tablas y/o Vistas 
----------------------------------------------------------------------

----------------------------------------------------------------------
---------- Nuevas columnas en tablas y/o vistas 
----------------------------------------------------------------------

ALTER TABLE M_Locator ADD COLUMN IsIssue character(1);
ALTER TABLE C_OrderLine ADD COLUMN M_RequisitionLine_ID integer;
ALTER TABLE ad_org ADD COLUMN reportLogo bytea;

----------------------------------------------------------------------
---------- Modificación de tablas y/o vistas
----------------------------------------------------------------------

-- Se genero vista:
CREATE OR REPLACE VIEW rv_pp_order_storage_isissue AS 
 SELECT obl.ad_client_id, obl.ad_org_id, obl.createdby, obl.updatedby, obl.updated, obl.created, obl.isactive, obl.pp_order_bom_id, obl.pp_order_bomline_id, obl.pp_order_id, obl.iscritical, obl.m_product_id, ( SELECT p.name
           FROM m_product p
          WHERE p.m_product_id = o.m_product_id) AS name, obl.c_uom_id, s.qtyonhand, round(obl.qtyrequired, 4) AS qtyrequired, 
        CASE
            WHEN o.qtybatchs = 0::numeric THEN 1::numeric
            ELSE round(obl.qtyrequired / o.qtybatchs, 4)
        END AS qtybatchsize, round(bomqtyreserved(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, 0::numeric), 4) AS qtyreserved, 
        COALESCE(( ( SELECT sum(round(bomqtyonhand(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, auxl.m_locator_id::numeric), 4) ) FROM m_locator auxl
          WHERE auxl.m_warehouse_id = obl.m_warehouse_id AND auxl.isissue = 'Y'::bpchar)
        - round(bomqtyreserved(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, 0::numeric), 4)
           ),0) AS qtyavailable, obl.m_warehouse_id, obl.qtybom, obl.isqtypercentage, round(obl.qtybatch, 4) AS qtybatch, obl.m_attributesetinstance_id, l.m_locator_id, l.x, l.y, l.z
   FROM pp_order_bomline obl
   JOIN pp_order o ON o.pp_order_id = obl.pp_order_id
   LEFT JOIN m_storage s ON s.m_product_id = obl.m_product_id AND s.qtyonhand <> 0::numeric AND obl.m_warehouse_id = (( SELECT ld.m_warehouse_id
   FROM m_locator ld
  WHERE s.m_locator_id = ld.m_locator_id))
   LEFT JOIN m_locator l ON l.m_locator_id = s.m_locator_id
  ORDER BY obl.m_product_id;

ALTER TABLE rv_pp_order_storage_isissue
  OWNER TO libertya;

--SE MODIFICO VISTA
CREATE OR REPLACE VIEW rv_pp_picking_list AS 
 SELECT a.ad_client_id, a.ad_org_id, a.createdby, a.updatedby, a.updated, a.created, a.pp_order_bom_id, a.pp_order_bomline_id, a.pp_order_id, a.iscritical, a.m_product_id, a.c_uom_id, a.qtyonhand, a.qtyrequired, a.qtybatchsize, a.qtyreserved, a.qtyavailable, a.m_warehouse_id, a.qtybom, a.isqtypercentage, a.qtybatch, a.m_attributesetinstance_id, a.m_locator_id, a.componenttype, a.pp_om_id, a.line, 
        CASE
            WHEN (a.qtyrequired - a.qtydelivered) >= 0::numeric THEN a.qtyrequired - a.qtydelivered
            ELSE 0::numeric
        END AS missingqty
   FROM (         SELECT obl.ad_client_id, obl.ad_org_id, obl.createdby, obl.updatedby, obl.updated, obl.created, obl.pp_order_bom_id, obl.pp_order_bomline_id, obl.pp_order_id, obl.iscritical, obl.m_product_id, obl.c_uom_id, s.qtyonhand, round(obl.qtyrequired, 4) AS qtyrequired, 
                        CASE
                            WHEN o.qtybatchs = 0::numeric THEN 1::numeric
                            ELSE round(obl.qtyrequired / o.qtybatchs, 4)
                        END AS qtybatchsize, round(bomqtyreserved(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, 0::numeric), 4) AS qtyreserved, COALESCE(( ( SELECT sum(round(bomqtyonhand(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, auxl.m_locator_id::numeric), 4) ) FROM m_locator auxl
          WHERE auxl.m_warehouse_id = obl.m_warehouse_id AND auxl.isissue = 'Y'::bpchar)
        - round(bomqtyreserved(obl.m_product_id::numeric, obl.m_warehouse_id::numeric, 0::numeric), 4)
           ),0) AS qtyavailable, obl.m_warehouse_id, obl.qtybom, obl.isqtypercentage, round(obl.qtybatch, 4) AS qtybatch, round(obl.qtydelivered, 4) AS qtydelivered, s.m_attributesetinstance_id, s.m_locator_id, obl.componenttype, obl.pp_order_id AS pp_om_id, obl.line
                   FROM pp_order_bomline obl
              JOIN pp_order o ON o.pp_order_id = obl.pp_order_id
         LEFT JOIN m_storage s ON s.m_product_id = obl.m_product_id AND s.qtyonhand > 0::numeric AND obl.m_warehouse_id = (( SELECT ld.m_warehouse_id
                 FROM m_locator ld
                WHERE s.m_locator_id = ld.m_locator_id and ld.isissue='Y'))
        WHERE obl.componenttype = ANY (ARRAY['CO'::bpchar, 'PH'::bpchar])
        UNION 
                 SELECT obl.ad_client_id, obl.ad_org_id, obl.createdby, obl.updatedby, obl.updated, obl.created, obl.pp_order_bom_id, obl.pp_order_bomline_id, obl.pp_order_id, obl.iscritical, obl.m_product_id, obl.c_uom_id, 0::numeric(22,4) AS "numeric", round(obl.qtyrequired, 4) AS qtyrequired, 0::numeric(22,4) AS qtybatchsize, 0 AS qtyreserved, 0::numeric(22,4) AS qtyavailable, obl.m_warehouse_id, obl.qtybom, obl.isqtypercentage, round(obl.qtybatch, 4) AS qtybatch, round(obl.qtydelivered, 4) AS qtydelivered, 0 AS int4, 0 AS int4, obl.componenttype, obl.pp_order_id AS pp_om_id, obl.line
                   FROM pp_order_bomline obl
              JOIN pp_order o ON o.pp_order_id = obl.pp_order_id
             WHERE obl.componenttype = 'CP'::bpchar) a
  ORDER BY a.pp_order_id, a.line;

ALTER TABLE rv_pp_picking_list
  OWNER TO libertya;

CREATE OR REPLACE FUNCTION qtyavailableforbomline(p_order_bomline_id integer, p_m_locator_id integer)
  RETURNS numeric AS
' DECLARE 		v_Warehouse_ID		INTEGER; 		v_Product_ID		INTEGER; 		v_QuantityReserved	NUMERIC := 0;
 BEGIN
  		IF (p_order_bomline_id IS NULL) 
		THEN
			RETURN 0; 		
		ELSE 			
			SELECT 	M_Warehouse_ID,M_Product_ID,qtyReserved INTO v_Warehouse_ID,v_Product_ID,v_QuantityReserved 			
			FROM	PP_Order_BOMLine
			 			WHERE	PP_Order_BOMLine_ID=p_order_bomline_id; 		
		END IF; 		 		 		
		IF (bomQtyOnHand(v_Product_ID, v_Warehouse_ID, p_m_locator_id) < bomQtyReserved(v_Product_ID, v_Warehouse_ID, 0) ) 
		THEN
			RETURN bomQtyOnHand(v_Product_ID, v_Warehouse_ID, p_m_locator_id); 
		ELSE 			
			RETURN bomQtyOnHand(v_Product_ID, v_Warehouse_ID, p_m_locator_id) - bomQtyReserved(v_Product_ID, v_Warehouse_ID, 0) + v_QuantityReserved;
		END IF; 	 
	END; '
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION qtyavailableforbomline(integer)
  OWNER TO libertya;

--Se modifico funcion
DROP FUNCTION bompricestd(integer,integer,integer);

CREATE OR REPLACE FUNCTION bompricestd(
    mm_product_id integer,
    mm_pricelist_version_id integer,
    mm_attributesetinstance_id integer)
  RETURNS numeric AS
$BODY$
DECLARE
    i_price NUMERIC;
BEGIN
    
    IF (mm_attributesetinstance_id = 0) THEN
	select bomPriceStd(mM_Product_ID,mM_PriceList_Version_ID) into i_price;
    else	
	SELECT pricestd into i_price FROM M_ProductPriceInstance pi WHERE pi.M_PriceList_Version_ID=mM_PriceList_Version_ID AND pi.M_Product_ID=mM_Product_ID AND pi.M_AttributeSetInstance_ID=mM_AttributeSetInstance_ID;
	IF (i_price ISNULL) THEN
		select bomPriceStd(mM_Product_ID,mM_PriceList_Version_ID) into i_price;
	END IF;	
    END IF;
    return i_price;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bompricestd(integer, integer, integer)
  OWNER TO libertya;

------------------------------------------------------
-- Optimizacion de Recepcion y Surtimiento
------------------------------------------------------
CREATE INDEX ppccollector_i ON libertya.PP_Cost_Collector(PP_Order_BOMLine_ID);
