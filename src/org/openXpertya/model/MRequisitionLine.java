package org.openXpertya.model;

/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2007 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *                 Teo Sarca, www.arhipac.ro								  * 	
 *                 José Fantasia jose.fantasia@geneos.com.ar		 	  	  *                                 *
 *****************************************************************************/

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.openXpertya.util.DB;
import org.openXpertya.util.Env;
import org.openXpertya.util.Msg;

import ar.com.geneos.mrp.plugin.model.LP_M_RequisitionLine;
import ar.com.geneos.mrp.plugin.model.MRPValidator;
import ar.com.geneos.mrp.plugin.model.ModelValidator;
import ar.com.geneos.mrp.plugin.util.MUColumnNames;

/**
 * Requisition Line Model
 *
 * @author Jorg Janke
 * @version $Id: MRequisitionLine.java,v 1.2 2006/07/30 00:51:03 jjanke Exp $
 * 
 * @author Teo Sarca, www.arhipac.ro <li>BF [ 2419978 ] Voiding PO, requisition
 *         don't set on NULL <li>BF [ 2608617 ] Error when I want to delete a PO
 *         document <li>BF [ 2609604 ] Add M_RequisitionLine.C_BPartner_ID <li>
 *         FR [ 2841841 ] Requisition Improvements
 *         https://sourceforge.net/tracker
 *         /?func=detail&aid=2841841&group_id=176962&atid=879335
 */
public class MRequisitionLine extends LP_M_RequisitionLine {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2567343619431184322L;
	
	private MProduct product; //Variable usada en el reporte de impresión
	private MBPartner partner;

	public static String COLUMNNAME_C_OrderLine_ID = "C_OrderLine_ID";

	/**
	 * Get corresponding Requisition Line for given Order Line
	 * 
	 * @param ctx
	 * @param C_OrderLine_ID
	 *            order line
	 * @param trxName
	 * @return Requisition Line
	 */
	public static MRequisitionLine[] forC_Order_ID(Properties ctx, int C_Order_ID, String trxName) {
		final String whereClause = "EXISTS (SELECT 1 FROM C_OrderLine ol" + " WHERE ol.C_OrderLine_ID=M_RequisitionLine.C_OrderLine_ID"
				+ " AND ol.C_Order_ID=?)";
		List<MRequisitionLine> list = new Query(ctx, X_M_RequisitionLine.Table_Name, whereClause, trxName).setParameters(C_Order_ID).list();
		return list.toArray(new MRequisitionLine[list.size()]);
	}

	/**
	 * UnLink Requisition Lines for given Order
	 * 
	 * @param ctx
	 * @param C_Order_ID
	 * @param trxName
	 */
	public static void unlinkC_Order_ID(Properties ctx, int C_Order_ID, String trxName) {
		for (MRequisitionLine line : MRequisitionLine.forC_Order_ID(ctx, C_Order_ID, trxName)) {
			line.setC_OrderLine_ID(0);
			line.save();
		}
	}

	/**
	 * Get corresponding Requisition Line(s) for given Order Line
	 * 
	 * @param ctx
	 * @param C_OrderLine_ID
	 *            order line
	 * @param trxName
	 * @return array of Requisition Line(s)
	 */
	public static MRequisitionLine[] forC_OrderLine_ID(Properties ctx, int C_OrderLine_ID, String trxName) {
		final String whereClause = MUColumnNames.COLUMNNAME_C_OrderLine_ID + "=?";
		List<MRequisitionLine> list = new Query(ctx, X_M_RequisitionLine.Table_Name, whereClause, trxName).setParameters(C_OrderLine_ID).list();
		return list.toArray(new MRequisitionLine[list.size()]);
	}

	/**
	 * UnLink Requisition Lines for given Order Line
	 * 
	 * @param ctx
	 * @param C_OrderLine_ID
	 * @param trxName
	 */
	public static boolean unlinkC_OrderLine_ID(Properties ctx, int C_OrderLine_ID, String trxName) {
		for (MRequisitionLine line : forC_OrderLine_ID(ctx, C_OrderLine_ID, trxName)) {
			line.setC_OrderLine_ID(0);
			if (!line.save())
				return false;
		}
		return true;
	}

	/**
	 * Standard Constructor
	 *
	 * @param ctx
	 *            context
	 * @param M_RequisitionLine_ID
	 *            id
	 * @param trxName
	 *            transaction
	 */
	public MRequisitionLine(Properties ctx, int M_RequisitionLine_ID, String trxName) {
		super(ctx, M_RequisitionLine_ID, trxName);
		if (M_RequisitionLine_ID == 0) {
			// setM_Requisition_ID (0);
			setLine(0); // @SQL=SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue
						// FROM M_RequisitionLine WHERE
						// M_Requisition_ID=@M_Requisition_ID@
			setLineNetAmt(Env.ZERO);
			setPriceActual(Env.ZERO);
			setQty(Env.ONE); // 1
		}

	} // MRequisitionLine

	/**
	 * Load Constructor
	 *
	 * @param ctx
	 *            context
	 * @param rs
	 *            result set
	 * @param trxName
	 *            transaction
	 */
	public MRequisitionLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	} // MRequisitionLine

	/**
	 * Parent Constructor
	 *
	 * @param req
	 *            requisition
	 */
	public MRequisitionLine(MRequisition req) {
		this(req.getCtx(), 0, req.get_TrxName());
		setClientOrg(req);
		setM_Requisition_ID(req.getM_Requisition_ID());
		m_M_PriceList_ID = req.getM_PriceList_ID();
		m_parent = req;
	} // MRequisitionLine

	/** Parent */
	private MRequisition m_parent = null;

	/** PriceList */
	private int m_M_PriceList_ID = 0;

	/**
	 * Get Ordered Qty
	 * 
	 * @return Ordered Qty
	 */
	public BigDecimal getQtyOrdered() {
		if (getC_OrderLine_ID() > 0)
			return getQty();
		else
			return Env.ZERO;
	}

	/**
	 * Get Parent
	 *
	 * @return parent
	 */
	public MRequisition getParent() {
		if (m_parent == null)
			m_parent = new MRequisition(getCtx(), getM_Requisition_ID(), get_TrxName());
		return m_parent;
	} // getParent

	public MRequisition getM_Requisition() {
		return getParent();
	}

	/**
	 * @return Date when this product is required by planner
	 * @see MRequisition#getDateRequired()
	 */
	public Timestamp getDateRequired() {
		return getParent().getDateRequired();
	}

	/**
	 * Set Price
	 */
	public void setPrice() {
		if (getC_Charge_ID() != 0) {
			MCharge charge = new MCharge(getCtx(), getC_Charge_ID(), get_TrxName());
			setPriceActual(charge.getChargeAmt());
		}
		if (getM_Product_ID() == 0)
			return;
		if (m_M_PriceList_ID == 0)
			m_M_PriceList_ID = getParent().getM_PriceList_ID();
		if (m_M_PriceList_ID == 0) {
			throw new RuntimeException("PriceList unknown!");
		}
		setPrice(m_M_PriceList_ID);
	} // setPrice

	/**
	 * Set Price for Product and PriceList
	 * 
	 * @param M_PriceList_ID
	 *            price list
	 */
	public void setPrice(int M_PriceList_ID) {
		if (getM_Product_ID() == 0)
			return;
		//
		log.fine("M_PriceList_ID=" + M_PriceList_ID);
		boolean isSOTrx = false;
		MProductPricing pp = new MProductPricing(getM_Product_ID(), getC_BPartner_ID(), getQty(), isSOTrx);
		pp.setM_PriceList_ID(M_PriceList_ID);
		// pp.setPriceDate(getDateOrdered());
		//
		setPriceActual(pp.getPriceStd());
	} // setPrice

	/**
	 * Calculate Line Net Amt
	 */
	public void setLineNetAmt() {
		BigDecimal lineNetAmt = getQty().multiply(getPriceActual());
		super.setLineNetAmt(lineNetAmt);
	} // setLineNetAmt

	/**************************************************************************
	 * Before Save
	 *
	 * @param newRecord
	 *            new
	 * @return true
	 */
	protected boolean beforeSave(boolean newRecord) {
		if (newRecord && getParent().isComplete()) {
			log.saveError("ParentComplete", Msg.translate(getCtx(), "M_RequisitionLine"));
			return false;
		}
		if (getLine() == 0) {
			String sql = "SELECT COALESCE(MAX(Line),0)+10 FROM M_RequisitionLine WHERE M_Requisition_ID=?";
			int ii = DB.getSQLValueEx(get_TrxName(), sql, getM_Requisition_ID());
			setLine(ii);
		}
		// Product & ASI - Charge
		if (getM_Product_ID() != 0 && getC_Charge_ID() != 0)
			setC_Charge_ID(0);
		if (getM_AttributeSetInstance_ID() != 0 && getC_Charge_ID() != 0)
			setM_AttributeSetInstance_ID(0);
		// Product UOM
		if (getM_Product_ID() > 0 && getC_UOM_ID() <= 0) {
			setC_UOM_ID(getM_Product().getC_UOM_ID());
		}
		//
		if (getPriceActual().signum() == 0)
			setPrice();
		setLineNetAmt();
		
		/*
		 * Si el producto de la linea no pertenece a la tarifa seleccionada
		 * en la requisicion, entonces se debe reportar el error. 
		 */
		if(this.getParent().getM_PriceList_ID() != 0) {
			List<Object> params = new ArrayList<Object>();
			final StringBuffer whereClause = new StringBuffer();
			whereClause.append("M_Product_ID = ?");
			params.add(getM_Product_ID());
			whereClause.append(" AND M_PriceList_Version_ID = (SELECT M_PriceList_Version_ID FROM M_PriceList_Version WHERE m_priceList_id = ?)");
			params.add(this.getParent().getM_PriceList_ID());
			Query q = new Query(this.getCtx(), MProductPrice.Table_Name, whereClause.toString(), this.get_TrxName());
			q.setParameters(params);
			List<MProductPrice> result = q.list();
			if(result == null || result.size() == 0) {
				log.saveError("", "El producto asignado no pertenece a la Tarifa selecciona en la cabecera del documento");
				return false;
			}
		}
		
		return true;
	} // beforeSave

	/**
	 * After Save. Update Total on Header
	 *
	 * @param newRecord
	 *            if new record
	 * @param success
	 *            save was success
	 * @return true if saved
	 */
	protected boolean afterSave(boolean newRecord, boolean success) {
		if (!success)
			return success;
		if (!updateHeader())
			return false;
		if (newRecord)
			MRPValidator.modelChange(this, ModelValidator.TYPE_AFTER_NEW, log);
		else
			MRPValidator.modelChange(this, ModelValidator.TYPE_AFTER_CHANGE, log);
		return true;
	} // afterSave

	/**
	 * After Delete
	 *
	 * @param success
	 * @return true/false
	 */
	protected boolean afterDelete(boolean success) {
		if (!success)
			return success;
		if (!updateHeader())
			return false;
		MRPValidator.modelChange(this, ModelValidator.TYPE_AFTER_DELETE, log);
		return true;
	} // afterDelete

	public MProduct getM_Product() {
		return MProduct.get(getCtx(), getM_Product_ID());
	}

	/**
	 * Update Header
	 *
	 * @return header updated
	 */
	private boolean updateHeader() {
		log.fine("");
		String sql = "UPDATE M_Requisition r" + " SET TotalLines=" + "(SELECT COALESCE(SUM(LineNetAmt),0) FROM M_RequisitionLine rl "
				+ "WHERE r.M_Requisition_ID=rl.M_Requisition_ID) " + "WHERE M_Requisition_ID=" + getM_Requisition_ID();
		int no = DB.executeUpdate(sql, get_TrxName());
		if (no != 1)
			log.log(Level.SEVERE, "Header update #" + no);
		m_parent = null;
		return no == 1;
	} // updateHeader
	
	
	/****************************************************************************************************************/
	/**************************** Metodos agregados para el reporte de impresion ************************************/
	/****************************************************************************************************************/
	private MProduct getProductAsociated() {
		if(this.product == null) {
			this.product = new MProduct(getCtx(), getM_Product_ID(), get_TrxName());
		}
		return this.product;
	}
	
	public String getProductCode() {
		return getProductAsociated().getValue();
	}
	
	public String getProductName() {
		return getProductAsociated().getName();
	}
	
	public String getProductCompleteName() {
		return getProductAsociated().getM_Product_ID()+"_"+this.product.getName();
	}
	
	private MBPartner getPartnerAsociated() {
		if(this.partner == null) {
			this.partner = new MBPartner(getCtx(), getC_BPartner_ID(), get_TrxName());
		}
		return this.partner;
	}
	
	public String getProductProvider() {	
		return getPartnerAsociated().getName();
	}
	
	public String getProviderCompleteName() {
		return getPartnerAsociated().getC_BPartner_ID()+"_"+this.partner.getName();
	}

} // MRequisitionLine
