package org.openXpertya.JasperReport;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.openXpertya.JasperReport.DataSource.MovimientoInventarioDataSource;
import org.openXpertya.model.MMovement;
import org.openXpertya.model.MProcess;
import org.openXpertya.process.ProcessInfo;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.Env;

import ar.com.geneos.mrp.plugin.model.MOrg;

public class LaunchMovimientoInventario extends SvrProcess {
	
	/** Jasper Report Wrapper*/
	private MJasperReport jasperwrapper;
	/** Jasper Report */
	private int AD_JasperReport_ID;
	/** Table */
	private int AD_Table_ID;	
	/** Record */
	private int AD_Record_ID;
	
	protected void prepare() {
		Integer m_Movement_ID = null;
		ProcessInfoParameter[] para = getParameter();
        for( int i = 0;i < para.length;i++ ) {
            String name = para[ i ].getParameterName();
            if( para[ i ].getParameter() == null ) ;
            else if( name.equalsIgnoreCase( "M_Movement_ID" )) {
            	m_Movement_ID = para[ i ].getParameterAsInt();
        	} 
        }

		// Determinar JasperReport para wrapper, tabla y registro actual
		ProcessInfo base_pi = getProcessInfo();
		int AD_Process_ID = base_pi.getAD_Process_ID();
		MProcess proceso = MProcess.get(Env.getCtx(), AD_Process_ID);
		if(proceso.isJasperReport() != true)
			return;

		this.AD_JasperReport_ID = proceso.getAD_JasperReport_ID();
		this.AD_Table_ID = getTable_ID();
		
		if(getRecord_ID()==0){
			AD_Record_ID = m_Movement_ID;
		} else{
			AD_Record_ID = getRecord_ID();	
		}
		jasperwrapper = new MJasperReport(getCtx(), AD_JasperReport_ID, get_TrxName());
	}
	
	@Override
	protected String doIt() throws Exception {
		
		MMovement movement = new MMovement(getCtx(), AD_Record_ID, get_TrxName());
		
		MovimientoInventarioDataSource ds = new MovimientoInventarioDataSource(get_TrxName());
		ds.setM_movement_id(movement.getM_Movement_ID());
		
		try {
			ds.loadData();
		} catch (RuntimeException e){
			throw new RuntimeException("No se pueden cargar los datos del informe", e);
		}
		
		//Parametros del Encabezado
		jasperwrapper.addParameter("NROCOMPROBANTE", movement.getDocumentNo());
		jasperwrapper.addParameter("FECHA", movement.getMovementDate());
		//jasperwrapper.addParameter("IMAGEPATH", getClass().getResourceAsStream("/images/mateo01.png"));
		
		MOrg org = new MOrg(getCtx(), Env.getAD_Org_ID(getCtx()), get_TrxName());
		InputStream is = null;
		byte[] aux = org.getreportlogo();
		if (aux != null)
			is = new ByteArrayInputStream(org.getreportlogo());
		jasperwrapper.addParameter("IMAGEPATH", is);
		
		/* 
		 * Tabla -> se completa con los Fields declarados en el JasperReport 
		 * (ver MovimientoInventarioDataSource.MovimientoInventarioDataSource(String))
		 */			
		try {
			jasperwrapper.fillReport(ds);
			jasperwrapper.showReport(getProcessInfo());
		} catch (RuntimeException e){
			throw new RuntimeException ("No se ha podido rellenar el informe.", e);
		}
		
		return "doIt";
	}
}
