package org.openXpertya.JasperReport;

import java.sql.Timestamp;
import java.util.Calendar;

import org.openXpertya.JasperReport.DataSource.Informe_OC_RR_DataSource;
import org.openXpertya.model.MProcess;
import org.openXpertya.process.ProcessInfo;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.Env;

public class LaunchInforme_OC_RR  extends SvrProcess {

	/** Jasper Report Wrapper*/
	private MJasperReport jasperwrapper;
	/** Jasper Report */
	private int AD_JasperReport_ID;
	/** Table */
	@SuppressWarnings("unused")
	private int AD_Table_ID;
	
	/** Parameters */
	private Timestamp from = null;
	private Timestamp to = null;
	private Integer partner = null;
	private Integer product = null;
	private Integer locator = null;
	
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
        for(int i = 0;i < para.length;i++) {
            String name = para[i].getParameterName();
            if(para[i].getParameter() != null) {
            	if(name.equalsIgnoreCase("From")) {
	            	from = (Timestamp)para[i].getParameter();
	            	continue;
	        	}
            	if(name.equalsIgnoreCase("To")) {
	            	to = (Timestamp)para[i].getParameter();
	            	continue;
	        	}
            	if(name.equalsIgnoreCase("Partner")) {
	            	partner = para[i].getParameterAsInt();
	            	continue;
	        	}
            	if(name.equalsIgnoreCase("Product")) {
	            	product = para[i].getParameterAsInt();
	            	continue;
	        	}
            	if(name.equalsIgnoreCase("Locator")) {
	            	locator = para[i].getParameterAsInt();
	            	continue;
	        	}
            }
        }

		// Determinar JasperReport para wrapper, tabla y registro actual
		ProcessInfo base_pi = getProcessInfo();
		int AD_Process_ID = base_pi.getAD_Process_ID();
		MProcess proceso = MProcess.get(Env.getCtx(), AD_Process_ID);
		if(proceso.isJasperReport() != true)
			return;

		this.AD_JasperReport_ID = proceso.getAD_JasperReport_ID();
		this.AD_Table_ID = getTable_ID();
		
		jasperwrapper = new MJasperReport(getCtx(), AD_JasperReport_ID, get_TrxName());
	}
	
	@Override
	protected String doIt() throws Exception {		
		Informe_OC_RR_DataSource ds = new Informe_OC_RR_DataSource(get_TrxName());
		ds.setFrom(from);
		ds.setTo(to);
		ds.setPartner_id(partner);
		ds.setProduct_id(product);
		ds.setLocator_id(locator);
		
		if(from == null && to == null && partner == null && product == null && locator == null) {
			throw new RuntimeException("Todos los parámetros están vacíos");
		}
		
		try {
			ds.loadData();
		} catch (RuntimeException e){
			throw new RuntimeException("No se pueden cargar los datos del informe", e);
		}
		
		//Parametros del Encabezado
		jasperwrapper.addParameter("FECHA", new Timestamp(Calendar.getInstance().getTime().getTime()));
		jasperwrapper.addParameter("IMAGEPATH", getClass().getResourceAsStream("/images/mateo01.png"));
		
		/* 
		 * Tabla -> se completa con los Fields declarados en el JasperReport 
		 * (ver Informe_OC_RR_DataSource)
		 */			
		try {
			jasperwrapper.fillReport(ds);
			jasperwrapper.showReport(getProcessInfo());
		} catch (RuntimeException e){
			throw new RuntimeException ("No se ha podido rellenar el informe.", e);
		}
		
		return "doIt";
	}

}
