package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;
import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.JasperReport.DataSource.RemitosComprasOMDataSource;
import org.openXpertya.model.MBPartner;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MProductCategory;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.wf.MWorkflow;

public class LaunchRemitosCompras extends JasperReportLaunch {
	
	/**	Proveedor */
	private int		p_C_BPartner_ID = 0;
	
	/**	Artículo */
	private int		p_M_Product_ID = 0;
	
	/**	Subfamilia */
	private int		p_M_Product_Category_ID = 0;
	
	/**	Correspondencia Remito/Pedido */
	private String		p_Correspondencia = "N";
	
	/**	Fecha de remito desde */
	private Timestamp p_Date_Remito = null;

	/**	Fecha de remito hasta */
	private Timestamp p_Date_Remito_To = null;
	
	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/** Linea del Informe **/
	protected int line = 0;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("Correpondencia"))
				p_Correspondencia =  (String)para[i].getParameter();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("MovementDate")){
				p_Date_Remito =  (Timestamp) para[i].getParameter();
				p_Date_Remito_To = (Timestamp) para[i].getParameter_To();
			} else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare
	
	

	@Override
	protected void loadReportParameters() throws Exception {
		
		MBPartner prov = new MBPartner(getCtx(), p_C_BPartner_ID, get_TrxName());
		MProduct prod = new MProduct(getCtx(), p_M_Product_ID, get_TrxName());
		MProductCategory prodCat = new MProductCategory(getCtx(), p_M_Product_Category_ID, get_TrxName());
		
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
		
		String[] desc;
		
		if(p_AD_Process.getDescription() != null && p_AD_Process.getDescription() != ""){
			desc = p_AD_Process.getDescription().split("%");
			
			if(desc.length > 1) {
				addReportParameter("P_FORM", desc[0]);
				addReportParameter("P_VERS", desc[1]);	
			} else {
				addReportParameter("P_FORM", "S/E");
				addReportParameter("P_VERS", "S/E");			
			}	
		
		} else {
			addReportParameter("P_FORM", "S/E");
			addReportParameter("P_VERS", "S/E");			
		}
		
		
		if(p_C_BPartner_ID != 0)
			addReportParameter("P_PROV", prov.getName());	
		else
			addReportParameter("P_PROV", "");
		
		if(p_M_Product_ID != 0)
			addReportParameter("P_PROD", prod.getName());
		else
			addReportParameter("P_PROD", "");

		if(p_M_Product_Category_ID != 0)
			addReportParameter("P_CAT", prodCat.getName());
		else
			addReportParameter("P_CAT", "");
		
		addReportParameter("P_CORRESPONDENCIA", p_Correspondencia);
		
		addReportParameter("P_DESDE", p_Date_Remito);
		addReportParameter("P_HASTA", p_Date_Remito_To);
			
		
	}	
	@Override
	protected String doIt() throws Exception {
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}

	@Override
	protected OXPJasperDataSource createReportDataSource() {
		return new RemitosComprasOMDataSource(p_C_BPartner_ID, p_M_Product_ID, p_M_Product_Category_ID, p_Date_Remito, p_Date_Remito_To, p_Correspondencia, get_TrxName());
	}

}	