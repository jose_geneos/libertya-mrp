package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;
import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.FaltantesOMDataSource;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.model.MLocator;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProduct;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.wf.MWorkflow;

public class LaunchFaltantesOM extends JasperReportLaunch {
	
	/**	ID Product */
	private int		p_M_Product_ID = 0;	
	
	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;	
	
	/**	Product */
	private MProduct	m_product = null;
	
	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule = null;

	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule_To = null;
	
	/**	Ubicación Origen */
	
	private int p_Locator_Origen_ID = 0;
	
	/**	Ubicación Destino */
	
	private int p_Locator_Destino_ID = 0;

	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/** Linea del Informe **/
	protected int line = 0;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("l1"))
				p_Locator_Origen_ID = para[i].getParameterAsInt();
			else if (name.equals("l2"))
				p_Locator_Destino_ID = para[i].getParameterAsInt();
			else if (name.equals("AD_Workflow_ID"))
				p_AD_Workflow_ID = para[i].getParameterAsInt();
			else if (name.equals("DateStartSchedule")){
				p_Start_Schedule =  (Timestamp) para[i].getParameter();
				p_Start_Schedule_To = (Timestamp) para[i].getParameter_To();
			} else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare
	
	

	@Override
	protected void loadReportParameters() throws Exception {
		
		MProduct prod = new MProduct(getCtx(), p_M_Product_ID, get_TrxName());
		MLocator loc_origen = new MLocator(getCtx(), p_Locator_Origen_ID, get_TrxName());
		MLocator loc_destino = new MLocator(getCtx(), p_Locator_Destino_ID, get_TrxName());
		MWorkflow wf = new MWorkflow(getCtx(), p_AD_Workflow_ID, get_TrxName());
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
		
		String[] desc;
		
		if(p_AD_Process.getDescription() != null && p_AD_Process.getDescription() != ""){
			desc = p_AD_Process.getDescription().split("%");
			
			if(desc.length > 1) {
				addReportParameter("P_FORM", desc[0]);
				addReportParameter("P_VERS", desc[1]);	
			} else {
				addReportParameter("P_FORM", "S/E");
				addReportParameter("P_VERS", "S/E");			
			}	
		
		} else {
			addReportParameter("P_FORM", "S/E");
			addReportParameter("P_VERS", "S/E");			
		}
		
		if(prod.getName() != null)
			addReportParameter("P_PROD", prod.getName());
		else
			addReportParameter("P_PROD", "");
		if(wf.getName() != null)
			addReportParameter("P_WF", wf.getName());
		else
			addReportParameter("P_WF", "");
		if( loc_origen.getValue() != null)
			addReportParameter("P_ORIGEN", loc_origen.getValue());
		else
			addReportParameter("P_ORIGEN", "");
		if(loc_destino.getValue() != null)
			addReportParameter("P_DESTINO", loc_destino.getValue());
		else
			addReportParameter("P_DESTINO", "");
		
				
		
	}
	
	@Override
	protected String doIt() throws Exception {
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}
	
	@Override
	protected OXPJasperDataSource createReportDataSource() {
		return new FaltantesOMDataSource(p_M_Product_ID, p_AD_Workflow_ID, p_Start_Schedule, p_Start_Schedule_To, p_Locator_Origen_ID, p_Locator_Destino_ID, get_TrxName());
	}

}
