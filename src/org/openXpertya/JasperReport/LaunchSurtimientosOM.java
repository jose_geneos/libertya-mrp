package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;
import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.JasperReport.DataSource.SurtimientosOMDataSource;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProductCategory;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.wf.MWorkflow;

public class LaunchSurtimientosOM extends JasperReportLaunch {
	
	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;	
	
	/**	Subfamilia de Artículo */
	private int		p_M_Product_Category_ID = 0;
	
	/**	Fecha de movimiento */
	private Timestamp p_Date = null;

	/**	Fecha de movimiento */
	private Timestamp p_Date_To = null;
	
	private String p_Entrega = "N";
	private String p_Terminado = "N";
	private String p_Coproducto = "N";
	
	
	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/** Linea del Informe **/
	protected int line = 0;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("AD_Workflow_ID"))
				p_AD_Workflow_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID")){
				p_M_Product_Category_ID =  para[i].getParameterAsInt();
			}
			else if (name.equals("copro")){
				p_Coproducto =  (String)para[i].getParameter();
			}
			else if (name.equals("mat")){
				p_Entrega =  (String)para[i].getParameter();
			}
			else if (name.equals("term")){
				p_Terminado =  (String)para[i].getParameter();
			}
			else if (name.equals("MovementDate")){
				p_Date =  (Timestamp) para[i].getParameter();
				p_Date_To = (Timestamp) para[i].getParameter_To();
			} else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare
	
	

	@Override
	protected void loadReportParameters() throws Exception {
		
		MWorkflow wf = new MWorkflow(getCtx(), p_AD_Workflow_ID, get_TrxName());
		
		MProductCategory pc = new MProductCategory(getCtx(), p_M_Product_Category_ID, get_TrxName());
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
		
		String[] desc = p_AD_Process.getDescription().split("%");
		
		addReportParameter("P_DESDE", p_Date);
		addReportParameter("P_HASTA", p_Date_To);
		
		String tipo = "";
		if(p_Entrega.equals("Y")) {
			tipo += "Materiales - ";
		}
		if(p_Terminado.equals("Y")) {
			tipo += "Terminado - ";
		}
		if(p_Coproducto.equals("Y")) {
			tipo += "Co Producto - ";
		}
		addReportParameter("P_TIPO", tipo);
		
		
		if(p_AD_Workflow_ID != 0)
			addReportParameter("P_WF", wf.getName());
		else
			addReportParameter("P_WF", "");
		
		if(p_M_Product_Category_ID != 0)
			addReportParameter("P_CAT", pc.getName());
		else
			addReportParameter("P_CAT", "");
		
		if(desc.length > 1) {
			addReportParameter("P_FORM", desc[0]);
			addReportParameter("P_VERS", desc[1]);	
		}		
			
	}	
	
	@Override
	protected String doIt() throws Exception {
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}

	@Override
	protected OXPJasperDataSource createReportDataSource() {
		return new SurtimientosOMDataSource(p_AD_Workflow_ID, p_M_Product_Category_ID, p_Date, p_Date_To, p_Entrega, p_Terminado, p_Coproducto, get_TrxName());
	}

	
}	