package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.EstadoPronosticoDataSource;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.model.MForecastLine;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MStorage;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;
import org.openXpertya.util.Trx;

import ar.com.geneos.mrp.plugin.model.MForecast;
import ar.com.geneos.mrp.plugin.model.MPPOrder;
import ar.com.geneos.mrp.plugin.model.MPPProductBOM;
import ar.com.geneos.mrp.plugin.model.MPPProductBOMLine;
import ar.com.geneos.mrp.plugin.model.MPPProductPlanning;

public class LaunchEstadoPronostico extends JasperReportLaunch {
	
	protected int p_M_Forecast_ID = 0;
	protected int p_S_Resource_ID = 0;
	protected int p_M_Warehouse_ID = 0;
	protected BigDecimal p_Qty = Env.ZERO;
	
	/** Linea del Informe **/
	protected int line = 0;

	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Forecast_ID"))
				p_M_Forecast_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare

	@Override
	protected void loadReportParameters() throws Exception {
		MForecast pron = new MForecast(getCtx(), p_M_Forecast_ID, get_TrxName());
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
				
		String[] desc;
		
		if(p_AD_Process.getDescription() != null && p_AD_Process.getDescription() != ""){
			desc = p_AD_Process.getDescription().split("%");
			
			if(desc.length > 1) {
				addReportParameter("P_FORM", desc[0]);
				addReportParameter("P_VERS", desc[1]);	
			} else {
				addReportParameter("P_FORM", "S/E");
				addReportParameter("P_VERS", "S/E");			
			}	
		
		} else {
			addReportParameter("P_FORM", "S/E");
			addReportParameter("P_VERS", "S/E");			
		}
		
		addReportParameter("P_PRON", pron.getName());

		
	}
	
	@Override
	protected String doIt() throws Exception {
	
		DB.executeUpdate("DELETE FROM T_RPTESTADOPRONOSTICO");
		
		MForecastLine[] forecastLines = MForecastLine.getMForecastLineFromForecast(getCtx(), p_M_Forecast_ID, null);

		for(int ind=0;ind<forecastLines.length;ind++) {
			
			int m_Product_ID = forecastLines[ind].getM_Product_ID();
			MProduct prod = new MProduct(getCtx(), m_Product_ID, null);
			int m_Warehouse_ID = forecastLines[ind].getM_Warehouse_ID();
			int s_Resource_ID = forecastLines[ind].getS_Resource_ID();
			
			ciclo (prod, m_Warehouse_ID, s_Resource_ID, forecastLines[ind].getQty(), forecastLines[ind].getDatePromised().toString());
					
		}
		
		 
		Trx.getTrx(get_TrxName()).commit();
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}
	
	/**
	 * 	Ciclo de componentes de producto elaborado
	 *	@param Producto
	 *  @param Almacén
	 *  @param Recurso
	 *	@return Info
	 * @throws Exception 
	 */
	
	private String ciclo (MProduct prod, int m_Warehouse_ID, int s_Resource_ID, BigDecimal qty, String datePromised) throws Exception {

		int ad_Workflow_ID = 0;
		String isBOM = "N";
		BigDecimal qtyProy = Env.ZERO;
		BigDecimal qtyItem = Env.ZERO;

		
		BigDecimal qtyAvailable = MStorage.getQtyAvailable(m_Warehouse_ID, prod.getM_Product_ID());
		
		if(qtyAvailable == null)
			qtyAvailable = Env.ZERO;
		
		qtyProy = MPPOrder.getQtyOrdersProduct(prod.getM_Product_ID());
		
		if (prod.isBOM()) {
			
			MPPProductPlanning prod_planning = MPPProductPlanning.get(getCtx(), m_Warehouse_ID, 
					s_Resource_ID, prod.getM_Product_ID(), get_TrxName(), true);
			
			if(prod_planning != null) {
				ad_Workflow_ID = prod_planning.getAD_Workflow_ID();
				isBOM = "Y";
		
				addLine(prod.getM_Product_ID(), qty, qtyAvailable, qtyProy, 
						ad_Workflow_ID, prod.getC_UOM_ID(), isBOM, datePromised);
								
				
				MPPProductBOM bom = prod_planning.getPP_Product_BOM();
				MPPProductBOMLine bomline[] = bom.getLines();
				MPPProductBOMLine bomline_item = null;

				for(int ind=0;ind<bomline.length; ind++) {
					bomline_item = bomline[ind];
					MProduct prodBOM = new MProduct(getCtx(), bomline_item.getM_Product_ID(), null);
					qtyItem = qty.multiply(bomline_item.getQty());
					ciclo(prodBOM, m_Warehouse_ID, s_Resource_ID, qtyItem, datePromised);
				}
			
			} else {
				// Existe producto sin planificación
				return "Producto sin planificación:  " + prod.getName();
			}
				
		} else {
			
			addLine(prod.getM_Product_ID(), qty, qtyAvailable, qtyProy, 
					ad_Workflow_ID, prod.getC_UOM_ID(), isBOM, datePromised);
			
		}
		return "#";
		
	}	//	validateProduct
	
	
	private void addLine(int product_ID, BigDecimal qty_ordered, BigDecimal qty_stock, BigDecimal qty_proy, 
						 int workflow_ID, int uom_ID, String isBOM, String date) throws Exception {
		// TODO Auto-generated method stub
		
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO T_RPTESTADOPRONOSTICO (ad_pinstance_id, ad_client_id, ad_org_id, m_product_id, ad_workflow_id, c_uom_id, isbom, qty_ordered, qty_stock, qty_proy, datepromised)");
		sql.append(" VALUES ( ").append(getAD_PInstance_ID()).append(",")
								.append(getAD_Client_ID()).append(",")
								.append(getCtx().getProperty("#AD_Org_ID")).append(",")
								.append(product_ID).append(",")
								.append(workflow_ID).append(",")
								.append(uom_ID).append(",'")
								.append(isBOM).append("',")
								.append(qty_ordered).append(",")
								.append(qty_stock).append(",")
								.append(qty_proy).append(",")
								.append("to_date('" + date + "', 'YYYY-MM-DD')").append(")");
		
		int no = DB.executeUpdate(sql.toString(), get_TrxName());
		if(no == 0) {
			throw new Exception("Error insertando datos en la tabla temporal");
		}
		
	}	

	protected Integer getProductID(){
		return (Integer)getParameterValue("M_Product_ID");
	}
	
	protected Integer getQty(){
		return (Integer)getParameterValue("Qty");
	}
	
	protected Integer getResourceID(){
		return (Integer)getParameterValue("M_Resource_ID");
	}	

	protected Integer getWarehouseID(){
		return (Integer)getParameterValue("M_Warehouse_ID");
	}
	
	@Override
	protected OXPJasperDataSource createReportDataSource() {
		return new EstadoPronosticoDataSource(get_TrxName());
	}

}
