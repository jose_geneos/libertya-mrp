package org.openXpertya.JasperReport.DataSource;

import java.sql.Timestamp;
import java.util.Vector;

public class Informe_OC_RR_DataSource extends QueryDataSource {
	
	private Vector<Object> parameters = new Vector<Object>();
	private Timestamp from = null;
	private Timestamp to = null;
	private Integer partner_id = null;
	private Integer product_id = null;
	private Integer locator_id = null;
	
	public Informe_OC_RR_DataSource(String trxName) {
		super(trxName);
	}

	@Override
	protected String getQuery() {
		String sql = ""+
			"SELECT o.documentno AS NRO_PEDIDO, o.dateOrdered AS FECHA_PEDIDO, bp.name AS PROVEEDOR, p.name AS ARTICULO, ol.qtyOrdered AS CANT_PEDIDO, io.documentno AS NRO_REMITO, io.dateacct AS FECHA_REMITO, iol.qtyEntered AS CANT_REMITO, l.value AS UBICACION, ol.qtyEntered AS CANT_TOTAL " + 
			"FROM M_MatchPO mpo " + 
			"INNER JOIN C_OrderLine ol ON (mpo.c_orderLine_id = ol.c_orderLine_id) " + 
			"INNER JOIN C_Order o ON (ol.c_order_id = o.c_order_id) " + 
			"INNER JOIN M_Product p ON (mpo.m_product_id = p.m_product_id) " + 
			"INNER JOIN C_BPartner bp ON (o.c_bPartner_id = bp.c_bPartner_id)" + 
			"INNER JOIN M_InOutLine iol ON (mpo.m_inoutline_id = iol.m_inoutline_id) " + 
			"INNER JOIN M_InOut io ON (iol.m_inout_id = io.m_inout_id) " + 
			"INNER JOIN M_Locator l ON (iol.m_locator_id = l.m_locator_id) " + 
			"WHERE 1 = 1";
		
		if(from != null && to != null) {
			sql += " AND o.dateOrdered BETWEEN ? AND ?";
			parameters.addElement(from);
			parameters.addElement(to);
		}
		if(partner_id != null) {
			sql += " AND bp.c_bPartner_id = ?";
			parameters.addElement(partner_id);
		}
		if(product_id != null) {
			sql += " AND p.m_product_id = ?";
			parameters.addElement(product_id);
		}
		if(locator_id != null) {
			sql += " AND l.locator_id = ?";
			parameters.addElement(locator_id);
		}
		
		sql += " ORDER BY o.documentno";
		
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return parameters.toArray();
	}

	public Timestamp getFrom() {
		return from;
	}

	public void setFrom(Timestamp from) {
		this.from = from;
	}

	public Timestamp getTo() {
		return to;
	}

	public void setTo(Timestamp to) {
		this.to = to;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Integer getLocator_id() {
		return locator_id;
	}

	public void setLocator_id(Integer locator_id) {
		this.locator_id = locator_id;
	}

}
