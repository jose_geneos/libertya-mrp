package org.openXpertya.JasperReport.DataSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.model.MLocator;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MStorage;
import org.openXpertya.model.MTransaction;
import org.openXpertya.util.CPreparedStatement;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;

import ar.com.geneos.mrp.plugin.model.MPPMRP;
import ar.com.geneos.mrp.plugin.util.MUDB;
import ar.com.geneos.mrp.plugin.util.MUMStorage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class StockProyectadoDataSource implements OXPJasperDataSource {

	int m_currentRecord = -1;
	int total_lines = -1;

	private List<MStockProyectadoPeriodoLineDTO> m_lines;
	private MStockProyectadoPeriodoLineDTO currentLine = null;

	/** Parametros */
	private Timestamp p_Date = null;
	private Timestamp p_Date_To = null;
	
	private int p_Product = 0;
	private int p_Product_Category = 0;
	private int p_Locator = 0;

	/** Context */
	private Properties p_ctx;
	private String trxName = null;

	/** Utilizado para mapear los campos con las invocaciones de los metodos */
	HashMap<String, String> methodMapper = new HashMap<String, String>();

	public StockProyectadoDataSource(Properties ctx, Timestamp p_Date, Timestamp p_Date_To,int p_Product, int p_Product_Category,int p_Locator,String trxName) {
		setTrxName(trxName);
		// TODO Auto-generated constructor stub
		this.p_ctx = ctx;
		this.p_Date = p_Date;
		this.p_Date_To = p_Date_To;
		this.p_Product = p_Product;
		this.p_Product_Category = p_Product_Category;
		this.p_Locator = p_Locator;

		methodMapper.put("AD_CLIENT_ID", "getAd_client_id");
		methodMapper.put("AD_ORG_ID", "getAd_org_id");
		methodMapper.put("ISACTIVE", "getIsActive");
		methodMapper.put("CREATED", "getCreated");
		methodMapper.put("CREATEDBY", "getCreatedby");
		methodMapper.put("UPDATED", "getUpdated");
		methodMapper.put("UPDATEDBY", "getUpdatedby");
		methodMapper.put("F_PRODUCTO", "getProducto");
		methodMapper.put("F_UBICACION", "getUbicacion");
		methodMapper.put("F_STOCKINICIAL", "getStockInicial");
		methodMapper.put("F_STOCKFINAL", "getStockFinal");
		methodMapper.put("F_TOTALENTRADAS", "getTotalEntradas");
		methodMapper.put("F_TOTALSALIDAS", "getTotalSalidas");
		methodMapper.put("F_VALORIZACION", "getValorizacion");

	}

	public String getTrxName() {
		return trxName;
	}

	public void setTrxName(String trxName) {
		this.trxName = trxName;
	}

	private List<MStockProyectadoPeriodoLineDTO> getLines() {
		return m_lines;
	}

	/*
	 * Obtengo todos los productos sobre los cuales se solicita la informacion
	 */
	protected Integer[] getProducts() {
	
		ArrayList<Integer> retValue = new ArrayList();
		int AD_Client_ID = Env.getAD_Client_ID(p_ctx);
		int AD_Ord_ID = Env.getAD_Org_ID(p_ctx);

		if (p_Product != 0)
			retValue.add(p_Product);
		else {
			String sql = "SELECT m_product_id from m_product where AD_Client_ID = "+AD_Client_ID + " AND AD_Org_Id = "+AD_Ord_ID;
			if (p_Product_Category != 0)
				sql += " AND m_product_category_id = "+p_Product_Category;
			sql+= " order by name";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {

				pstmt = DB.prepareStatement(sql, trxName);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					retValue.add(rs.getInt(1));
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
					if (rs != null)
						rs.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			
		}

		Integer[] aux = new Integer[retValue.size()];
		return retValue.toArray(aux);
	}
	
	/*
	 * Obtengo todos los productos sobre los cuales se solicita la informacion
	 */
	protected Integer[] getLocators() {
		//int M_Warehouse_ID = Integer.valueOf(Env.getContext(p_ctx, "#M_Warehouse_ID"));
		int AD_Client_ID = Env.getAD_Client_ID(p_ctx);
		int AD_Ord_ID = Env.getAD_Org_ID(p_ctx);
		ArrayList<Integer> retValue = new ArrayList();

		if (p_Locator != 0)
			retValue.add(p_Locator);
		else {
			String sql = "SELECT m_locator_id from m_locator where AD_Client_ID = "+AD_Client_ID + " AND AD_Org_Id = "+AD_Ord_ID;// + " AND M_Warehouse_ID =" + M_Warehouse_ID;
			sql+= " order by value";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {

				pstmt = DB.prepareStatement(sql, trxName);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					retValue.add(rs.getInt(1));
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (pstmt != null)
						pstmt.close();
					if (rs != null)
						rs.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			
		}

		Integer[] aux = new Integer[retValue.size()];
		return retValue.toArray(aux);
	}

	public void loadData() throws RuntimeException {
		
		m_lines = new ArrayList<MStockProyectadoPeriodoLineDTO>();
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer[] M_Locators_IDs = getLocators();
		try {
			
			for (Integer aProduct : getProducts()){
			
				MProduct prod = new MProduct(p_ctx, aProduct, trxName);
				
				
				//Por cada producto obtengo ubicaciones
				for (Integer aLocator : M_Locators_IDs) {
				
					MStockProyectadoPeriodoLineDTO aLine = new MStockProyectadoPeriodoLineDTO();
					
					aLine.setProducto(prod.getName());

					MLocator loc = new MLocator(p_ctx,aLocator,null);
					aLine.setUbicacion(loc.getValue());
					
					//Por cada ubicacion obtengo transacciones
					String whereClause = "M_Locator_Id = "+aLocator+" AND M_Product_ID = "+aProduct;
					BigDecimal stock = getSumColumn(p_ctx, MStorage.Table_Name, "qtyOnHand", whereClause, null, null);
					
					//Cantidad inicial (stock Actual - transacciones hasta fecha inicial)
					whereClause = "M_Locator_Id = "+aLocator+" AND M_Product_ID = "+aProduct+" AND MovementDate >= ?";
					BigDecimal qtyTransaction = getSumColumn(p_ctx, MTransaction.Table_Name, "MovementQty", whereClause, new Object[] {p_Date}, null);
					//Calculo
					BigDecimal cantInicial = stock.subtract(qtyTransaction);
					aLine.setStockInicial(cantInicial);
					
					//Ingresos entre fechas
					whereClause = "M_Locator_Id = "+aLocator+" AND M_Product_ID = "+aProduct+" AND MovementQty > 0 AND MovementDate between ? AND ?";
					BigDecimal ingresos = getSumColumn(p_ctx, MTransaction.Table_Name, "MovementQty", whereClause, new Object[] {p_Date,p_Date_To}, null);
					aLine.setTotalEntradas(ingresos);
					
					//Egresos entre fechas
					whereClause = "M_Locator_Id = "+aLocator+" AND M_Product_ID = "+aProduct+" AND MovementQty < 0 AND MovementDate between ? AND ?";
					BigDecimal egresos = getSumColumn(p_ctx, MTransaction.Table_Name, "MovementQty", whereClause, new Object[] {p_Date,p_Date_To}, null);
					aLine.setTotalSalidas(egresos);
					
					aLine.setStockFinal(cantInicial.add(ingresos).add(egresos));
					
					if ( aLine.getTotalEntradas().compareTo(BigDecimal.ZERO) == 0
							&& aLine.getTotalSalidas().compareTo(BigDecimal.ZERO) == 0
							&& aLine.getStockInicial().compareTo(BigDecimal.ZERO)== 0)
						continue;
					
					m_lines.add(aLine);
				}
			}

			total_lines = m_lines.size();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (rs != null)
					rs.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	class MStockProyectadoPeriodoLineDTO {

		private Date created;
		private String createdby;
		private String updatedby;
		private Date updated;
		private String ad_client_id;
		private int ad_org_id;
		private boolean isActive;

		private String producto;
		private BigDecimal stockInicial;
		private BigDecimal stockFinal;
		private BigDecimal totalEntradas;
		private BigDecimal totalSalidas;

		private BigDecimal valorizacion;
		private String ubicacion;


		public MStockProyectadoPeriodoLineDTO(ResultSet rs) throws SQLException {

			this.producto = rs.getString("producto");
			this.stockInicial = rs.getBigDecimal("stockInicial");
			this.stockFinal = rs.getBigDecimal("stockFinal");
			this.totalEntradas = rs.getBigDecimal("totalEntradas");
			this.totalSalidas = rs.getBigDecimal("totalSalidas");
			;
			this.valorizacion = rs.getBigDecimal("valorizacion");
			this.ubicacion = rs.getString("ubicacion");
		}

		public MStockProyectadoPeriodoLineDTO() {
			// TODO Auto-generated constructor stub
		}

		public Date getCreated() {
			return created;
		}

		public void setCreated(Date created) {
			this.created = created;
		}

		public String getCreatedby() {
			return createdby;
		}

		public void setCreatedby(String createdby) {
			this.createdby = createdby;
		}

		public String getUpdatedby() {
			return updatedby;
		}

		public void setUpdatedby(String updatedby) {
			this.updatedby = updatedby;
		}

		public Date getUpdated() {
			return updated;
		}

		public void setUpdated(Date updated) {
			this.updated = updated;
		}

		public String getAd_client_id() {
			return ad_client_id;
		}

		public void setAd_client_id(String ad_client_id) {
			this.ad_client_id = ad_client_id;
		}

		public int getAd_org_id() {
			return ad_org_id;
		}

		public void setAd_org_id(int ad_org_id) {
			this.ad_org_id = ad_org_id;
		}

		public boolean isActive() {
			return isActive;
		}

		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}

		public String getProducto() {
			return producto;
		}

		public void setProducto(String producto) {
			this.producto = producto;
		}

		public BigDecimal getStockInicial() {
			return stockInicial;
		}

		public void setStockInicial(BigDecimal stockInicial) {
			this.stockInicial = stockInicial;
		}

		public BigDecimal getStockFinal() {
			return stockFinal;
		}

		public void setStockFinal(BigDecimal stockFinal) {
			this.stockFinal = stockFinal;
		}

		public BigDecimal getTotalEntradas() {
			return totalEntradas;
		}

		public void setTotalEntradas(BigDecimal totalEntradas) {
			this.totalEntradas = totalEntradas;
		}

		public BigDecimal getTotalSalidas() {
			return totalSalidas;
		}

		public void setTotalSalidas(BigDecimal totalSalidas) {
			this.totalSalidas = totalSalidas;
		}

		public BigDecimal getValorizacion() {
			return valorizacion;
		}

		public void setValorizacion(BigDecimal valorizacion) {
			this.valorizacion = valorizacion;
		}

		public String getUbicacion() {
			return ubicacion;
		}

		public void setUbicacion(String ubicacion) {
			this.ubicacion = ubicacion;
		}

		

	}

	@Override
	/* Retorna el valor correspondiente al campo indicado */
	public Object getFieldValue(JRField field) throws JRException {

		String name = null;
		Class<?> clazz = null;
		Method method = null;
		Object output = null;
		try {
			// Invocar al metodo segun el campo correspondiente
			name = field.getName().toUpperCase();
			clazz = MStockProyectadoPeriodoLineDTO.class;
			method = clazz.getMethod(methodMapper.get(name));
			output = (Object) method.invoke(currentLine);
		} catch (NoSuchMethodException e) {
			throw new JRException("No se ha podido invocar el metodo " + methodMapper.get(name));
		} catch (InvocationTargetException e) {
			throw new JRException("Excepcion al invocar el método " + methodMapper.get(name));
		} catch (Exception e) {
			e.printStackTrace();
			throw new JRException("Excepcion general al acceder al campo " + name);
		}
		return output;
	}

	@Override
	public boolean next() throws JRException {
		m_currentRecord++;

		if (m_currentRecord >= total_lines) {
			return false;
		}

		currentLine = getLines().get(m_currentRecord);
		return true;
	}
	
	
	//Remove on migration (compatibilidad mateo) !
	/**
	 * Suma la columna con nombre columnName de la tabla con nombre tableName 
	 * que cumplen con las condiciones dadas en la cláusula where parámetro. 
	 * Esta cláusula puede tener parámetros (notación ?) que luego serán 
	 * cargados al Prepared Statement. Estos parámetros (whereParams) 
	 * que llevará el where deben pasarse en el orden de aparición en la 
	 * cláusula. Por ejemplo, el primer parámetro dentro de la cláusula, 
	 * debe ser el primer elemento dentro del array de objetos whereParams, 
	 * y así sucesivamente.
	 * <strong>NOTAS:</strong> No agregar WHERE a la cláusula parámetro, se
	 * agrega automáticamente.
	 * 
	 * @param ctx
	 *            el contexto se utilizará para crear los PO.
	 * @param tableName
	 *            nombre de la tabla involucrada.
	 * @param columnName
	 *            nombre de la columna involucrada.
	 * @param whereClause
	 *            cláusula where, en el caso de colocar algunas condiciones.
	 * @param whereParams
	 *            parámetros de la cláusula where, en el caso que hubiere.
	 * @param trxName
	 *            nombre de la transacción actual.
	 * @return Retorna la suma de la columna indicada, BigDecimal.ZERO
	 * 			en caso de no se encuentren registros para sumar.
	 * @author Gabriel Hernández - Disytel versión 1.0
	 */
	public static BigDecimal getSumColumn(Properties ctx, String tableName, String columnName,
			String whereClause, Object[] whereParams, String trxName) {
		// Armar la consulta sql
		StringBuffer sql = new StringBuffer("SELECT COALESCE(SUM(").append(columnName).append("),0) as ").append(columnName).append(" FROM ").append(tableName);
		// Si hay cláusula where, entonces coloco el where parámetro
		if ((whereClause != null) && (whereClause.trim().length() > 0)) {
			sql.append(" WHERE ");
			sql.append(whereClause);
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		BigDecimal res= BigDecimal.ZERO;
		try {
			ps = DB.prepareStatement(sql.toString(),
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE,
					trxName);
			if (whereParams != null) {
				int p = 1;
				for (int i = 0; i < whereParams.length; i++) {
					ps.setObject(p, whereParams[i]);
					p++;
				}
			}
			rs = ps.executeQuery();
			if (rs.next()) {
				res = rs.getBigDecimal(columnName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return res;
	}
}
