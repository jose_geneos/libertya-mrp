package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

public class CatProductosDataSource extends QueryDataSource {

	private int	p_M_Product_Category_ID = 0;

	private int p_M_Product_Gamas_ID = 0;

	private int p_M_Product_Lines_ID = 0;

	private String p_Comprado = "";

	private String p_Fabricado = "";
	
	public CatProductosDataSource(int d_M_Product_Category_ID, int d_M_Product_Gamas_ID, int d_M_Product_Lines_ID, String d_Comprado, String d_Fabricado, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		p_M_Product_Category_ID = d_M_Product_Category_ID;
		p_M_Product_Gamas_ID = d_M_Product_Gamas_ID;
		p_M_Product_Lines_ID = d_M_Product_Lines_ID;
		p_Comprado = d_Comprado;
		p_Fabricado = d_Fabricado;
	}
	
	
	@Override
	protected String getQuery() {
		
		String sql = 
				
				"SELECT prod.value as CODPROD, prod.name as NAMEPROD, " +
				"prod.isPurchased as ESCOMP, prod.isBOM as ESFAB, " +
				"subfam.name as SUBFAM, " +
				"fam.name as FAM, " +
				"lin.name as LINEA " +
				"FROM M_Product prod " + 
				"INNER JOIN M_Product_Category subfam ON subfam.M_Product_Category_ID = prod.M_Product_Category_ID " +
				"INNER JOIN M_Product_Gamas fam ON fam.M_Product_Gamas_ID = subfam.M_Product_Gamas_ID " + 
				"INNER JOIN M_Product_Lines lin ON lin.M_Product_Lines_ID = fam.M_Product_Lines_ID " + 
				"WHERE 1 = 1";
		
				if(p_M_Product_Gamas_ID != 0)
					sql += 	" AND fam.M_Product_Gamas_ID = " + p_M_Product_Gamas_ID;

				if(p_M_Product_Lines_ID != 0)
					sql += 	" AND lin.M_Product_Lines_ID = " + p_M_Product_Lines_ID;
				
				if(p_M_Product_Category_ID != 0)
					sql += 	" AND subfam.M_Product_Category_ID = " + p_M_Product_Category_ID;

				if(p_Comprado.equals("N"))
					sql += 	" AND prod.isPurchased = 'N'";
				else
					sql += 	" AND prod.isPurchased = 'Y'";

				if(p_Fabricado.equals("N"))
					sql += 	" AND prod.isBOM = 'N'";
				else
					sql += 	" AND prod.isBOM = 'Y'";
				
				sql += " order by prod.value";
				
				System.out.println(sql);
				
				
				
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
