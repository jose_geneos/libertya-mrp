package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

public class SuficienciaOMDataSource extends QueryDataSource {
	
	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;	
	
	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule = null;
	private Timestamp p_Start_Schedule_To = null;
	
	
	public SuficienciaOMDataSource(int p_AD_Work_ID, Timestamp p_Start_Sch, Timestamp p_Start_Sch_To, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		p_AD_Workflow_ID = p_AD_Work_ID;	
		p_Start_Schedule = p_Start_Sch;
		p_Start_Schedule_To = p_Start_Sch_To;		
	}

	@Override
	protected String getQuery() {
		
		String sql = "select v.documentno as NROORD, " +
				"p2.name as NAMECOMP, " +
				"p1.name as NAMEPROD, " +
				"v.componenttype as TIPOCOMP, " +
				"v.qtyentered as QTY, " +
				"v.qty_per_batch as QTYLOTE, " +
				"v.stock as STOCK, " +
				"v.available as DISP, " +
				"v.asi_description as PARTIDAS " +
				"FROM RV_PP_ORDER_LDM_STOCK v " +
				"INNER JOIN M_PRODUCT p1 ON p1.M_Product_ID = v.M_Product_ID " +
				"INNER JOIN M_PRODUCT p2 ON p2.M_Product_ID = v.m_product_id_final " +
				"WHERE v.AD_Workflow_ID = " + p_AD_Workflow_ID + " ";
				if(p_Start_Schedule != null && p_Start_Schedule_To != null) {
					sql += 	"AND datestartschedule >= to_date('" + p_Start_Schedule +  "', 'yyyy/mm/dd') " +
							"AND datestartschedule <= to_date('" + p_Start_Schedule_To +  "', 'yyyy/mm/dd') ";	
					
				}
				
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
