package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

public class FaltantesOMDataSource extends QueryDataSource {
	
	/**	ID Product */
	private int		p_M_Product_ID = 0;	
	
	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;	
	
	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule = null;
	private Timestamp p_Start_Schedule_To = null;
	
	/**	Ubicación Origen */
	
	private int p_Locator_Origen_ID = 0;
	
	/**	Ubicación Destino */
	
	private int p_Locator_Destino_ID = 0;

	
	
	public FaltantesOMDataSource(int p_M_Prod_ID, int p_AD_Work_ID, Timestamp p_Start_Sch, Timestamp p_Start_Sch_To, int p_Loc_Origen_ID, int p_Loc_Destino_ID, String trxName) {
		super(trxName);
		
		// TODO Auto-generated constructor stub

		p_M_Product_ID = p_M_Prod_ID;		
		p_AD_Workflow_ID = p_AD_Work_ID;	
		p_Start_Schedule = p_Start_Sch;
		p_Start_Schedule_To = p_Start_Sch_To;
		p_Locator_Origen_ID = p_Loc_Origen_ID;
		p_Locator_Destino_ID = p_Loc_Destino_ID;		
		
	}

	@Override
	protected String getQuery() {
		String sql = 	"select v.documentno as NROORD, " +
						"p2.name as PRODFINAL, " + 
						"v.datestartschedule as FECHAINCIOPROM, " +
						"p1.name as NAMEPROD, " +
						"v.qtyentered as QTYTEORICA, " +
						"v.qtymissing as QTYFALTANTE, " +
						"v.stock1 as STOCK1, " +
						"v.available1 as DISP1, " +
						"v.asi_description_l1 as DESC1, " +
						"v.stock2 as STOCK2, " +
						"v.available2 as DISP2, " +
						"v.asi_description_l2 as DESC2 " +
						"FROM RV_PP_OM_MISSING_COMPONENTS v " +
						"INNER JOIN M_PRODUCT p1 ON p1.M_Product_ID = v.M_Product_ID " +
						"INNER JOIN M_PRODUCT p2 ON p2.M_Product_ID = v.m_product_id_final " +
						"WHERE v.AD_Workflow_ID = " + p_AD_Workflow_ID + " " +
						"AND l2 = " + p_Locator_Destino_ID + " " +
						"AND datestartschedule >= to_date('" + p_Start_Schedule +  "', 'yyyy/mm/dd') " +
						"AND datestartschedule <= to_date('" + p_Start_Schedule_To +  "', 'yyyy/mm/dd') ";
		if(p_M_Product_ID != 0)
			sql += "AND M_Product_ID = " + p_M_Product_ID + " ";
		
		if(p_Locator_Origen_ID != 0)
			sql += "AND l1 = " + p_Locator_Origen_ID + " ";
		
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
