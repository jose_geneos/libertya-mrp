package org.openXpertya.JasperReport.DataSource;

public class MovimientoInventarioDataSource extends QueryDataSource {

	int m_movement_id;
	
	public MovimientoInventarioDataSource(String trxName) {
		super(trxName);
	}
	
	public void setM_movement_id(int m_movement_id) {
		this.m_movement_id = m_movement_id;
	}
	
	public int getM_movement_id() {
		return m_movement_id;
	}

	@Override
	protected String getQuery() {
		String sql = ""+
				"SELECT movementqty as CANTIDAD, p.name as ARTICULO , asi.description as ATRIBUTO, l.value AS DESDE, l2.value as HACIA " + 
				"FROM M_MovementLine ml " + 
				"INNER JOIN M_Product p ON (ml.m_product_id = p.m_product_id) " + 
				"INNER JOIN M_Locator l ON (ml.m_locator_id = l.m_locator_id) " + 
				"INNER JOIN M_Locator l2 ON (ml.m_locatorTo_id = l2.m_locator_id) " + 
				"INNER JOIN M_AttributeSetInstance asi ON (ml.m_attributeSetInstance_id = asi.m_attributeSetInstance_id) " + 
				"WHERE ml.m_movement_id = ?";
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		Object[] parameters = {m_movement_id};
		return parameters;
	}
}
