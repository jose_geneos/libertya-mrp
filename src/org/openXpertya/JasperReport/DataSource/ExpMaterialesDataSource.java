package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

public class ExpMaterialesDataSource extends QueryDataSource {
	
	public ExpMaterialesDataSource(String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getQuery() {
		String sql = 	"select prod.value || prod.name as NAMEPROD, temp.m_product_qty as QTYPROD, " +
						"comp.value || comp.name as NAMECOMP, temp.m_product_comp_qty as QTYCOMP, um.name as UM " + 	
						"FROM T_RPTComponentes temp " +
						"INNER JOIN M_Product prod ON (prod.M_Product_ID = temp.M_Product_ID) " +
						"INNER JOIN M_Product comp ON (comp.M_Product_ID = temp.M_Product_Comp_ID) " +
						"INNER JOIN C_UOM um ON (um.C_UOM_ID = temp.C_UOM_ID)";
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
