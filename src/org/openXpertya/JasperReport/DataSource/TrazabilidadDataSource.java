package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

public class TrazabilidadDataSource extends QueryDataSource {
	
	public TrazabilidadDataSource(String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getQuery() {
		String sql = 	"select pp_order_name as NROORD, " +
						"ad_workflow_name as NAMEWF, " + 
						"product_name as NAMEPROD, " +
						"m_attributesetinstance_name as NAMEATTR, " +
						"c_uom_name as NAMEUOM, " +
						"movementqty as QTYMOV, " +
						"movementdate as DATEMOV " +
						"FROM T_RPTTrazabilidad";
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
