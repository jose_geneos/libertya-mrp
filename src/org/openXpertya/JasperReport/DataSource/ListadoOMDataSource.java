package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

public class ListadoOMDataSource extends QueryDataSource {
	
	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule = null;
	/**	Fecha de inicio programada */
	private Timestamp p_Start_Schedule_To = null;
	
	/**	Fecha de finalización programada */
	private Timestamp p_Finish_Schedule = null;
	/**	Fecha de finalización programada */
	private Timestamp p_Finish_Schedule_To = null;
	
	/**	ID Product */
	private int	p_M_Product_ID = 0;	
	 
	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;		

	protected String p_Doc_Status = "";

	
	public ListadoOMDataSource(Timestamp p_St_Schedule, Timestamp p_St_Schedule_To, Timestamp p_Fin_Schedule, Timestamp p_Fin_Schedule_To, int p_M_Prod_ID, int p_AD_Work_ID, String p_DocSt, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		
		p_Start_Schedule = p_St_Schedule;
		p_Start_Schedule_To = p_St_Schedule_To;
		p_Finish_Schedule = p_Fin_Schedule;
		p_Finish_Schedule_To = p_Fin_Schedule_To;
		p_M_Product_ID = p_M_Prod_ID;	
		p_AD_Workflow_ID = p_AD_Work_ID;		
		p_Doc_Status = p_DocSt;		
		
	}

	@Override
	protected String getQuery() {
		String sql = 	"select om.documentno as NROORD, prod.value || prod.name as NAMEPROD, " +
						"om.docstatus as EST, alm.name as ALM, " +
						"wf.name as FLUJO, om.datestartschedule as FECHAINPROM, om.datefinishschedule as FECHAFINPROM, " +
						"om.qtyentered as QTYORD, om.qtydelivered as QTYDELIVERED " + 	
						"FROM PP_OM om " +
						"INNER JOIN M_Product prod ON (prod.M_Product_ID = om.M_Product_ID) " +
						"INNER JOIN AD_Workflow wf ON (wf.AD_Workflow_ID = om.AD_Workflow_ID) " +
						"INNER JOIN M_Warehouse alm ON (alm.M_Warehouse_ID = om.M_Warehouse_ID) ";
						
		if(p_Start_Schedule != null || p_Start_Schedule_To != null || p_Finish_Schedule != null 
		|| p_Finish_Schedule_To != null || p_M_Product_ID != 0 || p_AD_Workflow_ID != 0
		|| p_Doc_Status != "") {
			
			sql += "WHERE ";
			if(p_AD_Workflow_ID != 0)
				sql += "om.AD_Workflow_ID = " + p_AD_Workflow_ID + " AND ";
			if(p_M_Product_ID != 0)
				sql += "om.M_Product_ID = " + p_M_Product_ID + " AND ";
			if(p_Doc_Status != "")
				sql += "om.DocStatus = '" + p_Doc_Status + "' AND ";
			if(p_Start_Schedule != null && p_Start_Schedule_To != null)
				sql += 	"datestartschedule >= to_date('" + p_Start_Schedule +  "', 'yyyy/mm/dd') AND " +
						"datestartschedule <= to_date('" + p_Start_Schedule_To +  "', 'yyyy/mm/dd') AND ";
			if(p_Finish_Schedule != null && p_Finish_Schedule_To != null)
				sql += 	"datestartschedule >= to_date('" + p_Start_Schedule +  "', 'yyyy/mm/dd') AND " +
						"datestartschedule <= to_date('" + p_Start_Schedule_To +  "', 'yyyy/mm/dd')";
			
		}	
		
		String fin = sql.substring(sql.length()-4, sql.length());
		
		if (fin.equals("AND "))
			sql = sql.substring(0, sql.length()-4);
		

		
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
