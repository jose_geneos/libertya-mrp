package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

public class RemitosComprasOMDataSource extends QueryDataSource {
	
	public RemitosComprasOMDataSource(String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
	}

	/**	Proveedor */
	private int		p_C_BPartner_ID = 0;
	
	/**	Artículo */
	private int		p_M_Product_ID = 0;
	
	/**	Subfamilia */
	private int		p_M_Product_Category_ID = 0;
	
	/**	Correspondencia Remito/Pedido */
	private String		p_Correspondencia = "N";
	
	/**	Fecha de remito desde */
	private Timestamp p_Date_Remito = null;

	/**	Fecha de remito hasta */
	private Timestamp p_Date_Remito_To = null;
	
	
	public RemitosComprasOMDataSource(int d_C_BPartner_ID, int d_M_Product_ID, int d_M_Product_Category_ID, Timestamp d_Date_Remito,Timestamp d_Date_Remito_To, String d_Correspondencia, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		p_C_BPartner_ID = d_C_BPartner_ID;	
		p_M_Product_ID = d_M_Product_ID;
		p_M_Product_Category_ID = d_M_Product_Category_ID;
		p_Date_Remito = d_Date_Remito;
		p_Date_Remito_To = d_Date_Remito_To;
		p_Correspondencia = d_Correspondencia;
	}

	@Override
	protected String getQuery() {
		
		String sql = 
				
				"SELECT cab.documentno as NROREM, p.value as CODPROD, p.name as NAMEPROD, " +
				"line.movementqty as MOVQTY, cab.movementdate as DATE, " +
				"inst.description as PART, " +
				"CASE WHEN ord.documentno=null THEN '' ELSE ord.documentno END as NROOC," +
				"bp.name as NAMEPROV " +
				"FROM M_Inoutline line " + 
				"INNER JOIN M_Inout cab ON cab.M_Inout_ID = line.M_Inout_ID " + 
				"INNER JOIN C_Doctype dt ON dt.C_Doctype_ID = cab.C_Doctype_ID " + 
				"INNER JOIN M_Product p ON p.M_Product_ID = line.M_Product_ID " +
				"LEFT JOIN M_MatchPO matchpo ON matchpo.M_Inoutline_ID = line.M_Inoutline_ID " +
				"INNER JOIN M_AttributeSetInstance inst ON inst.M_AttributeSetInstance_ID = line.M_AttributeSetInstance_ID " +
				"LEFT JOIN C_OrderLine ordline on ordline.C_OrderLine_ID = matchpo.C_OrderLine_ID " +
				"LEFT JOIN C_Order ord on ord.C_Order_ID = ordline.C_Order_ID " +
				"INNER JOIN C_BPartner bp on bp.C_BPartner_ID = cab.C_BPartner_ID " +
				"WHERE 1 = 1";
				if(p_C_BPartner_ID != 0) {
					sql += 	" AND cab.C_BPartner_ID = " + p_C_BPartner_ID;	
				}
				if(p_M_Product_ID != 0) {
					sql += 	" AND line.M_Product_ID = " + p_M_Product_ID;	
				}
				if(p_M_Product_Category_ID != 0) {
					sql += 	" AND p.M_Product_Category_ID = " + p_M_Product_Category_ID;	
				}
				if(p_Date_Remito != null && p_Date_Remito_To != null) {
					sql += 	" AND cab.movementdate >= to_date('" + p_Date_Remito +  "', 'yyyy/mm/dd') " +
							" AND cab.movementdate <= to_date('" + p_Date_Remito_To +  "', 'yyyy/mm/dd') ";	
				}
				if(p_Correspondencia.equals("N")) {
					sql += 	" AND ord.documentno is null";	
				} else {
					sql += 	" AND ord.documentno is not null";
				}
				sql += " order by cab.M_Inout_ID";
				
				System.out.println(sql);
				
				
				
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
