package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

public class ProductoEnLDMDataSource extends QueryDataSource {

	private int	p_M_Product_ID = 0;
	
	public ProductoEnLDMDataSource(int d_M_Product_ID, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		p_M_Product_ID = d_M_Product_ID;
	}
	
	
	@Override
	protected String getQuery() {
		
		String sql = 
				
				"SELECT form.name as FORMULA, " +
				"CASE WHEN formlin.componenttype='CO' THEN 'Componente' WHEN formlin.componenttype='CP' THEN 'Coproducto' WHEN formlin.componenttype='PH' THEN 'Fantasma' ELSE 'Desconocido' END as TIPOCOMP, " +
				"formLin.name as NAMEPROD, " +
				"formLin.isCritical as ESCRITICO, formLin.isQtyPercentage as ESPORC, " +
				"uom.name as UM, " +
				"formLin.qtybom as QTY " +
				"FROM PP_Product_BOMLine formLin " + 
				"INNER JOIN PP_Product_BOM form ON form.PP_Product_BOM_ID = formLin.PP_Product_BOM_ID " +
				"INNER JOIN C_UOM uom ON uom.C_UOM_ID = formLin.C_UOM_ID " + 
				"WHERE 1 = 1";
		
				if(p_M_Product_ID != 0)
					sql += 	" AND formlin.M_Product_ID = " + p_M_Product_ID;

				sql += " order by form.name";
				
				System.out.println(sql);
				
				
				
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
