package org.openXpertya.JasperReport.DataSource;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

public class SurtimientosOMDataSource extends QueryDataSource {
	
	public SurtimientosOMDataSource(String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
	}

	/**	Flujo de Trabajo */
	private int		p_AD_Workflow_ID = 0;	
	
	/**	Subfamilia de Artículo */
	private int		p_M_Product_Category_ID = 0;
	
	/**	Fecha de movimiento */
	private Timestamp p_Date = null;

	/**	Fecha de movimiento */
	private Timestamp p_Date_To = null;
	
	private String p_Entrega = "N";
	private String p_Terminado = "N";
	private String p_Coproducto = "N";
	
	
	public SurtimientosOMDataSource(int p_AD_Work_ID, int p_M_Pro_Cat_ID, Timestamp p_DateMov, Timestamp p_DateMov_To, String p_Ent, String p_Ter, String p_Cop, String trxName) {
		super(trxName);
		// TODO Auto-generated constructor stub
		p_AD_Workflow_ID = p_AD_Work_ID;	
		p_M_Product_Category_ID = p_M_Pro_Cat_ID;
		p_Date = p_DateMov;
		p_Date_To = p_DateMov_To;	
		p_Entrega = p_Ent;
		p_Terminado = p_Ter;
		p_Coproducto = p_Cop;
	}

	@Override
	protected String getQuery() {
		
		String sql = "select p.name as NAMEPROD, " +
				"pc.name as NAMECAT, " +
				"part.description as PART, " +
				"cc.movementqty as MOVQTY, " +
				"w.name as FLUJO, " +
				"om.documentno as NROORD, " +
				"cc.movementdate as FECHA, " +
				"CASE WHEN cc.costcollectortype::text = ANY (ARRAY['110'::character varying::text, '115'::character varying::text]) THEN 'Materiales' " +
				"WHEN cc.costcollectortype::text = ANY (ARRAY['95'::character varying::text, '100'::character varying::text]) THEN 'Terminado' " +
				"WHEN cc.costcollectortype::text = ANY (ARRAY['85'::character varying::text, '105'::character varying::text]) THEN 'Co Producto.' " +
				"ELSE 'desconocido' END as TIPO " +
				"FROM pp_cost_collector cc " +
				"INNER JOIN m_product p ON p.m_product_id = cc.m_product_id " +
				"INNER JOIN pp_order om ON om.pp_order_id = cc.pp_order_id " +
				"INNER JOIN ad_workflow w ON w.ad_workflow_id = om.ad_workflow_id " +
				"INNER JOIN m_product_category pc ON pc.m_product_category_id = p.m_product_category_id " +
				"INNER JOIN m_attributesetinstance part ON part.m_attributesetinstance_id = cc.m_attributesetinstance_id " +
				"WHERE om.docstatus in ('CO','CL')" +
				"AND movementdate >= to_date('" + p_Date +  "', 'yyyy/mm/dd') " +
				"AND movementdate <= to_date('" + p_Date_To +  "', 'yyyy/mm/dd') ";
				
				if(p_AD_Workflow_ID != 0) {
					sql += 	"AND om.AD_Workflow_ID = " + p_AD_Workflow_ID + " ";					
				}
				if(p_M_Product_Category_ID != 0) {
					sql += 	"AND pc.M_Product_Category_ID = " + p_M_Product_Category_ID + " ";					
				}
				
				int flag = 0;
				
				if(p_Entrega.equals("Y")) {
					sql += 	"AND (cc.costcollectortype::text = ANY (ARRAY['110'::character varying::text, '115'::character varying::text])  ";
					flag = 1;
				}
				if(p_Terminado.equals("Y")) {
					if(flag == 0) {
						sql += 	"AND (cc.costcollectortype::text = ANY (ARRAY['95'::character varying::text, '100'::character varying::text]) ";
						flag = 1;
					} else
						sql += 	"OR cc.costcollectortype::text = ANY (ARRAY['95'::character varying::text, '100'::character varying::text]) ";
				}
				if(p_Coproducto.equals("Y")) {
					if(flag == 0) {
						sql += 	"AND (cc.costcollectortype::text = ANY (ARRAY['85'::character varying::text, '105'::character varying::text]) ";
						flag = 1;
					}else
						sql += 	"OR cc.costcollectortype::text = ANY (ARRAY['85'::character varying::text, '105'::character varying::text]) ";
				}				
				
				if (flag == 0)
					sql += 	"AND cc.docstatus = 'CO'::bpchar";
				else
					sql += 	") AND cc.docstatus = 'CO'::bpchar";
				
		return sql;
	}

	@Override
	protected Object[] getParameters() {
		return new Object[] {};
	}
	
	@Override
	protected boolean isQueryNoConvert(){
		return true;
	}

}
