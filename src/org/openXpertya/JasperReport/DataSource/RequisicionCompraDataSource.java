package org.openXpertya.JasperReport.DataSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.openXpertya.model.MRequisition;
import org.openXpertya.model.MRequisitionLine;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class RequisicionCompraDataSource implements JRDataSource {
		
	/** Utilizado para mapear los campos con las invocaciones de los metodos  */
	HashMap<String, String> methodMapper = new HashMap<String, String>();
	
	private int m_currentRecord = -1;
	private int total_lines = -1;
	private MRequisition requisition;
	private MRequisitionLine reqLine;
	
	public RequisicionCompraDataSource(MRequisition req) {
		this.requisition = req;
		
		//Fields declarados en el JasperReport
		methodMapper.put("CODARTICULO", "getProductCode");
		methodMapper.put("CANTIDAD", "getQty");
		methodMapper.put("DESCRIPCION", "getProductName");		
		methodMapper.put("PROVEEDOR", "getProductProvider");
		methodMapper.put("PRECIO_UNITARIO", "getPriceActual");
		methodMapper.put("IMPORTE", "getLineNetAmt");
		
	}

	public void loadData() throws RuntimeException {
		total_lines = requisition.getLines().length;
	}
	
	/* Retorna el valor correspondiente al campo indicado */
	public Object getFieldValue(JRField field) throws JRException {
		
		String name = null;
		Class<?> clazz = null;
		Method method = null;
		Object output = null;
		try {
			// Invocar al metodo segun el campo correspondiente
			name = field.getName().toUpperCase();
		    clazz = Class.forName("org.openXpertya.model.MRequisitionLine");
		    method = clazz.getMethod(methodMapper.get(name));
		    output = (Object) method.invoke(reqLine);
		} catch (ClassNotFoundException e) { 
			throw new JRException("No se ha podido obtener el valor del campo " + name); 
		} catch (NoSuchMethodException e) { 
			throw new JRException("No se ha podido invocar el metodo " + methodMapper.get(name)); 
		} catch (InvocationTargetException e) { 
			throw new JRException("Excepcion al invocar el método " + methodMapper.get(name)); 
		} catch (Exception e) { 
			throw new JRException("Excepcion general al acceder al campo " + name); 
		}
		return output;
		
	}

	public boolean next() throws JRException {
		m_currentRecord++;
		if (m_currentRecord >= total_lines )	{
			return false;
		}		
		reqLine = requisition.getLines()[m_currentRecord]; 
		return true;
	}

}
