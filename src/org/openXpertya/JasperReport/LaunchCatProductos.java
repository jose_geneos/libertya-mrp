package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.CatProductosDataSource;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProductCategory;
import org.openXpertya.model.MProductGamas;
import org.openXpertya.model.MProductLines;
import org.openXpertya.process.ProcessInfoParameter;

public class LaunchCatProductos extends JasperReportLaunch {
	
	private int p_M_Product_Category_ID = 0;
	private int p_M_Product_Gamas_ID;
	private int p_M_Product_Lines_ID;
	private String p_Fabricado;
	private String p_Comprado;
	
	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/** Linea del Informe **/
	protected int line = 0;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_Gamas_ID"))
				p_M_Product_Gamas_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Lines_ID"))
				p_M_Product_Lines_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("Fabricado"))
				p_Fabricado = (String)para[i].getParameter();
			else if (name.equals("Comprado"))
				p_Comprado = (String)para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare
	
	

	@Override
	protected void loadReportParameters() throws Exception {
		
		MProductGamas prodCat = new MProductGamas(getCtx(), p_M_Product_Gamas_ID, get_TrxName());
		MProductLines prodLin = new MProductLines(getCtx(), p_M_Product_Lines_ID, get_TrxName());
		MProductCategory prodSubCat = new MProductCategory(getCtx(), p_M_Product_Category_ID, get_TrxName());
		
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
		
		String[] desc = p_AD_Process.getDescription().split("%");
		
		
		if(p_M_Product_Gamas_ID != 0)
			addReportParameter("P_CAT", prodCat.getName());	
		else
			addReportParameter("P_CAT", "");
		
		if(p_M_Product_Lines_ID != 0)
			addReportParameter("P_LIN", prodLin.getName());
		else
			addReportParameter("P_LIN", "");

		if(p_M_Product_Category_ID != 0)
			addReportParameter("P_SUBCAT", prodSubCat.getName());
		else
			addReportParameter("P_SUBCAT", "");
		
		addReportParameter("P_FAB", p_Fabricado);
		addReportParameter("P_COMP", p_Comprado);
		
		if(desc.length > 1) {
			addReportParameter("P_FORM", desc[0]);
			addReportParameter("P_VERS", desc[1]);	
		}		
		
	}	
	@Override
	protected String doIt() throws Exception {
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}

	@Override
	protected OXPJasperDataSource createReportDataSource() {

		return new CatProductosDataSource(p_M_Product_Category_ID, p_M_Product_Gamas_ID, p_M_Product_Lines_ID, p_Comprado, p_Fabricado, get_TrxName());
	}

}	