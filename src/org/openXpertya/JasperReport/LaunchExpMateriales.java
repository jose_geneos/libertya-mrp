package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.util.logging.Level;

import org.openXpertya.JasperReport.DataSource.ExpMaterialesDataSource;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;

import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MWarehouse;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;
import org.openXpertya.util.Trx;

import ar.com.geneos.mrp.plugin.model.MPPProductBOM;
import ar.com.geneos.mrp.plugin.model.MPPProductBOMLine;
import ar.com.geneos.mrp.plugin.model.MPPProductPlanning;

public class LaunchExpMateriales extends JasperReportLaunch {
	
	protected int p_M_Product_ID = 0;
	protected int p_S_Resource_ID = 0;
	protected int p_M_Warehouse_ID = 0;
	protected BigDecimal p_Qty = Env.ZERO;
	
	/** Linea del Informe **/
	protected int line = 0;

	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("S_Resource_ID"))
				p_S_Resource_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("Qty"))
				p_Qty = (BigDecimal) para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare

	@Override
	protected void loadReportParameters() throws Exception {
		MProduct prod = new MProduct(getCtx(), p_M_Product_ID, get_TrxName());
		MWarehouse alm = new MWarehouse(getCtx(), p_M_Warehouse_ID, get_TrxName());
		
		// En esta versión inicial usamos el separados % para determinar nombre de formulario y versión
		// desde la descripción de los reportes accediendo desde el rol System. Ver mejora agregando
		// como datos actualizables en parámetros desde el rol Configuración de la Compañía.
		
		String[] desc = p_AD_Process.getDescription().split("%");
		
		addReportParameter("P_PROD", prod.getName());
		addReportParameter("P_CANT", p_Qty);
		addReportParameter("P_ALM", alm.getName());
		
		if(desc.length > 1) {
			addReportParameter("P_FORM", desc[0]);
			addReportParameter("P_VERS", desc[1]);	
		}
		
	}
	
	@Override
	protected String doIt() throws Exception {
	
		DB.executeUpdate("DELETE FROM T_RPTCOMPONENTES");
		
		String ret = ciclo(p_M_Product_ID, p_Qty, p_M_Warehouse_ID, p_S_Resource_ID, 0, Env.ZERO, line);
		
		Trx.getTrx(get_TrxName()).commit();
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		OXPJasperDataSource dataSource = createReportDataSource();
		dataSource.loadData();
		// Se rellena el reporte con el data source y se muestra.
		try {
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return ret;
	}
	
	/**
	 * 	Ciclo de componentes de producto elaborado
	 *	@param Producto
	 *  @param Almacén
	 *  @param Recurso
	 *	@return Info
	 * @throws Exception 
	 */
	
	private String ciclo (int prod, BigDecimal cant, int alm, int rec, int prod_padre, BigDecimal cant_padre, int line) throws Exception {
		
		MProduct prod_inst = new MProduct(getCtx(), prod, get_TrxName());
		
		if (prod_inst.isBOM()) {
		
			MPPProductPlanning prod_planning = MPPProductPlanning.get(getCtx(), alm, rec, prod, get_TrxName(), true);
			
			if(prod_planning != null) {
				
				MPPProductBOM bom = prod_planning.getPP_Product_BOM();
				MPPProductBOMLine bomline[] = bom.getLines();
				MPPProductBOMLine bomline_item = null;
				BigDecimal cant_item = Env.ZERO;
				
				addLine(prod, cant, prod_padre, cant_padre, prod_inst.getC_UOM_ID());
				
				for(int ind=0;ind<bomline.length; ind++) {
					bomline_item = bomline[ind];
					cant_item = cant.multiply(bomline_item.getQty());
					ciclo (bomline_item.getM_Product_ID(),cant_item, alm, rec, prod, cant, line);
				}
				
			} else {
				// Existe producto sin planificación
				return "Producto sin planificación:  " + prod_inst.getName();
			}
			
			
		} else {
			
			addLine(prod, cant, prod_padre, cant_padre, prod_inst.getC_UOM_ID());
			
		}

		return "#" + line;
		
	}	//	validateProduct


	private void addLine(int prod, BigDecimal cant, int prod_padre, BigDecimal cant_padre, int uom) throws Exception {
		// TODO Auto-generated method stub
		
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO T_RPTComponentes (ad_pinstance_id, ad_client_id, ad_org_id, m_product_id, m_product_qty, m_product_comp_id, m_product_comp_qty, c_uom_id)");
		sql.append(" VALUES ( ").append(getAD_PInstance_ID()).append(",")
								.append(getAD_Client_ID()).append(",")
								.append(getCtx().getProperty("#AD_Org_ID")).append(",")
								.append(prod_padre).append(",")
								.append(cant_padre).append(",")
								.append(prod).append(",")
								.append(cant).append(",")
								.append(uom).append(")");
		
		int no = DB.executeUpdate(sql.toString(), get_TrxName());
		if(no == 0) {
			throw new Exception("Error insertando datos en la tabla temporal");
		}
		
	}
	
	

	protected Integer getProductID(){
		return (Integer)getParameterValue("M_Product_ID");
	}
	
	protected Integer getQty(){
		return (Integer)getParameterValue("Qty");
	}
	
	protected Integer getResourceID(){
		return (Integer)getParameterValue("M_Resource_ID");
	}	

	protected Integer getWarehouseID(){
		return (Integer)getParameterValue("M_Warehouse_ID");
	}
	
	@Override
	protected OXPJasperDataSource createReportDataSource() {
		return new ExpMaterialesDataSource(get_TrxName());
	}

}
