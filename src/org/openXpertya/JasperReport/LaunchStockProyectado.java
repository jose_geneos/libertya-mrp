package org.openXpertya.JasperReport;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;
import java.util.logging.Level;

import org.openXpertya.JasperReport.JasperReportLaunch;
import org.openXpertya.JasperReport.DataSource.OXPJasperDataSource;
import org.openXpertya.JasperReport.DataSource.StockProyectadoDataSource;
import org.openXpertya.model.MLocator;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MProductCategory;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.wf.MWorkflow;

public class LaunchStockProyectado extends JasperReportLaunch {

	/**	Fecha desde / hasta */
	private Timestamp p_Date = null;
	private Timestamp p_Date_To = null;
	
	private int p_Product = 0;
	private int p_Product_Category = 0;
	private int p_Locator = 0;
	
	/**	Original Process */
	MProcess p_AD_Process = null;
	
	/** Linea del Informe **/
	protected int line = 0;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		int reg_proceso = this.getProcessInfo().getAD_Process_ID();
		p_AD_Process = new MProcess(getCtx(), reg_proceso, get_TrxName());
		
		
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("Date")){
				p_Date =  (Timestamp) para[i].getParameter();
				p_Date_To = (Timestamp) para[i].getParameter_To();
			} else if (name.equals("M_Product_ID")){
				p_Product =  para[i].getParameterAsInt();
			} else if (name.equals("M_Locator_ID")){
				p_Locator =  para[i].getParameterAsInt();
			} else if (name.equals("M_Product_Category_ID")){
				p_Product_Category = para[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare
	
	

	@Override
	protected void loadReportParameters() throws Exception {
		
		addReportParameter("P_DESDE", p_Date);
		addReportParameter("P_HASTA", p_Date_To);
		
		if (p_Product != 0){
			MProduct prod = new MProduct(getCtx(),p_Product,null);
			addReportParameter("P_PRODUCTO", prod.getValue());		
		}
		
		if (p_Product_Category != 0){
			MProductCategory cat = new MProductCategory(getCtx(),p_Product_Category,null);
			addReportParameter("P_SUBFAMILIA", cat.getName());	
		}
		
		if (p_Locator != 0) {
			MLocator loc = new MLocator(getCtx(),p_Locator,null);
			addReportParameter("P_UBICACION", loc.getValue());
		}

		
	}	
	
	@Override
	protected String doIt() throws Exception {
		
		// Se cargan los parámetros específicos del reporte.
		loadReportParameters();
		// Se crea el data source para el reporte y se cargan los datos del mismo.
		StockProyectadoDataSource dataSource = createReportDataSource();
		
		// Se rellena el reporte con el data source y se muestra.
		try {
			dataSource.loadData();
			getReportWrapper().fillReport(dataSource);
			getReportWrapper().showReport(getProcessInfo());
		} catch (RuntimeException e) {
			throw new Exception ("@JasperReportFillError@", e);
		}
		return "";
	}

	@Override
	protected StockProyectadoDataSource createReportDataSource() {
		return new StockProyectadoDataSource(getCtx(),p_Date, p_Date_To,p_Product,p_Product_Category,p_Locator, get_TrxName());
	}

}	