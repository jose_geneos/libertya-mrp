package org.openXpertya.JasperReport;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.openXpertya.JasperReport.DataSource.RequisicionCompraDataSource;
import org.openXpertya.model.MProcess;
import org.openXpertya.model.MRequisition;
import org.openXpertya.process.ProcessInfo;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.Env;

import ar.com.geneos.mrp.plugin.model.MOrg;

public class LaunchRequisicionCompra extends SvrProcess {
	
	/** Jasper Report Wrapper*/
	private MJasperReport jasperwrapper;
	/** Jasper Report */
	private int AD_JasperReport_ID;
	/** Table */
	private int AD_Table_ID;	
	/** Record */
	private int AD_Record_ID;
	
	@Override
	protected void prepare() {
		// Se toma el parametro C_Order_ID. Este parametro es necesario cuando se invoca el proceso 
		// imprimir desde codigo.
		// En el caso de ser necesario también se podria pasar como parámetro AD_Table_ID
		Integer m_Requisition_ID = null;
		ProcessInfoParameter[] para = getParameter();
        for( int i = 0;i < para.length;i++ ) {
            String name = para[ i ].getParameterName();
            if( para[ i ].getParameter() == null ) ;
            else if( name.equalsIgnoreCase( "M_Requisition_ID" )) {
            	m_Requisition_ID = para[ i ].getParameterAsInt();
        	} 
        }

		// Determinar JasperReport para wrapper, tabla y registro actual
		ProcessInfo base_pi = getProcessInfo();
		int AD_Process_ID = base_pi.getAD_Process_ID();
		MProcess proceso = MProcess.get(Env.getCtx(), AD_Process_ID);
		if(proceso.isJasperReport() != true)
			return;

		this.AD_JasperReport_ID = proceso.getAD_JasperReport_ID();
		this.AD_Table_ID = getTable_ID();
		
		if(getRecord_ID()==0){
			AD_Record_ID = m_Requisition_ID;
		}
		else{
			AD_Record_ID = getRecord_ID();	
		}
		jasperwrapper = new MJasperReport(getCtx(), AD_JasperReport_ID, get_TrxName());

	}
	
	@Override
	protected String doIt() throws Exception {
		
		MRequisition requisition = new MRequisition(getCtx(), AD_Record_ID, get_TrxName());
		
		RequisicionCompraDataSource ds = new RequisicionCompraDataSource(requisition);
		
		try {
			ds.loadData();
		} catch (RuntimeException e){
			throw new RuntimeException("No se pueden cargar los datos del informe", e);
		}
		
		//Parametros del Encabezado
		jasperwrapper.addParameter("NROCOMPROBANTE", requisition.getDocumentNo());
		jasperwrapper.addParameter("FECHA", requisition.getDateDoc());
		jasperwrapper.addParameter("FECHA_REQUERIDA", requisition.getDateRequired());
		//jasperwrapper.addParameter("IMAGEPATH", getClass().getResourceAsStream("/images/mateo01.png"));
		
		MOrg org = new MOrg(getCtx(), Env.getAD_Org_ID(getCtx()), get_TrxName());
		InputStream is = null;
		byte[] aux = org.getreportlogo();
		if (aux != null)
			is = new ByteArrayInputStream(org.getreportlogo());
		jasperwrapper.addParameter("IMAGEPATH", is);
		
		/*
		 * Otra manera de cargar la imagen del informe (desde una ruta estatica). 
		 */
		/*BufferedImage img = null;
		InputStream is = null;
		try {
		    img = ImageIO.read(new File("/home/igomez/workspace/mateo_plugin/src/images/mateo01.png"));
		    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		    ImageIO.write(img, "jpg", outStream); 
		    is = new ByteArrayInputStream(outStream.toByteArray());
		} catch (IOException e) {
		}
		jasperwrapper.addParameter("IMAGEPATH", is);*/
		
		/* 
		 * Tabla -> se completa con los Fields declarados en el JasperReport 
		 * (ver RequisitionCompraDataSource.RequisicionCompraDataSource(MRequisition))
		 */
		
		//Parametros del pie de página
		jasperwrapper.addParameter("TOTAL", requisition.getTotalLines());
		
		try {
			jasperwrapper.fillReport(ds);
			jasperwrapper.showReport(getProcessInfo());
		} catch (RuntimeException e)	{
			throw new RuntimeException ("No se ha podido rellenar el informe.", e);
		}
		
		return "doIt";
	}

}
