package org.openXpertya.grid.ed;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import ar.com.geneos.mrp.plugin.process.RequisitionPOCreate;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Vector;

public class Vedit_ReqToOC extends JPanel implements ActionListener { 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JFrame frame;    
	private JTable table;
    private JButton apply;
    private JButton duplicate;
    private JButton save;
    private JTextArea output;
    private Dimension dimension;
        
    public Vedit_ReqToOC(Vector<Vector<Object>> info, final RequisitionPOCreate req){
        super();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        RequisitionPOCreate.process_cancel = true;
        
        if(getDimension() == null) {
        	this.dimension = new Dimension(800, 350);
        }
        
        MyTableModel myTableModel = new MyTableModel(info);        
        table = new JTable(myTableModel);
        table.setPreferredScrollableViewportSize(this.dimension);
        table.setFillsViewportHeight(true);
        add(new JScrollPane(table));

       //Area de botones de funcionalidad
        JPanel centerPanel = new JPanel(new GridLayout(1, 2));
        JPanel partnerAction = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final VLookup partnerField = VLookup.createBPartner(3);
        partnerField.setSize(new Dimension(110, 30));
        partnerAction.add(partnerField);           
        apply = new JButton("Aplicar");
        apply.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {				
				int lines = 0;
				for(int r=0; r<table.getModel().getRowCount(); r++) {
					if((Boolean)(table.getModel().getValueAt(r, 5))) {
						table.getModel().setValueAt(partnerField.getDisplay().substring(partnerField.getDisplay().indexOf('_')+1), r, 4);
						table.getModel().setValueAt(partnerField.getValue(), r, 7);
						lines++;
					}
				}
				if(lines == 1) {
					output.append("Se actualizó 1 linea al valor: "+ partnerField.getDisplay().substring(partnerField.getDisplay().indexOf('_')+1) +"\n");
				} else {
					output.append("Se actualizaron "+ lines +" lineas al valor: "+ partnerField.getDisplay().substring(partnerField.getDisplay().indexOf('_')+1) +"\n");
				}				
			}
		});
        apply.setPreferredSize(new Dimension(90, 25));
        partnerAction.add(apply);                          
        centerPanel.add(partnerAction);
        
        JPanel duplicatePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        duplicate = new JButton("Duplicar");
        duplicate.setPreferredSize(new Dimension(90, 25));
        duplicate.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				int r = table.getSelectedRow();
				Vector<Object> last = new Vector<Object>();
				last.add(new Boolean(true));
				last.add(table.getModel().getValueAt(r, 1));
				last.add(0F);
		    	last.add(table.getModel().getValueAt(r, 3));
		    	last.add("");
		    	last.add(new Boolean(false));
		    	//Columnas no visibles
		    	last.add(table.getModel().getValueAt(r, 6)); //el ID debería ser = 0 ?
		    	last.add(table.getModel().getValueAt(r, 7));
		    	last.add(table.getModel().getValueAt(r, 8));
		    	last.add(table.getModel().getValueAt(r, 9));
		    	last.add(table.getModel().getValueAt(r, 10));
		    	last.add(table.getModel().getValueAt(r, 11));
		    	last.add(table.getModel().getValueAt(r, 12));
		    	last.add(table.getModel().getValueAt(r, 13));
		    	last.add(table.getModel().getValueAt(r, 14));
		    	last.add(table.getModel().getValueAt(r, 15));
		    	((MyTableModel)table.getModel()).getData().add(++r, last);
		    	table.updateUI();
				output.append("Se duplicó la línea del producto "+ table.getModel().getValueAt(r, 3) +"\n");				
			}
		});
        duplicatePanel.add(duplicate);
        centerPanel.add(duplicatePanel);
        add(centerPanel);

        //Area de salida de notificaciones
        output = new JTextArea(10, 40);
        output.setEditable(false);
        add(new JScrollPane(output));
        
        //Panel sur
        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        save = new JButton("Procesar");
        save.setPreferredSize(new Dimension(110, 30));
        save.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				output.append("Guardando y saliendo...\n");
				try{
					synchronized(req) {
						RequisitionPOCreate.process_cancel = false;
						req.notify();
					}
				} finally{
					frame.dispose();
				};
				
			}
		});
        southPanel.add(save);
        add(southPanel);
    }

    public void actionPerformed(ActionEvent event) {
//        String command = event.getActionCommand();
        //No hace nada.
    }
    
    public Dimension getDimension() {
    	return this.dimension;
    }
    
    public void setDimension(Dimension d) {
    	this.dimension = d;
    }
    
    class MyTableModel extends AbstractTableModel {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames = {"Procesar", "Requisición", "Cantidad", "Producto", "Proveedor", "Seleccion"};
        private Vector<Vector<Object>> data; 

        public MyTableModel() {
        	this.data = new Vector<Vector<Object>>();
        }
        
        public MyTableModel(Vector<Vector<Object>> d) {
        	this.data = d;
        }
        
        public int getColumnCount() {        
            return columnNames.length;
        }

        public int getRowCount() {
        	return data.size();
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
        	return data.elementAt(row).elementAt(col);
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {          
            if (col == 1 || col == 3 || col == 4)  {
                return false;
            } else {
                return true;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
        	data.elementAt(row).set(col, value);
            fireTableCellUpdated(row, col);
        }
        
        public void addRow(Object[] row){
        	Vector<Object> last = new Vector<Object>();
        	last.add(row[0]);
        	last.add(row[1]);
        	last.add(row[2]);
        	last.add(row[3]);
        	data.addElement(last);
        }
        
        public Vector<Vector<Object>> getData(){
        	return data;
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI(Vector<Vector<Object>> info, final RequisitionPOCreate req) {
        //Disable boldface controls.
        UIManager.put("swing.boldMetal", Boolean.FALSE); 

        //Create and set up the window.
        frame = new JFrame("Requisición a Orden de compra");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
            	/*if (JOptionPane.showConfirmDialog(frame, 
		            "Are you sure to close this window?", "Really Closing?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		            System.exit(0);
		        }*/
            	
            	try{
					synchronized(req) {
						RequisitionPOCreate.process_cancel = true;
						req.notify();
					}
				} finally{
					frame.dispose();
				};
            }
        });
        
        //Create and set up the content pane.
        Vedit_ReqToOC newContentPane = new Vedit_ReqToOC(info, req);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
        
        frame.setSize(new Dimension(800, 300));
        frame.setResizable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((dim.width - newContentPane.getDimension().width)/2, (dim.height - newContentPane.getDimension().height)/2);

        //Display the window.
        //frame.pack();
        frame.setVisible(true);
        
    }

    /*public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {           	
                Vector<Vector<Object>> info = new Vector<Vector<Object>>();
                RequisitionPOCreate req = new RequisitionPOCreate();
                createAndShowGUI(info, req);
            }
        });
    }*/
} 
