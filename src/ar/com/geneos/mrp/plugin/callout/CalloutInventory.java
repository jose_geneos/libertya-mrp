/*
 *    El contenido de este fichero está sujeto a la  Licencia Pública openXpertya versión 1.1 (LPO)
 * en tanto en cuanto forme parte íntegra del total del producto denominado:  openXpertya, solución 
 * empresarial global , y siempre según los términos de dicha licencia LPO.
 *    Una copia  íntegra de dicha  licencia está incluida con todas  las fuentes del producto.
 *    Partes del código son CopyRight (c) 2002-2007 de Ingeniería Informática Integrada S.L., otras 
 * partes son  CopyRight (c) 2002-2007 de  Consultoría y  Soporte en  Redes y  Tecnologías  de  la
 * Información S.L.,  otras partes son  adaptadas, ampliadas,  traducidas, revisadas  y/o mejoradas
 * a partir de código original de  terceros, recogidos en el  ADDENDUM  A, sección 3 (A.3) de dicha
 * licencia  LPO,  y si dicho código es extraido como parte del total del producto, estará sujeto a
 * su respectiva licencia original.  
 *     Más información en http://www.openxpertya.org/ayuda/Licencia.html
 */



package ar.com.geneos.mrp.plugin.callout;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.openXpertya.model.CalloutEngine;
import org.openXpertya.model.MField;
import org.openXpertya.model.MTab;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;
import org.openXpertya.util.Util;

/**
 * Descripción de Clase
 *
 *
 * @version    2.2, 12.10.07
 * @author     Equipo de Desarrollo de openXpertya    
 */

public class CalloutInventory extends CalloutEngine {
	
    /**
     * Descripción de Método
     *
     *
     * @param ctx
     * @param WindowNo
     * @param mTab
     * @param mField
     * @param value
     *
     * @return
     */

    public String locator( Properties ctx,int WindowNo,MTab mTab,MField mField,Object value ) {
        Integer M_Product_ID = (Integer) mTab.getValue( "M_Product_ID");
        if( (M_Product_ID == null) || (M_Product_ID.intValue() == 0) ) {
            Env.setContext(ctx, WindowNo, "ProductType", "");
            return "";
        }

       

        int M_Locator_ID = (Integer) mTab.getValue( "M_Locator_ID");
        int M_AttributeSetInstance_ID = (Integer) mTab.getValue( "M_AttributeSetInstance_ID");
   
        if (M_Locator_ID == 0 || M_AttributeSetInstance_ID == 0) {
        	return "";
        }
        
        // Set Qty
        setCalloutActive( true );
        String sql = "SELECT QtyOnHand FROM M_Storage " + "WHERE M_Product_ID=?"    // 1
                         + " AND M_Locator_ID=?" + " AND M_AttributeSetInstance_ID=?";    // 2

            try {
                PreparedStatement pstmt = DB.prepareStatement( sql );

                pstmt.setInt( 1,M_Product_ID.intValue());
                pstmt.setInt( 2,M_Locator_ID );
                pstmt.setInt( 3,M_AttributeSetInstance_ID );

                ResultSet rs = pstmt.executeQuery();

                if( rs.next()) {
                    BigDecimal bd = rs.getBigDecimal( 1 );

                    if( !rs.wasNull()) {
                        mTab.setValue( "QtyCount",bd );
                    }
                }

                rs.close();
                pstmt.close();
            } catch( SQLException e ) {
                log.log( Level.SEVERE,"product",e );
                setCalloutActive( false );
                return e.getLocalizedMessage();
            }
        return "";
    }    // product
 
}    // CalloutInventory



/*
 *  @(#)CalloutInventory.java   02.07.07
 * 
 *  Fin del fichero CalloutInventory.java
 *  
 *  Versión 2.2
 *
 */
