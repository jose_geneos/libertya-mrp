package ar.com.geneos.mrp.plugin.callout;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.util.Properties;

import org.openXpertya.model.CalloutEngine;
import org.openXpertya.model.MField;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MTab;

/**
 * Callout Sales Order
 *
 * @author Pablo Velazquez - pablo.velazquez@geneos.com.ar
 * @author José Maria Fantasia - jose.fantasia@geneos.com.ar
 */
public class CalloutSalesOrder extends CalloutEngine {


	/**
	 * Copy description from product
	 * 
	 * @param ctx
	 *            Context
	 * @param WindowNo
	 *            current Window No
	 * @param mTab
	 *            Model Tab
	 * @param mField
	 *            Model Field
	 * @param value
	 *            The new value
	 */
	
	public String copyDescription(Properties ctx, int WindowNo, MTab mTab, MField mField, Object value) {

		setCalloutActive(true);
		
		if (value == null) {
			setCalloutActive(false);
			return "";
		}		
		
		int M_Product_ID = (Integer)value;
		MProduct prod = new MProduct(ctx, M_Product_ID, null);
		
		if(prod != null)
			mTab.setValue("Description", prod.getDescription());
		
		setCalloutActive(false);
		
		return "";
		
	} // copyDescription

} // CalloutSalesOrder

