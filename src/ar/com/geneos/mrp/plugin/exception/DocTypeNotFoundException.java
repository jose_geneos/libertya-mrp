package ar.com.geneos.mrp.plugin.exception;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import org.openXpertya.model.MDocType;
import org.openXpertya.model.MRefList;
import org.openXpertya.util.Env;
import org.openXpertya.util.Util;

public class DocTypeNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4218893853798807816L;
	/** Doc Base Type */
	private String m_docBaseType = null;

	/**
	 * @param docBaseType
	 *            Document Base Type (see MDocType.DOCBASETYPE_*)
	 * @param additionalInfo
	 *            optional if there is some additional info
	 */
	public DocTypeNotFoundException(String docBaseType, String additionalInfo) {
		super(additionalInfo);
		m_docBaseType = docBaseType;
	}

	public String getDocBaseType() {
		return m_docBaseType;
	}

	@Override
	public String getMessage() {
		String additionalInfo = super.getMessage();
		String docBaseTypeName = MRefList.getListName(Env.getCtx(), MDocType.DOCBASETYPE_AD_Reference_ID, getDocBaseType());
		StringBuffer sb = new StringBuffer("@NotFound@ @C_DocType_ID@");
		sb.append(" - @DocBaseType@ : " + docBaseTypeName);
		if (!Util.isEmpty(additionalInfo, true)) {
			sb.append(" (").append(additionalInfo).append(")");
		}
		return sb.toString();
	}

}
