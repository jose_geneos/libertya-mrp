package ar.com.geneos.mrp.plugin.exception;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.openXpertya.model.MProduct;
import org.openXpertya.model.X_AD_Workflow;
import org.openXpertya.util.Env;

public class CostInsufficientQtyException extends RuntimeException {

	/**
	 * Exception to Cost Insufficient Qty
	 * 
	 * @param product_id
	 *            Product
	 * @param asi_id
	 *            Attribute Set Instance
	 * @param qty
	 *            Quantity
	 * @param remainingQty
	 *            Remaining Qty
	 */
	public CostInsufficientQtyException(int product_id, int asi_id, BigDecimal qty, BigDecimal remainingQty) {
		super(createMessage(product_id, asi_id, qty, remainingQty));
	}

	/**
	 * Create Msg
	 * 
	 * @param product_id
	 *            Product
	 * @param asi_id
	 *            Attribute Set Instance
	 * @param qty
	 *            Quantity
	 * @param remainingQty
	 *            Remaining Qty
	 * @return String with the Msg
	 */
	private static String createMessage(int product_id, int asi_id, BigDecimal qty, BigDecimal remainingQty) {
		MProduct product = MProduct.get(Env.getCtx(), product_id);
		String productValue = product != null ? product.getValue() : "?";
		String productName = product != null ? product.getName() : "?";
		//
		return "@M_Product_ID@ : " + productValue + " - " + productName + ", @Qty@ : " + qty + ", @RemainingQty@ : " + remainingQty + " (ASI:" + asi_id + ")";
	}
}
