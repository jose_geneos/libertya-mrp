package ar.com.geneos.mrp.plugin.exception;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

import org.openXpertya.model.X_AD_Workflow;

public class RoutingExpiredException extends MRPException {

	/**
		 * 
		 */
	private static final long serialVersionUID = -7522979292063177848L;

	public RoutingExpiredException(X_AD_Workflow wf, Timestamp date) {
		super(buildMessage(wf, date));
	}

	private static final String buildMessage(X_AD_Workflow wf, Timestamp date) {
		return "@NotValid@ @AD_Workflow_ID@:" + wf.getValue() + " - @Date@:" + date;
	}

}
