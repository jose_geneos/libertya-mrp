package ar.com.geneos.mrp.plugin.exception;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.sql.Timestamp;

import ar.com.geneos.mrp.plugin.model.LP_PP_Product_BOM;

public class BOMExpiredException extends MRPException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3084324343550833077L;

	public BOMExpiredException(LP_PP_Product_BOM bom, Timestamp date) {
		super(buildMessage(bom, date));
	}

	private static final String buildMessage(LP_PP_Product_BOM bom, Timestamp date) {
		return "@NotValid@ @PP_Product_BOM_ID@:" + bom.getValue() + " - @Date@:" + date;
	}
}
