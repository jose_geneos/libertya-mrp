package ar.com.geneos.mrp.plugin.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MOrg extends LP_AD_Org {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MOrg(Properties ctx, int AD_Org_ID, String trxName) {
		super(ctx, AD_Org_ID, trxName);
	}

	public MOrg(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
