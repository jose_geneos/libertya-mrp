package ar.com.geneos.mrp.plugin.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openXpertya.model.PO;
import org.openXpertya.model.Query;
import org.openXpertya.model.X_AD_WF_Node;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.wf.MWorkflow;

public class MWFNode extends MPluginPO {

	public MWFNode(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */

	public MPluginStatusPO preBeforeSave(PO po, boolean success) {
		
		X_AD_WF_Node node = (X_AD_WF_Node) po;
		 
		int workflow_ID = node.getAD_Workflow_ID();
		
		MWorkflow workflow = new MWorkflow(this.m_ctx, workflow_ID, this.m_trx);
		
		if(workflow.getWorkflowType().equals("M")) {
			if(node.getS_Resource_ID() == 0) {
				status_po.setErrorMessage("Falta recurso en nodo del flujo de tipo manufactura");
				status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);				
			}
		}
			
		return status_po;
		
	}
	
	public MPluginStatusPO preBeforeDelete(PO po) {
		X_AD_WF_Node node = (X_AD_WF_Node) po;
		/*
		 * Si existen Ordenes de manufacturas ligadas a la actividad
		 * no se puede borrar. 
		 */
		List<Object> params = new ArrayList<Object>();
		final StringBuffer whereClause = new StringBuffer();
		whereClause.append("AD_Wf_Node_ID = ?");
		params.add(node.getID());
		Query q = new Query(node.getCtx(), MPPOrderNode.Table_Name, whereClause.toString(), node.get_TrxName());
		q.setParameters(params);
		List<MPPOrderNode> result = q.list();
		if(result.size() > 0) {
			status_po.setErrorMessage("No se puede eliminar porque existen Ordenes de manufactura ligadas a este registro");
			status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);
		}
		return status_po;
	}
}