package ar.com.geneos.mrp.plugin.model;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


public interface ModelValidator extends org.openXpertya.model.ModelValidator {

	/** Called before document is completed */
	public static final int TIMING_BEFORE_COMPLETE = 7;
	/** Called after document is prepared */
	public static final int TIMING_AFTER_PREPARE = 8;
	/** Called before document is close */
	public static final int TIMING_BEFORE_CLOSE = 3;
	/** Called before document is reactivate */
	public static final int TIMING_BEFORE_REACTIVATE = 4;
	/** Called after document is void */
	public static final int TIMING_AFTER_VOID = 10;
	/** Called before document is void */
	public static final int TIMING_BEFORE_VOID = 2;
	/** Called after document is closed */
	public static final int TIMING_AFTER_CLOSE = 11;
	/** Called after document is reactivated */
	public static final int TIMING_AFTER_REACTIVATE = 12;
	public static final int TIMING_BEFORE_REVERSEACCRUAL = 6;
	public static final int TIMING_AFTER_REVERSEACCRUAL = 14;
	public static final int TIMING_BEFORE_REVERSECORRECT = 5;
	public static final int TIMING_AFTER_REVERSECORRECT = 13;

	/** Model Change Type New */
	public static final int TYPE_BEFORE_NEW = 1;
	public static final int CHANGETYPE_NEW = 1;
	public static final int	TYPE_AFTER_NEW = 4;

	/** Model Change Type Change */
	public static final int TYPE_BEFORE_CHANGE = 2;
	public static final int CHANGETYPE_CHANGE = 2;
	public static final int	TYPE_AFTER_CHANGE = 5;

	/** Model Change Type Delete */
	public static final int TYPE_BEFORE_DELETE = 3;
	public static final int CHANGETYPE_DELETE = 3;
	public static final int	TYPE_AFTER_DELETE = 6;
}
