package ar.com.geneos.mrp.plugin.model;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.Properties;

import org.openXpertya.model.MProduct;
import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.CLogger;

public class MInOutLine extends MPluginPO {

	/** Logger */
	private static CLogger log = CLogger.getCLogger(MInOutLine.class);

	public MInOutLine(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */
	public MPluginStatusPO postBeforeSave(PO po, boolean newRecord) {
		org.openXpertya.model.MInOutLine inOutLine = (org.openXpertya.model.MInOutLine) po;

		if (inOutLine.getC_OrderLine_ID() != 0) {
			org.openXpertya.model.MOrderLine ol = new org.openXpertya.model.MOrderLine(inOutLine.getCtx(), inOutLine.getC_OrderLine_ID(),
					inOutLine.get_TrxName());
			if (ol.getM_Product_ID() != inOutLine.getM_Product_ID()) {
				/*
				MProduct prod = new MProduct(inOutLine.getCtx(), ol.getM_Product_ID(), inOutLine.get_TrxName());
				status_po.setErrorMessage("El producto debe coincidir con el ingresado en la linea de la orden (" + prod.getName()
						+ "), si desea agregar un producto distinto al remito por favor agregue una nueva linea al remito");
				status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);*/
				inOutLine.setC_OrderLine_ID(0);
			}
			
		}
		return status_po;
	}

}