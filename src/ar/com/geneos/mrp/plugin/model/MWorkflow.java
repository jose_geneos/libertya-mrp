package ar.com.geneos.mrp.plugin.model;


/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2007 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *                 Teo Sarca, www.arhipac.ro								  * 	
 *                 Pablo Velazquez pablo.velazquez@geneos.com.ar		 	  *                                 *
 *****************************************************************************/

import java.util.Properties;

import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.DB;
import org.openXpertya.wf.MWFNode;

public class MWorkflow extends MPluginPO {

	public MWorkflow(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
	}

	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO preBeforeSave(PO po, boolean success) {
		
		org.openXpertya.wf.MWorkflow workflow = (org.openXpertya.wf.MWorkflow) po;
		
		if(workflow.getWorkflowType().equals("M")) {
		
			MWFNode[] nodes = workflow.getNodes(true);
			
			for(int i=0; i<nodes.length; i++) {
				MWFNode node = nodes[i];
				if(node.getS_Resource_ID() == 0) {
					status_po.setErrorMessage("Falta recurso en nodo del flujo de tipo manufactura.");
					status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);
					return status_po;
				}			
			}
		}
		
		return status_po;
		
	}
	
	@Override
	public MPluginStatusPO postAfterSave(PO po, boolean newRecord, boolean success) {		
		org.openXpertya.wf.MWorkflow workflow = (org.openXpertya.wf.MWorkflow) po;
		/*
		 * Al guardar un Workflow actualizo las traducciones 
		 */
		int state = DB.executeUpdate("UPDATE AD_Workflow_Trl SET name = '"+ workflow.getName() +"' WHERE AD_Workflow_ID = "+workflow.getAD_Workflow_ID(), po.get_TrxName());
		if(state < 0) {
			status_po.setErrorMessage("Error al tratar de actualizar la traducción del flujo");
			status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);
		}
		return status_po;
		
	}

}