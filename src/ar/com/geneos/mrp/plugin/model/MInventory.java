package ar.com.geneos.mrp.plugin.model;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.math.BigDecimal;
import java.util.Properties;

import org.openXpertya.model.MInventoryLine;
import org.openXpertya.model.MStorage;
import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginDocAction;
import org.openXpertya.plugin.MPluginStatusDocAction;
import org.openXpertya.process.DocAction;
import org.openXpertya.util.CLogger;

import ar.com.geneos.mrp.plugin.util.MUMProduct;

public class MInventory extends MPluginDocAction {

	/** Logger */
	private static CLogger log = CLogger.getCLogger(MInventory.class);

	public MInventory(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
	}
	
	@Override
	public MPluginStatusDocAction preCompleteIt(DocAction document) {		
		org.openXpertya.model.MInventory inv = (org.openXpertya.model.MInventory) document;
		MInventoryLine[] lines = inv.getLines(true);
		boolean error = false;
		StringBuffer msg = new StringBuffer();
		
		for(MInventoryLine aLine : lines) {
			org.openXpertya.model.MProduct product = new org.openXpertya.model.MProduct(inv.getCtx(),aLine.getM_Product_ID(),inv.get_TrxName());
			if (aLine.getM_AttributeSetInstance_ID() == 0 && MUMProduct.isASIMandatory(product, false, inv.getAD_Org_ID())) {
				error = true;
				msg.append("Linea "+aLine.getLine()+", Producto: "+product.getValue()+" ==> Sin Conjunto de atributos, @M_AttributeSet_ID@ @IsMandatory@");
				msg.append("\n");
			}
			
			/* El chequeo solo lo hago cuando la cantidad es negativa
			 * porque es el unico que puede traer inconvenientes
			 */
			if(aLine.getQtyCount().compareTo(new BigDecimal(0)) == -1){
				//Intenta sobreescribir un inventario con valor negativo
				if(aLine.getInventoryType().equals(MInventoryLine.INVENTORYTYPE_OverwriteInventory)){
					error = true;
					msg.append("Linea "+aLine.getLine()+", Producto: "+product.getValue()+" ==> Genera un stock negativo");
					msg.append("\n");				
				} else {
					if(aLine.getInventoryType().equals(MInventoryLine.INVENTORYTYPE_InventoryDifference)){
						//¿Existe el almacenamiento?
						MStorage storage = MStorage.get(inv.getCtx(), aLine.getM_Locator_ID(), aLine.getM_Product_ID(), aLine.getM_AttributeSetInstance_ID(), inv.get_TrxName());						
				        if(storage == null) {
							error = true;
							msg.append("Linea "+aLine.getLine()+", Producto: "+product.getValue()+" ==> Genera un stock negativo");
							msg.append("\n");
				        } else {
				        	//¿La cantidad disponible es suficiente?
							BigDecimal qtyAvailable = storage.getQtyOnHand().add(storage.getQtyReserved());
							if (qtyAvailable.add(aLine.getQtyCount()).compareTo(new BigDecimal(0)) == -1) {
								error = true;
								msg.append("Linea "+aLine.getLine()+", Producto: "+product.getValue()+" ==> Disponible:"+qtyAvailable+", Necesario: "+aLine.getQtyCount());
								msg.append("\n");
							}	        	
				        }
					}
				}			
			}						
		}
		
		//Si hubo un error lo notifico
		if (error) {
			status_docAction.setContinueStatus(0);
			status_docAction.setDocStatus(org.openXpertya.model.MMovement.DOCSTATUS_Invalid);
			status_docAction.setProcessMsg("Ajuste Inválido:\n"+msg);
		}
		return status_docAction;
	}
}