/** Modelo Generado - NO CAMBIAR MANUALMENTE - Disytel */
package ar.com.geneos.mrp.plugin.model;
import org.openXpertya.model.*;
import java.util.logging.Level;
import java.util.*;
import java.sql.*;
import java.math.*;
import org.openXpertya.util.*;
/** Modelo Generado por M_Locator
 *  @author Comunidad de Desarrollo Libertya*         *Basado en Codigo Original Modificado, Revisado y Optimizado de:*         * Jorg Janke 
 *  @version  - 2017-06-13 13:00:57.286 */
public class LP_M_Locator extends org.openXpertya.model.MLocator
{
/** Constructor estándar */
public LP_M_Locator (Properties ctx, int M_Locator_ID, String trxName)
{
super (ctx, M_Locator_ID, trxName);
/** if (M_Locator_ID == 0)
{
setIsIssue (false);
}
 */
}
/** Load Constructor */
public LP_M_Locator (Properties ctx, ResultSet rs, String trxName)
{
super (ctx, rs, trxName);
}
public String toString()
{
StringBuffer sb = new StringBuffer ("LP_M_Locator[").append(getID()).append("]");
return sb.toString();
}
/** Set Is Issue */
public void setIsIssue (boolean IsIssue)
{
set_Value ("IsIssue", new Boolean(IsIssue));
}
/** Get Is Issue */
public boolean isIssue() 
{
Object oo = get_Value("IsIssue");
if (oo != null) 
{
 if (oo instanceof Boolean) return ((Boolean)oo).booleanValue();
 return "Y".equals(oo);
}
return false;
}
}
