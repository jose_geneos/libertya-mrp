/** Modelo Generado - NO CAMBIAR MANUALMENTE - Disytel */
package ar.com.geneos.mrp.plugin.model;
import org.openXpertya.model.*;
import java.util.logging.Level;
 import java.util.*;
import java.sql.*;
import java.math.*;
import org.openXpertya.util.*;
/** Modelo Generado por AD_Org
 *  @author Comunidad de Desarrollo Libertya*         *Basado en Codigo Original Modificado, Revisado y Optimizado de:*         * Jorg Janke 
 *  @version  - 2017-11-21 14:32:14.459 */
public class LP_AD_Org extends org.openXpertya.model.MOrg
{
/** Constructor estándar */
public LP_AD_Org (Properties ctx, int AD_Org_ID, String trxName)
{
super (ctx, AD_Org_ID, trxName);
/** if (AD_Org_ID == 0)
{
}
 */
}
/** Load Constructor */
public LP_AD_Org (Properties ctx, ResultSet rs, String trxName)
{
super (ctx, rs, trxName);
}
public String toString()
{
StringBuffer sb = new StringBuffer ("LP_AD_Org[").append(getID()).append("]");
return sb.toString();
}
/** Set reportlogo */
public void setreportlogo (byte[] reportlogo)
{
set_Value ("reportlogo", reportlogo);
}
/** Get reportlogo */
public byte[] getreportlogo() 
{
	if (get_Value("reportlogo") != null)
		return (byte[])get_Value("reportlogo");
	else
		return null;
}
}
