package ar.com.geneos.mrp.plugin.model;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.Properties;

import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.CLogger;

public class MOrder extends MPluginPO {

	/** Logger */
	private static CLogger log = CLogger.getCLogger(MOrder.class);

	public MOrder(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO postAfterSave(PO po, boolean newRecord, boolean success) {
		/*
	     *  Actualizacion de tabla PP_MRP
	     */
        if (newRecord)
			MRPValidator.modelChange(po, ModelValidator.TYPE_AFTER_NEW, log);
		else
			MRPValidator.modelChange(po, ModelValidator.TYPE_AFTER_CHANGE, log);
		return status_po;
	}
	
	@Override
	public MPluginStatusPO postBeforeDelete(PO po) {
	    /*
	     *  Actualizacion de tabla PP_MRP
	     */
	    MRPValidator.modelChange(po, ModelValidator.TYPE_BEFORE_DELETE, log);
		return status_po;
	}
	
	
	


}