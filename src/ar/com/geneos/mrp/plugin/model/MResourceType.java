/*
 *    El contenido de este fichero está sujeto a la  Licencia Pública openXpertya versión 1.1 (LPO)
 * en tanto en cuanto forme parte íntegra del total del producto denominado:  openXpertya, solución 
 * empresarial global , y siempre según los términos de dicha licencia LPO.
 *    Una copia  íntegra de dicha  licencia está incluida con todas  las fuentes del producto.
 *    Partes del código son CopyRight (c) 2002-2007 de Ingeniería Informática Integrada S.L., otras 
 * partes son  CopyRight (c) 2002-2007 de  Consultoría y  Soporte en  Redes y  Tecnologías  de  la
 * Información S.L.,  otras partes son  adaptadas, ampliadas,  traducidas, revisadas  y/o mejoradas
 * a partir de código original de  terceros, recogidos en el  ADDENDUM  A, sección 3 (A.3) de dicha
 * licencia  LPO,  y si dicho código es extraido como parte del total del producto, estará sujeto a
 * su respectiva licencia original.  
 *     Más información en http://www.openxpertya.org/ayuda/Licencia.html
 */



package ar.com.geneos.mrp.plugin.model;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.CLogger;
import org.openXpertya.util.DB;

/**
 * Descripción de Clase
 *
 *
 * @version    2.2, 12.10.07
 * @author     Equipo de Desarrollo de openXpertya    
 */

public class MResourceType extends MPluginPO {
	
	/** Logger */
	//private static CLogger log = CLogger.getCLogger(MResourceType.class);
    
    public MResourceType(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
	}

    public MPluginStatusPO preBeforeSave(PO po, boolean newRecord) { 
    	org.openXpertya.model.MResourceType rType = (org.openXpertya.model.MResourceType) po;
    	
    	if(rType.getM_Product_Category_ID() == 0) {
    		String sql = "SELECT M_Product_Category_ID FROM M_Product_Category WHERE IsDefault='Y'";
    		Integer id = DB.getSQLValue(rType.get_TrxName(), sql);  		
    		if(id == -1) { //Si no hay categoria predeterminada
    			status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);
    			status_po.setErrorMessage("No se pudo asignar categoría de producto porque no existe una predeterminada");
    			return status_po;
    		}
    		rType.setM_Product_Category_ID(id);
    		//Informar que se asigno la categoria de producto por defecto con un warning info
    	}	
    	return status_po;
    }  
    
}    // MResourceType



/*
 *  @(#)MResourceType.java   02.07.07
 * 
 *  Fin del fichero MResourceType.java
 *  
 *  Versión 2.2
 *
 */
