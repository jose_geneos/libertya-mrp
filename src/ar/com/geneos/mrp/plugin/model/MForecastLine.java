package ar.com.geneos.mrp.plugin.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.openXpertya.model.PO;
import org.openXpertya.model.Query;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.DB;


/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2007 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *                 Teo Sarca, www.arhipac.ro                                  *
 *****************************************************************************/



/**
 * PP Cost Collector Model
 *
 * @author victor.perez@e-evolution.com, e-Evolution http://www.e-evolution.com
 *         <li>Original contributor of Manufacturing Standard Cost <li>FR [
 *         2520591 ] Support multiples calendar for Org
 * @see http 
 *      ://sourceforge.net/tracker2/?func=detail&atid=879335&aid=2520591&group_id
 *      =176962
 * 
 * @author Teo Sarca, www.arhipac.ro
 * @version $Id: MPPCostCollector.java,v 1.1 2004/06/19 02:10:34 vpj-cd Exp $
 */
public class MForecastLine extends MPluginPO {


	private static final long serialVersionUID = 4495034527896093858L;

	public MForecastLine(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
	}
	
	/**
	 * Ejecución posterior al afterSave
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO postBeforeDelete(PO po) {
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		ar.com.geneos.mrp.plugin.model.LP_M_ForecastLine fl = (ar.com.geneos.mrp.plugin.model.LP_M_ForecastLine) po;
				
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * from PP_MRP where M_ForecastLine_ID = " + fl.getM_ForecastLine_ID());
		

		pstmt = DB.prepareStatement(sql.toString(), null);
		try {
			rs = pstmt.executeQuery();
			while (rs.next()) {
				DB.executeUpdate("DELETE FROM PP_MRP where M_ForecastLine_ID = " + fl.getM_ForecastLine_ID(), null);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status_po;
		
	}
	

	



} // MForecast
