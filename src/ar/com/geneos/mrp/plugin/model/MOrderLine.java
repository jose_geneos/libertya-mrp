package ar.com.geneos.mrp.plugin.model;

import java.math.BigDecimal;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.Properties;

import org.openXpertya.model.MRequisitionLine;
import org.openXpertya.model.PO;
import org.openXpertya.plugin.MPluginPO;
import org.openXpertya.plugin.MPluginStatusPO;
import org.openXpertya.util.CLogger;
import org.openXpertya.util.Env;

public class MOrderLine extends MPluginPO {

	/** Logger */
	private static CLogger log = CLogger.getCLogger(MOrderLine.class);

	public MOrderLine(PO po, Properties ctx, String trxName, String aPackage) {
		super(po, ctx, trxName, aPackage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO postAfterSave(PO po, boolean newRecord, boolean success) {
		if (newRecord)
			MRPValidator.modelChange(po, ModelValidator.TYPE_AFTER_NEW, log);
		else
			MRPValidator.modelChange(po, ModelValidator.TYPE_AFTER_CHANGE, log);
		return status_po;
	}

	/**
	 * Ejecución posterior al afterSave
	 * 
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO postBeforeDelete(PO po) {
		MRPValidator.modelChange(po, ModelValidator.TYPE_BEFORE_DELETE, log);

		org.openXpertya.model.MOrderLine ol = (org.openXpertya.model.MOrderLine) po;

		// Borro las referencias de la linea en las requisiciones

		if (!MRequisitionLine.unlinkC_OrderLine_ID(ol.getCtx(), ol.getC_OrderLine_ID(), ol.get_TrxName())) {
			status_po.setContinueStatus(MPluginStatusPO.STATE_FALSE);
			status_po.setErrorMessage("Error al borrar referencias en requisicion");
		}

		return status_po;
	}
	
	/**
	 * Ejecución posterior al beforeSave
	 * 
	 * @return estado del procesamiento
	 */
	@Override
	public MPluginStatusPO preBeforeSave(PO po, boolean newRecord) {

		org.openXpertya.model.MOrderLine ol = (org.openXpertya.model.MOrderLine) po;

		BigDecimal Discount = BigDecimal.ZERO;
		BigDecimal PriceList = ol.getPriceList();
		BigDecimal PriceEntered = ol.getPriceEntered();
		ol.setPriceActual(PriceEntered);
		BigDecimal QtyOrdered = ol.getQtyOrdered();
		
		// calculate Discount
        if( PriceList.intValue() == 0 ) {
            Discount = Env.ZERO;
        } else {
        	log.fine("pricelist = "+ PriceList.doubleValue()+ " - "+ "priceActual = "+PriceEntered.doubleValue()+" / "+" pricelist*100.0 ="+ PriceList.doubleValue() * 100.0);
            Discount = new BigDecimal(( PriceList.doubleValue() - PriceEntered.doubleValue()) / PriceList.doubleValue() * 100.0 );
        }

        if( Discount.scale() > 2 ) {
            Discount = Discount.setScale( 2,BigDecimal.ROUND_HALF_UP );
        }
        
        ol.setDiscount(Discount);
        System.out.println(Discount);
     // Line Net Amt
        BigDecimal LineNetAmt = QtyOrdered.multiply( PriceEntered );

        if( LineNetAmt.scale() > 2 ) {
            LineNetAmt = LineNetAmt.setScale( 2,BigDecimal.ROUND_HALF_UP );
        }

        ol.setLineNetAmt(LineNetAmt);
        System.out.println(LineNetAmt);
		return status_po;
	}

}