package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


public class MUColumnNames {

	public static final String COLUMNNAME_S_Resource_ID = "S_Resource_ID";
	public static final String COLUMNNAME_IsManufacturingResource = "IsManufacturingResource";
	public static final String COLUMNNAME_ManufacturingResourceType = "ManufacturingResourceType";
	public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";
	public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";
	public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";
	public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";
	public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";
	public static final String COLUMNNAME_C_AcctSchema_ID = "C_AcctSchema_ID";
	public static final String COLUMNNAME_M_CostElement_ID = "M_CostElement_ID";
	public static final String COLUMNNAME_M_CostType_ID = "M_CostType_ID";
	public static final String COLUMNNAME_C_Order_ID = "C_Order_ID";
	public static final String COLUMNNAME_DatePromised = "DatePromised";
	public static final String COLUMNNAME_DocStatus = "DocStatus";
	public static final String COLUMNNAME_DateOrdered = "DateOrdered";
	public static final String COLUMNNAME_QtyDelivered = "QtyDelivered";
	public static final String COLUMNNAME_DateRequired = "DateRequired";
	public static final String COLUMNNAME_C_BPartner_ID = "C_BPartner_ID";
	public static final String COLUMNNAME_QtyOrdered = "QtyOrdered";
	public static final String COLUMNNAME_Qty = "Qty";
	public static final String COLUMNNAME_C_OrderLine_ID = "C_OrderLine_ID";
	public static final String COLUMNNAME_IsActive = "IsActive";
	public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";
	public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";
	public static final String COLUMNNAME_Processed = "Processed";
	public static final String COLUMNNAME_LowLevel = "LowLevel";
	public static final String COLUMNNAME_QtyOnHand = "QtyOnHand";
	public static final String COLUMNNAME_PO_PriceList_ID = "PO_PriceList_ID";
	public static final Object COLUMNNAME_IsBOM = "IsBOM";
	public static final Object COLUMNNAME_IsPurchased = "IsPurchased";
	public static final Object COLUMNNAME_M_Product_Category_ID = "M_Product_Category_ID";
	public static final String COLUMNNAME_M_Requisition_ID = "M_Requisition_ID";
	public static final String COLUMNNAME_Line = "Line";
	public static final String COLUMNNAME_DocumentNo = "DocumentNos";
	public static final String COLUMNNAME_M_InOut_ID = "M_InOut_ID";
	public static final String COLUMNNAME_Value = "Value";
	public static final String COLUMNNAME_Name = "Name";
	public static final String COLUMNNAME_M_Forecast_ID = "M_Forecast_ID";
	public static final String COLUMNNAME_DateFrom = "DateFrom";
	public static final String COLUMNNAME_DateTo = "DateTo";
	public static final String COLUMNNAME_IsSOTrx = "IsSOTrx";
	public static final String COLUMNNAME_QtyReserved = "QtyReserved";
	public static final String COLUMNNAME_PP_Order_Workflow_ID = "PP_Order_Workflow_ID";
	public static final String COLUMNNAME_SeqNo = "SeqNo";
	public static final String COLUMNNAME_PP_Order_Node_ID = "PP_Order_Node_ID";
	


	
}
