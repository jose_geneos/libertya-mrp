package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import org.openXpertya.model.MResource;

import ar.com.geneos.mrp.plugin.model.LP_M_Resource;

public class MUMResource {


	public static int getPlanningHorizon(MResource plant) {
		LP_M_Resource aux = new LP_M_Resource(plant.getCtx(),plant.getID(),plant.get_TrxName());
		return aux.getPlanningHorizon();
	}
	
	public static int getC_UOM_ID(MResource plant)
	{
		return plant.getProduct().getC_UOM_ID();
	}

}
