package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;

import org.openXpertya.model.MBOM;
import org.openXpertya.model.MBOMProduct;
import org.openXpertya.model.MTable;
import org.openXpertya.model.Query;
import org.openXpertya.util.CLogger;
import org.openXpertya.util.DB;

public class MUMBOMProduct {

	protected static CLogger log = CLogger.getCLogger(MTable.class.getName());

	/**
	 * Grant independence to GenerateModel from AD_Table_ID
	 *
	 * @param String
	 *            tableName
	 * @return int retValue
	 * @author
	 */

	public static int getTable_ID(String tableName) {
		int retValue = 0;
		String SQL = "SELECT AD_Table_ID FROM AD_Table WHERE tablename = ?";
		try {
			PreparedStatement pstmt = DB.prepareStatement(SQL, null);
			pstmt.setString(1, tableName);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
				retValue = rs.getInt(1);
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			log.log(Level.SEVERE, SQL, e);
			retValue = -1;
		}
		return retValue;
	}

	/**
	 * Get Products of BOM
	 *
	 * @param bom
	 *            bom
	 * @return array of BOM Products
	 */
	public static MBOMProduct[] getOfBOM(MBOM bom) {
		// FR: [ 2214883 ] Remove SQL code and Replace for Query - red1
		String whereClause = "M_BOM_ID=?";
		List<MBOMProduct> list = new Query(bom.getCtx(), MBOMProduct.Table_Name, whereClause, bom.get_TrxName()).setParameters(bom.getM_BOM_ID())
				.setOrderBy("SeqNo").list();

		MBOMProduct[] retValue = new MBOMProduct[list.size()];
		list.toArray(retValue);
		return retValue;
	} // getOfProduct

}
