package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.openXpertya.util.CLogger;
import org.openXpertya.util.DB;
import org.openXpertya.util.DBException;

public class MUDB {

	private static CLogger log = CLogger.getCLogger(MUDB.class);

	/**
	 * Get Timestamp Value from sql
	 * 
	 * @param trxName
	 *            trx
	 * @param sql
	 *            sql
	 * @param params
	 *            array of parameters
	 * @return first value or null
	 * @throws DBException
	 *             if there is any SQLException
	 */
	public static Timestamp getSQLValueTSEx(String trxName, String sql, Object... params) {
		Timestamp retValue = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			DB.setParameters(pstmt, params);
			rs = pstmt.executeQuery();
			if (rs.next())
				retValue = rs.getTimestamp(1);
			else
				log.fine("No Value " + sql);
		} catch (SQLException e) {
			throw new DBException(e, sql);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return retValue;
	}

	/**
	 * Get int Value from sql
	 * 
	 * @param trxName
	 *            trx
	 * @param sql
	 *            sql
	 * @param params
	 *            array of parameters
	 * @return first value or -1 if not found or error
	 */
	public static int getSQLValue(String trxName, String sql, Object... params) {
		int retValue = -1;
		try {
			retValue = DB.getSQLValueEx(trxName, sql, params);
		} catch (Exception e) {
			log.log(Level.SEVERE, sql, DB.getSQLException(e));
		}
		return retValue;
	}

	/**
	 * Get BigDecimal Value from sql
	 * 
	 * @param trxName
	 *            trx
	 * @param sql
	 *            sql
	 * @param params
	 *            array of parameters
	 * @return first value or null if not found
	 * @throws DBException
	 *             if there is any SQLException
	 */
	public static BigDecimal getSQLValueBD(String trxName, String sql, Object... params) {
		BigDecimal retValue = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, trxName);
			DB.setParameters(pstmt, params);
			rs = pstmt.executeQuery();
			if (rs.next())
				retValue = rs.getBigDecimal(1);
			else
				log.fine("No Value " + sql);
		} catch (SQLException e) {
			// log.log(Level.SEVERE, sql, getSQLException(e));
			throw new DBException(e, sql);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		return retValue;
	}

}
