package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.openXpertya.wf.MWFNode;

public class MUMWFNode {

	/** WF Nodes */
	private List<MWFNode> m_nodes = new ArrayList<MWFNode>();

	/**
	 * Check if the workflow is valid for given date
	 * 
	 * @param date
	 * @return true if valid
	 */
	public static boolean isValidFromTo(MWFNode wfn, Timestamp date) {
		Timestamp validFrom = wfn.getValidFrom();
		Timestamp validTo = wfn.getValidTo();

		if (validFrom != null && date.before(validFrom))
			return false;
		if (validTo != null && date.after(validTo))
			return false;
		return true;
	}

	/**
	 * Get Workflow from Cache
	 *
	 * @param ctx
	 *            context
	 * @param AD_Workflow_ID
	 *            id
	 * @return workflow
	 */
	/*
	 * public static LP_M_WFNode get(Properties ctx, int AD_WFNode_ID) {
	 * LP_M_WFNode retValue = s_cache.get(AD_WFNode_ID); if (retValue != null)
	 * return retValue; retValue = new LP_M_WFNode(ctx, AD_WFNode_ID, null); if
	 * (retValue.getID() != 0) s_cache.put(AD_WFNode_ID, retValue); return
	 * retValue; } // get
	 */

}
