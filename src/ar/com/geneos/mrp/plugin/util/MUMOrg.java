package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.List;

import org.openXpertya.model.MOrg;
import org.openXpertya.model.PO;
import org.openXpertya.model.Query;

public class MUMOrg {

	private static final String COLUMNNAME_Value = "Value";

	/**
	 * Get Active Organizations Of Client
	 *
	 * @param po
	 *            persistent object
	 * @return array of orgs
	 */
	public static MOrg[] getOfClient(PO po) {
		List<MOrg> list = new Query(po.getCtx(), MOrg.Table_Name, "AD_Client_ID=?", null).setOrderBy(COLUMNNAME_Value).setOnlyActiveRecords(true)
				.setParameters(po.getAD_Client_ID()).list();
		return list.toArray(new MOrg[list.size()]);
	} // getOfClient

}
