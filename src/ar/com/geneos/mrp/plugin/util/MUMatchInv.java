package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.List;

import org.openXpertya.model.MMatchInv;
import org.openXpertya.model.Query;

import ar.com.geneos.mrp.plugin.model.LP_M_InOutLine;
import ar.com.geneos.mrp.plugin.model.LP_M_MatchInv;

public class MUMatchInv {

	/**
	 * get Match PO entity
	 * 
	 * @param ioLine
	 * @return
	 */
	
	//FIXME: Ver si se puede cambiar owner de la tabla para que funcione el Query
	public static List<LP_M_MatchInv> getInOutLine(LP_M_InOutLine ioLine) {
		return new Query(ioLine.getCtx(), MMatchInv.Table_Name, LP_M_InOutLine.COLUMNNAME_M_InOutLine_ID + "=?", ioLine.get_TrxName()).setClient_ID()
				.setParameters(ioLine.getM_InOutLine_ID()).list();
	}

}
