package ar.com.geneos.mrp.plugin.util;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import org.openXpertya.model.MOrder;
import org.openXpertya.model.MOrderLine;

import ar.com.geneos.mrp.plugin.model.LP_C_OrderLine;

public class MUMOrderLine {

	public static boolean isConsumesForecast(MOrderLine ol) {
		LP_C_OrderLine aux = new LP_C_OrderLine(ol.getCtx(), ol.getC_OrderLine_ID(), ol.get_TrxName());
		return aux.isConsumesForecast();
	}

	public static MOrder getParent(MOrderLine ol) {
		LP_C_OrderLine aux = new LP_C_OrderLine(ol.getCtx(), ol.getC_OrderLine_ID(), ol.get_TrxName());
		return aux.getParent();
	}
	
	public static int getPP_Cost_Collector_ID(MOrderLine ol) {
		LP_C_OrderLine aux = new LP_C_OrderLine(ol.getCtx(), ol.getC_OrderLine_ID(), ol.get_TrxName());
		return aux.getM_AttributeSetInstance_ID();
	}


}
