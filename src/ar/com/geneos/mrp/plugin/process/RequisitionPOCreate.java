package ar.com.geneos.mrp.plugin.process;


/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * Copyright (C) 2003-2007 e-Evolution,SC. All Rights Reserved.               *
 * Contributor(s): Victor Perez www.e-evolution.com                           *
 *                 Teo Sarca, www.arhipac.ro								  * 	
 *                 Pablo Velazquez pablo.velazquez@geneos.com.ar		 	  *                                 *
 *****************************************************************************/

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;


import org.openXpertya.grid.ed.Vedit_ReqToOC;
import org.openXpertya.model.MBPartner;
import org.openXpertya.model.MOrder;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MProductPO;
import org.openXpertya.model.MRequisition;
import org.openXpertya.model.MRequisitionLine;
import org.openXpertya.model.PO;
import org.openXpertya.model.POResultSet;
import org.openXpertya.model.Query;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.CLogger;
import org.openXpertya.util.DB;
import org.openXpertya.util.ValueNamePair;

import ar.com.geneos.mrp.plugin.model.LP_C_OrderLine;

/**
 * Create PO from Requisition
 *
 *
 * @author Jorg Janke
 * @version $Id: RequisitionPOCreate.java,v 1.2 2006/07/30 00:51:01 jjanke Exp $
 * 
 * @author Teo Sarca, www.arhipac.ro <li>BF [ 2609760 ] RequisitionPOCreate not
 *         using DateRequired <li>BF [ 2605888 ] CreatePOfromRequisition creates
 *         more PO than needed <li>BF [ 2811718 ] Create PO from Requsition
 *         without any parameter teminate in NPE
 *         http://sourceforge.net/tracker/?
 *         func=detail&atid=879332&aid=2811718&group_id=176962 <li>FR [ 2844074
 *         ] Requisition PO Create - more selection fields
 *         https://sourceforge.net
 *         /tracker/?func=detail&aid=2844074&group_id=176962&atid=879335
 */
public class RequisitionPOCreate extends SvrProcess {
	/** Org */
	private int p_AD_Org_ID = 0;
	/** Warehouse */
	private int p_M_Warehouse_ID = 0;
	/** Doc Date From */
	private Timestamp p_DateDoc_From;
	/** Doc Date To */
	private Timestamp p_DateDoc_To;
	/** Doc Date From */
	private Timestamp p_DateRequired_From;
	/** Doc Date To */
	private Timestamp p_DateRequired_To;
	/** Priority */
	private String p_PriorityRule = null;
	/** User */
	private int p_AD_User_ID = 0;
	/** Product */
	private int p_M_Product_ID = 0;
	/** Product Category */
	private int p_M_Product_Category_ID = 0;
	/** BPartner Group */
	private int p_C_BP_Group_ID = 0;
	/** BPartner Group */
	private int p_C_BPartner_ID = 0;
	/** Requisition */
	private int p_M_Requisition_ID = 0;

	/** Consolidate */
	private boolean p_ConsolidateDocument = false;

	/** Order */
	private MOrder m_order = null;
	/** Order Line */
	private LP_C_OrderLine m_orderLine = null;
	/** Orders Cache : (C_BPartner_ID, DateRequired, M_PriceList_ID) -> MOrder */
	private List<MOrder> m_cacheOrders = new ArrayList<MOrder>();
	
	public static boolean process_cancel = false;

	/**
	 * Prepare - e.g., get Parameters.
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("AD_Org_ID"))
				p_AD_Org_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("DateDoc")) {
				
				p_DateDoc_From = (Timestamp) para[i].getParameter();
				p_DateDoc_To = (Timestamp) para[i].getParameter_To();
				
				
				Calendar cal = Calendar.getInstance();
				// creates calendar 
				cal.setTime(p_DateDoc_To); 
				// sets calendar time/date 
				cal.add(Calendar.DATE, 1);
				cal.add(Calendar.SECOND, -1);
				// returns new date object, one hour in the future
				p_DateDoc_To = new Timestamp (cal.getTime().getTime());
				
				
				
			} else if (name.equals("DateRequired")) {
				p_DateRequired_From = (Timestamp) para[i].getParameter();
				
				p_DateRequired_To = (Timestamp) para[i].getParameter_To();
				
				Calendar cal = Calendar.getInstance();
				// creates calendar 
				cal.setTime(p_DateRequired_To); 
				// sets calendar time/date 
				cal.add(Calendar.DATE, 1);
				cal.add(Calendar.SECOND, -1);
				// returns new date object, one hour in the future
				p_DateRequired_To = new Timestamp (cal.getTime().getTime());
				
				
			} else if (name.equals("PriorityRule"))
				p_PriorityRule = (String) para[i].getParameter();
			else if (name.equals("AD_User_ID"))
				p_AD_User_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BP_Group_ID"))
				p_C_BP_Group_ID = para[i].getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();			
			else if (name.equals("M_Requisition_ID"))
				p_M_Requisition_ID = para[i].getParameterAsInt();
			else if (name.equals("ConsolidateDocument"))
				p_ConsolidateDocument = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	} // prepare
	
	/**
	 * Transforma una MRequisitionLine a un Vector<Object> que se usa en la tabla
	 * de la vista de reqiosion a Orden de compra
	 * @throws Exception 
	 **/
	public Vector<Object> reqLineToTableLine(MRequisitionLine line) throws Exception{
		Vector<Object> last = new Vector<Object>();
		Integer docNo = 0;
		String sql = ""+
				"SELECT documentNo " + 
				"FROM M_Requisition req " + 
				"INNER JOIN M_requisitionLine rl ON (rl.m_requisition_id = req.m_requisition_id) " + 
				"WHERE rl.M_RequisitionLine_ID = ? ";
		try {
            PreparedStatement pstmt = DB.prepareStatement(sql);
            pstmt.setInt(1, line.getM_RequisitionLine_ID());
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()) {
            	if (!(rs.getInt("documentNo") == 0)) {
            		docNo = rs.getInt("documentNo");
            	}            	
            }
            rs.close();
            pstmt.close();
        } catch( Exception e ) {
        	throw e;
        } 
		
		last.add(new Boolean(true));
		last.add(docNo);
		last.add(line.getQty().floatValue());//2
    	last.add(line.getProductName());
    	last.add(line.getProductProvider());
    	last.add(new Boolean(false));
    	//Columnas no visibles
    	last.add(line.getM_RequisitionLine_ID()); //6
    	last.add(line.getC_BPartner_ID());
    	last.add(line.getM_Product_ID());
    	last.add(line.getDateRequired());
    	last.add(line.getDescription());//10
    	last.add(line.getM_AttributeSetInstance_ID());
    	last.add(line.getC_Charge_ID());
    	last.add(line.getPriceActual());
    	last.add(line.getAD_Org_ID());//14
    	last.add(line.getM_Requisition_ID());
    	
    	return last;
	}
	
	/**
	 * Transforma un Vector<Object> -usado en la vista de requision a orden de compra-
	 * en una MRequisitionLne
	 **/
	public MRequisitionLine tableLineToReqLine(Vector<Object> v) {
		/*Integer ID = (Integer)v.elementAt(6);
		MRequisitionLine lineToProcess = null;
		if(ID != null && ID == 0) {
			lineToProcess = new MRequisitionLine(getCtx(), 0, get_TrxName());
		} else {
			lineToProcess = new MRequisitionLine(getCtx(), (Integer)v.elementAt(6), get_TrxName());
		}*/
//		MRequisitionLine lineToProcess = new MRequisitionLine(getCtx(), 0, get_TrxName());
		MRequisitionLine lineToProcess = new MRequisitionLine(getCtx(), (Integer)v.elementAt(6), get_TrxName());
		lineToProcess.setQty(new BigDecimal((Float)v.elementAt(2)));
		lineToProcess.setM_Product_ID((Integer)v.elementAt(8));
		lineToProcess.setC_BPartner_ID((Integer)v.elementAt(7));
//		lineToProcess.setM_RequisitionLine_ID((Integer)v.elementAt(6));
		lineToProcess.setDescription((String)v.elementAt(10));
		lineToProcess.setM_AttributeSetInstance_ID((Integer)v.elementAt(11));
		lineToProcess.setC_Charge_ID((Integer)v.elementAt(12));
		lineToProcess.setPriceActual((BigDecimal)v.elementAt(13));
		lineToProcess.setAD_Org_ID((Integer)v.elementAt(14));
		lineToProcess.setM_Requisition_ID((Integer)v.elementAt(15));
		return lineToProcess;
	}
	
	/**
	 * Busca en la base todas las MOrderLine que se generaron a partir de la MRequisitionLine
	 * y devuelve la suma de todas las cantidades ya ordenadas
	 * @throws Exception 
	 **/
	public BigDecimal getQuantityOrdered(MRequisitionLine req) throws Exception {
		String sql = "SELECT SUM(qtyOrdered) AS ordered FROM c_orderLine WHERE m_requisitionLine_ID = ?";		
		BigDecimal qtyOrdered = new BigDecimal(0);
		try {
            PreparedStatement pstmt = DB.prepareStatement(sql);
            pstmt.setInt(1, req.getM_RequisitionLine_ID());
            
            /*
    		 * descomentar el siguiente codigo si se desea visualizar en consola la consulta SQL 
    		 */
            //Object[] parameters = {req.getM_RequisitionLine_ID()};
    		//Swissknife.sqlToPrint(sql, parameters, "Consulta de getQuantityOrdered");
            
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()) {
            	if (!(rs.getBigDecimal("ordered") == null)) {
            		qtyOrdered = rs.getBigDecimal("ordered");
            	}            	
            }
            rs.close();
            pstmt.close();
        } catch( Exception e ) {
        	throw e;
        } 
		return qtyOrdered;
	}
	
	/**
	 * Process
	 *
	 * @return info
	 * @throws Exception
	 */
	protected String doIt() throws Exception {
		
		/* Matriz para la tabla visual. Caché */
		Vector<Vector<Object>> info = new Vector<Vector<Object>>(); 
				
		// Si la linea requisicion es solo una y ya existía
		if (p_M_Requisition_ID != 0) {
			log.info("M_Requisition_ID=" + p_M_Requisition_ID);
			MRequisition req = new MRequisition(getCtx(), p_M_Requisition_ID, get_TrxName());
			if (!MRequisition.DOCSTATUS_Completed.equals(req.getDocStatus())) {
				throw new IllegalStateException("@DocStatus@ = " + req.getDocStatus());
			}
			MRequisitionLine[] lines = req.getLines();
			for (int i = 0; i < lines.length; i++) {
				
				if (lines[i].getC_OrderLine_ID() == 0) {										
					/*
					 * Por cada linea de la requisicion cargo la caché que se usará en
					 * la tabla de la vista de modificacion de requisicion
					 */
					BigDecimal quantityOrdered = getQuantityOrdered(lines[i]);
					if(lines[i].getQty().compareTo(quantityOrdered) > 0) {
						lines[i].setQty(lines[i].getQty().subtract(quantityOrdered));
						info.add(i, reqLineToTableLine(lines[i]));
					} 
				}
			}
			
			/*
			 * Lanzo la ventana y quedo a la espera de que finalice 
			 */
			Vedit_ReqToOC.createAndShowGUI(info, this);		
			try {
				synchronized(this) {
				   this.wait();
				}
			} catch(Exception e) {
				throw e;
			}
			/*
			 * Si desde la ventana de modificacion de la requisicion cerraron con
			 * el boton X (cerrar ventana), entonces termino el proceso inmediatamente 
			 */
			if(process_cancel) {
				return "El proceso fué cancelado. Se descartan todos los cambios";
			}
			
			/*
			 * Chequeo que en la vista el usuario haya cargado las lineas correctamente 
			 */
			for(Vector<Object> v: info) {
				if((Boolean)v.elementAt(0)) {//Si esta marcada para ser procesada
					if(!checkFields(v)) {
						throw new Exception("Alguna de las lineas a procesar no está completa. Verifique cantidades y proveedores");
					}
				}
			}
			
			/*
			 * Por cada fila de la tabla, creo una linea de requisicion
			 * y la proceso. 
			 * Notar que no modifico la vieja linea de requisicion, sino que creo una nueva
			 */
			for(Vector<Object> v: info) {				
				if((Boolean)v.elementAt(0)) { //Solo se procesan las lineas Activas
					process(tableLineToReqLine(v));
				}
			}
			//closeOrder();
						
			return "OK";
		} // single Requisition

		// Si hay mas de una linea de requisicion
		log.info("AD_Org_ID=" + p_AD_Org_ID + ", M_Warehouse_ID=" + p_M_Warehouse_ID + ", DateDoc=" + p_DateDoc_From + "/" + p_DateDoc_To + ", DateRequired="
				+ p_DateRequired_From + "/" + p_DateRequired_To + ", PriorityRule=" + p_PriorityRule + ", AD_User_ID=" + p_AD_User_ID + ", M_Product_ID="
				+ p_M_Product_ID + ", ConsolidateDocument" + p_ConsolidateDocument);

		ArrayList<Object> params = new ArrayList<Object>();
		StringBuffer whereClause = new StringBuffer("C_OrderLine_ID IS NULL");
		if (p_AD_Org_ID > 0) {
			whereClause.append(" AND AD_Org_ID=?");
			params.add(p_AD_Org_ID);
		}
		if (p_M_Product_ID > 0) {
			whereClause.append(" AND M_Product_ID=?");
			params.add(p_M_Product_ID);
		} else if (p_M_Product_Category_ID > 0) {
			whereClause.append(" AND EXISTS (SELECT 1 FROM M_Product p WHERE M_RequisitionLine.M_Product_ID=p.M_Product_ID").append(
					" AND p.M_Product_Category_ID=?)");
			params.add(p_M_Product_Category_ID);
		}

		if (p_C_BP_Group_ID > 0) {
			whereClause.append(" AND (").append("M_RequisitionLine.C_BPartner_ID IS NULL")
					.append(" OR EXISTS (SELECT 1 FROM C_BPartner bp WHERE M_RequisitionLine.C_BPartner_ID=bp.C_BPartner_ID AND bp.C_BP_Group_ID=?)")
					.append(")");
			params.add(p_C_BP_Group_ID);
		}

		// Requisition Header
		whereClause.append(" AND EXISTS (SELECT 1 FROM M_Requisition r WHERE M_RequisitionLine.M_Requisition_ID=r.M_Requisition_ID").append(
				" AND r.DocStatus=?");
		params.add(MRequisition.DOCSTATUS_Completed);
		
		// Anexo de proveedor - p_C_BPartner_ID
		
		if (p_C_BPartner_ID > 0) {
			whereClause.append(" AND M_RequisitionLine.C_BPartner_ID=?");
			params.add(p_C_BPartner_ID);
		}
		
		if (p_M_Warehouse_ID > 0) {
			whereClause.append(" AND r.M_Warehouse_ID=?");
			params.add(p_M_Warehouse_ID);
		}
		if (p_DateDoc_From != null) {
			whereClause.append(" AND r.DateDoc >= ?");
			params.add(p_DateDoc_From);
		}
		if (p_DateDoc_To != null) {
			whereClause.append(" AND r.DateDoc <= ?");
			params.add(p_DateDoc_To);
		}
		if (p_DateRequired_From != null) {
			whereClause.append(" AND r.DateRequired >= ?");
			params.add(p_DateRequired_From);
		}
		if (p_DateRequired_To != null) {
			whereClause.append(" AND r.DateRequired <= ?");
			params.add(p_DateRequired_To);
		}
		if (p_PriorityRule != null) {
			whereClause.append(" AND r.PriorityRule >= ?");
			params.add(p_PriorityRule);
		}
		if (p_AD_User_ID > 0) {
			whereClause.append(" AND r.AD_User_ID=?");
			params.add(p_AD_User_ID);
		}
		whereClause.append(")"); // End Requisition Header
		
		// ORDER BY clause
		StringBuffer orderClause = new StringBuffer();
		if (!p_ConsolidateDocument) {
			orderClause.append("M_Requisition_ID, ");
		}
		orderClause.append("(SELECT DateRequired FROM M_Requisition r WHERE M_RequisitionLine.M_Requisition_ID=r.M_Requisition_ID),");
		orderClause.append("M_Product_ID, C_Charge_ID, M_AttributeSetInstance_ID");
		
		/*
		 * descomentar el siguiente codigo si se desea visualizar en consola la consulta SQL 
		 */
		//Swissknife.sqlToPrint("SELECT * FROM M_requisitionLine WHERE "+whereClause.toString()+" ORDER BY "+orderClause.toString(), params.toArray(), "Consulta Req -> OC");
		
		POResultSet<MRequisitionLine> rs = new Query(getCtx(), MRequisitionLine.Table_Name, whereClause.toString(), get_TrxName()).setParameters(params)
				.setOrderBy(orderClause.toString()).setClient_ID().scroll();
		int i=0;
		try {
			while (rs.hasNext()) {
				/*
				 * Por cada linea de la requisicion cargo la caché que se usará en
				 * la tabla de la vista de modificacion de requisicion				 
				 */	
				MRequisitionLine reqLine = rs.next();
				BigDecimal quantityOrdered = getQuantityOrdered(reqLine);
				if(reqLine.getQty().compareTo(quantityOrdered) > 0) {
					reqLine.setQty(reqLine.getQty().subtract(quantityOrdered));
					info.add(i, reqLineToTableLine(reqLine));
					i++;
				}        	
			}
		} catch (Exception e){
			throw e;
		}finally {
			rs.close();
			rs = null;
		}
		
		/*
		 * Lanzo la ventana y quedo a la espera de que finalice 
		 */
		Vedit_ReqToOC.createAndShowGUI(info, this);		
		try {
			synchronized(this) {
			   this.wait();
			}
		} catch(Exception e) {
			throw e;
		}
		/*
		 * Si desde la ventana de modificacion de la requisicion cerraron con
		 * el boton X (cerrar ventana), entonces termino el proceso inmediatamente 
		 */
		if(process_cancel) {
			throw new Exception("El proceso fué cancelado. Se descartan todos los cambios");
		}
		
		/*
		 * Chequeo que en la vista el usuario haya cargado las lineas correctamente 
		 */
		for(Vector<Object> v: info) {
			if((Boolean)v.elementAt(0)) {//Si esta marcada para ser procesada
				if(!checkFields(v)) {
					throw new Exception("Alguna de las lineas a procesar no está completa. Verifique cantidades y proveedores");
				}
			}
		}
		/*
		 * Por cada fila de la tabla caché, creo una linea de requisicion
		 * y la proceso. 
		 * Notar que no modifico la vieja linea de requisicion, sino que creo una nueva
		 */
		for(Vector<Object> v: info) {
			if((Boolean)v.elementAt(0)) { //Solo se procesan las lineas Activas
				process(tableLineToReqLine(v));
			}
		}
		
		//closeOrder();
		return "OK";
	} // doit

	/**
	 * Chequea que todos los campos modificables en la vista Vedit_ReqToOC
	 * esten cargados correctamente
	 **/
	private boolean checkFields(Vector<Object> v) {
		//Chequeo la cantidad
		if((Float)v.elementAt(2) != null && (Float)v.elementAt(2) <= 0) { 
			return false;
		}
		//Chequeo el partner
		if((Integer)v.elementAt(7) != null && (Integer)v.elementAt(7) <= 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Busca una orden de compra entre las creadas por el proceso, para realizar 
	 * la consolidación. Si la encuentra la devuelve, caso contrario devuelve null
	 * Se consolidarán aquellas órdenes cuyo Proveedor, fecha de entrega y precio de lista coincidan
	 **/
	private MOrder findOrder(int c_BPartner_ID, Timestamp dateRequired, int m_PriceList_ID) {
		for (MOrder order : m_cacheOrders) {
			if (order.getC_BPartner_ID() == c_BPartner_ID && order.getM_PriceList_ID() == m_PriceList_ID && order.getDatePromised().equals(dateRequired))
				return order;
		}
		return null;
	}
	
	/**
	 * Process Line
	 *
	 * @param rLine
	 *            request line
	 * @throws Exception
	 */
	private void process(MRequisitionLine rLine) throws Exception {
		/*
		 * Para eliminar las lineas generadas en la base de datos durante el testing 
		 * de la vista de creacion de ordenes de compra a partir de requisiciones
		 * 	
		 * delete from pp_mrp where created > timestamp '2017-12-25' OR updated > timestamp '2017-12-25';
		 * delete from c_orderline where created > timestamp '2017-12-25' OR updated > timestamp '2017-12-25';
		 * delete from c_order where created > timestamp '2017-12-25' OR updated > timestamp '2017-12-25';
		 */
		
		/*
		 * Busco o creo la orden 
		 */
		getOrder(rLine);
		
		/*
		 * Creo la orderLine correspondiente a la requisitionLine 
		 */
		createOrderLine(rLine);
		
		// No Order Line was produced (vendor was not valid/allowed) => SKIP
		if (m_orderLine == null) {
			return;
		}
		
//		rLine.setC_OrderLine_ID(m_orderLine.getC_OrderLine_ID()); // Asigno la ultia orderline usada
//		rLine.save();	
	} // process
	
	private MOrder getOrder(MRequisitionLine rLine) {
		/*
		 * Primero ubico en que Orden de compra debe ir la linea:
		 * Si hay que consolidar documentos y 
		 * existe una  orden en la base de datos, que este en estado borrador y coincida
		 * el proveedor, la fecha, y el precio de lista uso esa, sino creo una nueva.
		 * Las ordenes que voy creando o trayendo de la base, las voy guardando en m_cacheOrders
		 */
		m_order = null;
		if (p_ConsolidateDocument) {
			m_order = findOrder(rLine.getC_BPartner_ID(), rLine.getDateRequired(), rLine.getParent().getM_PriceList_ID());
			//Si no la encuentro entre las ordenes de la lista m_cacheOrders la busco en la base de datos
			if(m_order == null) { 
				List<Object> params = new ArrayList<Object>();
				final StringBuffer whereClause = new StringBuffer();
				whereClause.append("C_BPartner_ID = ?");
				params.add(rLine.getC_BPartner_ID());
				whereClause.append(" AND DatePromised BETWEEN ? AND ? ");
				Calendar cal = Calendar.getInstance();       
				cal.setTime(rLine.getDateRequired());                           
				cal.set(Calendar.HOUR_OF_DAY, 0);            
				cal.set(Calendar.MINUTE, 0);                 
				cal.set(Calendar.SECOND, 0);                 
				cal.set(Calendar.MILLISECOND, 0); 
				params.add(new Timestamp(cal.getTime().getTime()));
				cal.set(Calendar.HOUR_OF_DAY, 23);            
				cal.set(Calendar.MINUTE, 59);                 
				cal.set(Calendar.SECOND, 59);                 
				cal.set(Calendar.MILLISECOND, 0); 
				params.add(new Timestamp(cal.getTime().getTime()));
				whereClause.append(" AND M_PriceList_ID = ?");
				params.add(rLine.getParent().getM_PriceList_ID());
				whereClause.append(" AND docStatus = 'DR'");
				Query q = new Query(this.getCtx(), MOrder.Table_Name, whereClause.toString(), this.get_TrxName());
				q.setParameters(params);
				List<MOrder> result = q.list();
				if(result != null && result.size() > 0) {
					m_order = result.get(0); //Uso la primera que encuentro
					m_cacheOrders.add(m_order);
					addLog(0, null, null, "Orden "+m_order.getDocumentNo()+" actualizada");//Mensaje a mostrar en la ventana de resultado
				}
			}			
		} 
		
		/*
		 * En caso de no haber encontrado una orden donde colocar la linea
		 * ya sea porque no estaba en la base o no estaba en la m_cacheOrder; 
		 * Creo una nueva 
		 */
		if(m_order == null) {			
			MBPartner m_bpartner = new MBPartner(getCtx(), rLine.getC_BPartner_ID(), null);
			
			m_order = new MOrder(getCtx(), 0, get_TrxName());
			m_order.setAD_Org_ID(rLine.getAD_Org_ID());
			m_order.setM_Warehouse_ID(rLine.getParent().getM_Warehouse_ID());
			m_order.setDatePromised(rLine.getDateRequired());
			m_order.setIsSOTrx(false);
			m_order.setC_DocTypeTarget_ID();
			m_order.setBPartner(m_bpartner);
			m_order.setM_PriceList_ID(rLine.getParent().getM_PriceList_ID());				
			
			m_order.setDescription("Orden generada a partir del proceso CREAR OC DESDE REQUISICION:");
					
			m_order.save();
			m_cacheOrders.add(m_order);	
			addLog(0, null, null, "Orden "+m_order.getDocumentNo()+" creada");//Mensaje a mostrar en la ventana de resultado
		}
		return m_order;
	}
	
	private void createOrderLine(MRequisitionLine rLine) throws Exception {
				
		/*
		 * Codigo raro: estaba desde antes. No se que hace 
		 */
		MProduct product = MProduct.get(getCtx(), rLine.getM_Product_ID());
		// Get Business Partner
		int C_BPartner_ID = rLine.getC_BPartner_ID();
		if (C_BPartner_ID != 0) {
			;
		} else {
			// Find Strategic Vendor for Product
			MProductPO[] ppos = MProductPO.getOfProduct(getCtx(), product.getM_Product_ID(), null);
			for (int i = 0; i < ppos.length; i++) {
				if (ppos[i].isCurrentVendor() && ppos[i].getC_BPartner_ID() != 0) {
					C_BPartner_ID = ppos[i].getC_BPartner_ID();
					break;
				}
			}
			if (C_BPartner_ID == 0 && ppos.length > 0) {
				C_BPartner_ID = ppos[0].getC_BPartner_ID();
			}
			if (C_BPartner_ID == 0) {
				throw new RuntimeException(product.getName());
			}
		}
		if (!isGenerateForVendor(C_BPartner_ID)) {
			log.info("Skip for partner " + C_BPartner_ID);
			return;
		}//fin Codigo raro

		/*
		 * Busco si en la base existe una linea para la misma orden, mismo producto, misma fecha
		 * y misma requisicion. Si existe actualizo la cantidad ordenada
		 */
		m_orderLine = null;
		List<Object> params = new ArrayList<Object>();
		final StringBuffer whereClause = new StringBuffer();
		whereClause.append("C_Order_ID = ?");
		params.add(m_order.getID());
		whereClause.append(" AND M_Product_ID = ?");
		params.add(rLine.getM_Product_ID());
		whereClause.append(" AND DatePromised BETWEEN ? AND ? ");
		Calendar cal = Calendar.getInstance();       
		cal.setTime(rLine.getDateRequired());                           
		cal.set(Calendar.HOUR_OF_DAY, 0);            
		cal.set(Calendar.MINUTE, 0);                 
		cal.set(Calendar.SECOND, 0);                 
		cal.set(Calendar.MILLISECOND, 0); 
		params.add(new Timestamp(cal.getTime().getTime()));
		cal.set(Calendar.HOUR_OF_DAY, 23);            
		cal.set(Calendar.MINUTE, 59);                 
		cal.set(Calendar.SECOND, 59);                 
		cal.set(Calendar.MILLISECOND, 0); 
		params.add(new Timestamp(cal.getTime().getTime()));
		whereClause.append(" AND M_RequisitionLine_ID = ?");
		params.add(rLine.getM_RequisitionLine_ID());
		Query q = new Query(this.getCtx(), LP_C_OrderLine.Table_Name, whereClause.toString(), this.get_TrxName());
		q.setParameters(params);
		List<LP_C_OrderLine> result = q.list();
		if(result != null && result.size() > 0) {
			m_orderLine = new LP_C_OrderLine(getCtx(), ((PO)result.get(0)).getID(), get_TrxName());//Uso la primera que encuentro 
			m_orderLine.setQty(m_orderLine.getQtyOrdered().add(rLine.getQty()));
			addLog(0, null, null, "Linea "+m_orderLine.getLine()+" de la orden "+ m_order.getDocumentNo() +" actualizada ("+ m_orderLine.getQtyOrdered()+")");//Mensaje a mostrar en la ventana de resultado
		}
		
		/*
		 * Si la linea no existe en la base, la creo  
		 */
		if(m_orderLine == null) {
			m_orderLine = new LP_C_OrderLine(m_order); //Tiene el campo m_requisitionLine_ID
			m_orderLine.setDatePromised(rLine.getDateRequired());
			m_orderLine.setQty(rLine.getQty());
			m_orderLine.setAD_Org_ID(rLine.getAD_Org_ID());
			
			m_orderLine.setM_RequisitionLine_ID(rLine.getM_RequisitionLine_ID());
			if(m_orderLine.getM_RequisitionLine_ID() <= 0) {
				throw new Exception("Falló la asignación de Requisición en la linea de la orden de compra");
			}			
			
			m_orderLine.setDescription(""+rLine.getParent().getDocumentNo());
			if (product != null) {
				m_orderLine.setProduct(product);
				// Ticket 54 Mateo
				// Si la linea de requisición tiene descripción la copio sino copio la del producto
				if(rLine.getDescription() == null || rLine.getDescription() == "")
					m_orderLine.getDescription().concat(" - "+product.getDescription());
				else
					m_orderLine.getDescription().concat(" - "+rLine.getDescription());
				m_orderLine.setM_AttributeSetInstance_ID(rLine.getM_AttributeSetInstance_ID());
			} else {
				m_orderLine.setC_Charge_ID(rLine.getC_Charge_ID());
				m_orderLine.setPriceActual(rLine.getPriceActual());
			}		
		}
		
		m_orderLine.setAllowRepeatedProduct(true);
		// Guardo la linea
		if (!m_orderLine.save()){
			String msg = null;
			ValueNamePair err = CLogger.retrieveError();
			if (err != null)
				msg = err.getName();
			if (msg == null || msg.length() == 0)
				msg = "SaveError";
			addLog(0, null, new BigDecimal(-1).setScale(1), rLine + " | " + product+ " | " + msg);
		}
		
		//Actualizo la descripcion de la Orden agregando la requisicion en la descripcion
		MRequisition requisition = new MRequisition(getCtx(), rLine.getM_Requisition_ID(), this.get_TrxName());
		if(m_order.getDescription().endsWith(":")) {
			m_order.setDescription(m_order.getDescription()+" "+requisition.getDocumentNo());
			m_order.setPOReference(requisition.getDocumentNo());
		} else {			
			if(!m_order.getDescription().contains(requisition.getDocumentNo())) {
				m_order.setDescription(m_order.getDescription()+", "+requisition.getDocumentNo());
				m_order.setPOReference(", "+requisition.getDocumentNo());
			}
		}
		m_order.save();
	}

	/**
	 * Do we need to generate Purchase Orders for given Vendor
	 * 
	 * @param C_BPartner_ID
	 * @return true if it's allowed
	 */
	private boolean isGenerateForVendor(int C_BPartner_ID) {
		// No filter group was set => generate for all vendors
		if (p_C_BP_Group_ID <= 0)
			return true;

		if (m_excludedVendors.contains(C_BPartner_ID))
			return false;
		//
		boolean match = new Query(getCtx(), MBPartner.Table_Name, "C_BPartner_ID=? AND C_BP_Group_ID=?", get_TrxName()).setParameters(
				new Object[] { C_BPartner_ID, p_C_BP_Group_ID }).match();
		if (!match) {
			m_excludedVendors.add(C_BPartner_ID);
		}
		return match;
	}

	private List<Integer> m_excludedVendors = new ArrayList<Integer>();

} // RequisitionPOCreate
