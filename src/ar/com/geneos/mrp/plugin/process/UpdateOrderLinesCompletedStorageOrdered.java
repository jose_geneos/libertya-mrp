package ar.com.geneos.mrp.plugin.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;

import org.openXpertya.model.MOrderLine;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MStorage;
import org.openXpertya.model.MWarehouse;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;

/**
 * Actualizar cantidades ordenadas en Storage
 *
 *
 * @author Cooperativa Geneos
 * @version $Id: UpdateStorageReserved.java,v 1.0 2017/03/09
 * 
 * @author José Fantasia
 * 
 *         Actualización de cantidades ordenadas de artículos
 * 
 *         La cantidad ordenada de un articulo en el Storage se almacena en la
 *         ubicacion por defecto de cada almacen, sin partida asignada. La
 *         cantidad ordenada representa lo pendiente de ingresar a stock, ya sea
 *         por un pedido de compra o por una orden de manufactura
 * 
 */

public class UpdateOrderLinesCompletedStorageOrdered extends SvrProcess {

	protected int p_M_Warehouse_ID = 0;
	protected boolean p_FixQty = false;

	/**
	 * Prepare - e.g., get Parameters.
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("FixQty"))
				p_FixQty = ((String)para[i].getParameter() ).equals("Y");
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	} // prepare

	/**
	 * Process
	 *
	 * @return info
	 * @throws Exception
	 */
	protected String doIt() throws Exception {

		String msg = "";

		if (p_M_Warehouse_ID == 0)
			return "";

		MWarehouse wh = MWarehouse.get(getCtx(), p_M_Warehouse_ID);
		int locatorDefault_ID = wh.getDefaultLocator().getM_Locator_ID();
		int[] allProductIds = MProduct.getAllIDs(MProduct.Table_Name,
				"isPurchased = 'Y' and ad_org_id =" + Env.getAD_Org_ID(getCtx()), this.get_TrxName());
		int cOrderLine = 0;
		int cStorage = 0;
		int cStorageNotDefault = 0;

		for (int M_Product_ID : allProductIds) {
			/*
			 * Chequeo/Actualizacion de pendientes (cantidad reservada) en
			 * ordenes de compra
			 */

			int[] allCOrderLinesIds = MOrderLine.getAllIDs(MOrderLine.Table_Name,
					"m_product_id = " + M_Product_ID
							+ "and ( (qtyreserved != qtyordered - qtydelivered and qtyreserved <> 0) or (qtyreserved < 0) ) and c_order_id in (select c_order_id from c_order where docstatus = 'CO') AND M_Warehouse_ID = "+p_M_Warehouse_ID,
					get_TrxName());
			for (int C_OrderLine_ID : allCOrderLinesIds) {
				MOrderLine aOrderLine = new MOrderLine(getCtx(), C_OrderLine_ID, get_TrxName());
				BigDecimal qtyReserved = aOrderLine.getQtyOrdered().subtract(aOrderLine.getQtyDelivered());
				if (qtyReserved.signum() < 0)
					qtyReserved = BigDecimal.ZERO;
				cOrderLine++;
				log.severe("Cantidad pendiente de entrega erronea para linea: " + aOrderLine + ". Segun linea:"
						+ aOrderLine.getQtyReserved() + " , segun diferencia: " + qtyReserved);
				if (p_FixQty) {
					aOrderLine.setQtyReserved(qtyReserved);
					aOrderLine.save();
				}
			}

			/*
			 * Chequeo/Actulizacion de ordenados
			 */

			BigDecimal totalOrdered = Env.ZERO;

			PreparedStatement pstmt = null;
			String sql = "SELECT SUM(qtyreserved) FROM C_OrderLine" 
					+ " WHERE m_product_id = " + M_Product_ID
					+ " AND qtyreserved > 0"
					+ " AND M_Warehouse_ID = "+p_M_Warehouse_ID
					+ " AND c_order_id in (select c_order_id from c_order where docstatus = 'CO')";
			try {
				pstmt = DB.prepareStatement(sql.toString(), get_TrxName());

				ResultSet rs = pstmt.executeQuery();

				if (rs.next()) {
					totalOrdered = rs.getBigDecimal(1) == null ? BigDecimal.ZERO : rs.getBigDecimal(1);
				}

				rs.close();
				pstmt.close();
				pstmt = null;
			} catch (Exception e) {
				log.log(Level.SEVERE, "getSumReserved - " + sql, e);
			} finally {
				try {
					if (pstmt != null) {
						pstmt.close();
					}
				} catch (Exception e) {
				}

				pstmt = null;
			}

			List<MStorage> stList = MStorage.getAll(getCtx(), M_Product_ID, 0, 0, get_TrxName());

			// Default Storage
			MStorage defaultStorage = MStorage.getCreate(getCtx(), locatorDefault_ID, M_Product_ID, 0, get_TrxName());
			BigDecimal actualTotalOrdered = defaultStorage.getQtyOrdered();
			MProduct aProduct = MProduct.get(getCtx(), M_Product_ID);
			if (actualTotalOrdered.compareTo(totalOrdered.setScale(4, BigDecimal.ROUND_HALF_UP)) != 0) {
				cStorage++;
				log.severe("Cantidad ordenada erronea para producto: " + aProduct.getValue() + ". Segun ordenes:"
						+ totalOrdered + " , en storage (default): " + actualTotalOrdered);
			}
			if (p_FixQty) {
				// Todos los storages que no son el por defecto o tienen m_attributesetinstance != 0 deben tener
				// cantidad ordenada en 0
				for (MStorage aStorage : stList) {
					if (aStorage.getM_Warehouse_ID() == p_M_Warehouse_ID){
						if (aStorage.getM_Locator_ID() == locatorDefault_ID 
								&& aStorage.getM_AttributeSetInstance_ID() == 0)
							aStorage.setQtyOrdered(totalOrdered);
						else
							if (aStorage.getQtyOrdered().signum() == 1){
								cStorageNotDefault++;
								aStorage.setQtyOrdered(Env.ZERO);
							}
						aStorage.save();
					}
				}
			}
		}
		
		msg += "Pendientes de entrega en ordenes de compra erroneas: "+cOrderLine+" .Cantidades ordenadas erroneas: "+cStorage+", Storages no correspondientes con ordenados: "+cStorageNotDefault+". Para mas detalles por favor consulte el log.";
		return msg;
	} // doit

} // UpdateOrderLinesCompletedStorageOrdered
