package ar.com.geneos.mrp.plugin.process;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;

import org.openXpertya.model.MBOM;
import org.openXpertya.model.MBOMProduct;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MProductBOM;
import org.openXpertya.model.MQuery;
import org.openXpertya.model.MResource;
import org.openXpertya.model.MWarehouse;
import org.openXpertya.model.PrintInfo;
import org.openXpertya.model.X_C_Invoice;
import org.openXpertya.print.MPrintFormat;
import org.openXpertya.print.ReportEngine;
import org.openXpertya.print.Viewer;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;
import org.openXpertya.util.Language;
import org.openXpertya.util.Trx;

import ar.com.geneos.mrp.plugin.model.MPPProductBOM;
import ar.com.geneos.mrp.plugin.model.MPPProductBOMLine;
import ar.com.geneos.mrp.plugin.model.MPPProductPlanning;
import ar.com.geneos.mrp.plugin.util.MUMBOMProduct;

/**
 * 	Reporte de componentes de productos elaborados
 *	
 *  @author Jorg Janke
 *  @version $Id: BOMValidate.java,v 1.3 2006/07/30 00:51:01 jjanke Exp $
 */
public class RPT_Componentes extends SvrProcess
{
	/**	Recurso */
	private int		p_S_Resource_ID = 0;
	/**	Almacén */
	private int		p_M_Warehouse_ID = 0;
	/**	Producto */
	private int		p_M_Product_ID = 0;	
	/** Cantidad */
	private BigDecimal	p_Qty = Env.ZERO;
	
	/** Linea del Informe **/
	private int 	line = 0;
	
	/**	Product */
	private MProduct	m_product = null;
	/**	List of Products */
	private ArrayList<MProduct>		m_products = null;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("S_Resource_ID"))
				p_S_Resource_ID = para[i].getParameterAsInt();
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("Qty"))
				p_Qty = (BigDecimal) para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}	//	prepare

	/**
	 * 	Process
	 *	@return Info
	 *	@throws Exception
	 */
	protected String doIt() throws Exception
	{
		
		// delete all rows older than a week
		DB.executeUpdate("DELETE FROM T_RPTCOMPONENTES");
		
		String ret = ciclo(p_M_Product_ID, p_Qty, p_M_Warehouse_ID, p_S_Resource_ID, 0, Env.ZERO, line);
		
		Trx.getTrx(get_TrxName()).commit();
		
		Language language = Language.getLoginLanguage();    // Base Language
		
		// Get Format & Data
		
		MPrintFormat format = MPrintFormat.get( getCtx(),1011025,false );
        format.setLanguage( language );
        format.setTranslationLanguage( language );
        
        
        MQuery query = new MQuery("PP_RPTComponentes");
        
        
		// Engine

        PrintInfo info = new PrintInfo("Componentes",1010437,0);

        ReportEngine re = new ReportEngine(getCtx(),format,query,info);
        
        /* Ejemplo para agregar parametros a la cabecera pero sin que filtre */
        /*
        query.addRestriction("AD_ORG_ID", "=",Env.getAD_Org_ID(getCtx()), "infoName", "InfoDisplay");
        query.addRestriction("AD_ORG_ID", "=",Env.getAD_Org_ID(getCtx()), "infoName2", "InfoDisplay2");
        query.addRestriction("AD_ORG_ID", "=",Env.getAD_Org_ID(getCtx()), "infoName3", "InfoDisplay3");
        */
        
        
        new Viewer(re);
        
        return ret;
		
		
	}	//	doIt

	
	
	/**
	 * 	Ciclo de componentes de producto elaborado
	 *	@param Producto
	 *  @param Almacén
	 *  @param Recurso
	 *	@return Info
	 * @throws Exception 
	 */
	
	private String ciclo (int prod, BigDecimal cant, int alm, int rec, int prod_padre, BigDecimal cant_padre, int line) throws Exception {
		
		MProduct prod_inst = new MProduct(getCtx(), prod, get_TrxName());
		
		if (prod_inst.isBOM()) {
		
			MPPProductPlanning prod_planning = MPPProductPlanning.get(getCtx(), alm, rec, prod, get_TrxName(), true);
			
			if(prod_planning != null) {
				
				MPPProductBOM bom = prod_planning.getPP_Product_BOM();
				MPPProductBOMLine bomline[] = bom.getLines();
				MPPProductBOMLine bomline_item = null;
				BigDecimal cant_item = Env.ZERO;
				
				addLine(prod, cant, prod_padre, cant_padre, prod_inst.getC_UOM_ID());
				
				for(int ind=0;ind<bomline.length; ind++) {
					bomline_item = bomline[ind];
					cant_item = cant.multiply(bomline_item.getQty());
					ciclo (bomline_item.getM_Product_ID(),cant_item, alm, rec, prod, cant, line);
				}
				
			} else {
				// Existe producto sin planificación
				return "Producto sin planificación:  " + prod_inst.getName();
			}
			
			
		} else {
			
			addLine(prod, cant, prod_padre, cant_padre, prod_inst.getC_UOM_ID());
			
		}

		return "#" + line;
		
	}	//	validateProduct


	private void addLine(int prod, BigDecimal cant, int prod_padre, BigDecimal cant_padre, int uom) throws Exception {
		// TODO Auto-generated method stub
		
		StringBuffer sql = new StringBuffer();
		sql.append(" INSERT INTO T_RPTComponentes (ad_pinstance_id, ad_client_id, ad_org_id, m_product_id, m_product_qty, m_product_comp_id, m_product_comp_qty, c_uom_id)");
		sql.append(" VALUES ( ").append(getAD_PInstance_ID()).append(",")
								.append(getAD_Client_ID()).append(",")
								.append(getCtx().getProperty("#AD_Org_ID")).append(",")
								.append(prod_padre).append(",")
								.append(cant_padre).append(",")
								.append(prod).append(",")
								.append(cant).append(",")
								.append(uom).append(")");
		
		int no = DB.executeUpdate(sql.toString(), get_TrxName());
		if(no == 0) {
			throw new Exception("Error insertando datos en la tabla temporal");
		}
		
	}

	/**
	 * 	Validate Old BOM Product structure
	 *	@param product product
	 *	@return true if valid
	 */
	private boolean validateOldProduct (MProduct product)
	{
		if (!product.isBOM())
			return true;
		//
		if (m_products.contains(product))
		{
			log.warning (m_product.getName() + " recursively includes " + product.getName());
			return false;
		}
		m_products.add(product);
		log.fine(product.getName());
		//
		MProductBOM[] productsBOMs = MProductBOM.getBOMLines(product);
		for (int i = 0; i < productsBOMs.length; i++)
		{
			MProductBOM productsBOM = productsBOMs[i];
			MProduct pp = new MProduct(getCtx(), productsBOM.getM_ProductBOM_ID(), get_TrxName());
			if (!pp.isBOM())
				log.finer(pp.getName());
			else if (!validateOldProduct(pp))
				return false;
		}
		return true;
	}	//	validateOldProduct

	/**
	 * 	Validate BOM
	 *	@param bom bom
	 *	@return true if valid
	 */
	private boolean validateBOM (MBOM bom)
	{
		MBOMProduct[] BOMproducts = MUMBOMProduct.getOfBOM(bom);
		for (int i = 0; i < BOMproducts.length; i++)
		{
			MBOMProduct BOMproduct = BOMproducts[i];
			MProduct pp = new MProduct(getCtx(), BOMproduct.getM_BOMProduct_ID(), get_TrxName());
			if (pp.isBOM())
				return validateProduct(pp, bom.getBOMType(), bom.getBOMUse());
		}
		return true;
	}	//	validateBOM

	/**
	 * 	Validate Product
	 *	@param product product
	 *	@param BOMType type
	 *	@param BOMUse use
	 *	@return true if valid
	 */
	private boolean validateProduct (MProduct product, String BOMType, String BOMUse)
	{
		if (!product.isBOM())
			return true;
		//
		String restriction = "BOMType='" + BOMType + "' AND BOMUse='" + BOMUse + "'";
		MBOM[] boms = MBOM.getOfProduct(getCtx(), p_M_Product_ID, get_TrxName(),
			restriction);
		if (boms.length != 1)
		{
			log.warning(restriction + " - Length=" + boms.length);
			return false;
		}
		if (m_products.contains(product))
		{
			log.warning (m_product.getName() + " recursively includes " + product.getName());
			return false;
		}
		m_products.add(product);
		log.fine(product.getName());
		//
		MBOM bom = boms[0];
		MBOMProduct[] BOMproducts = MUMBOMProduct.getOfBOM(bom);
		for (int i = 0; i < BOMproducts.length; i++)
		{
			MBOMProduct BOMproduct = BOMproducts[i];
			MProduct pp = new MProduct(getCtx(), BOMproduct.getM_BOMProduct_ID(), get_TrxName());
			if (pp.isBOM())
				return validateProduct(pp, bom.getBOMType(), bom.getBOMUse());
		}
		return true;
	}	//	validateProduct
	
}	//	RPT_Componentes
