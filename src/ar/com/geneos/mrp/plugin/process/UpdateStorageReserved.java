package ar.com.geneos.mrp.plugin.process;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;

import org.openXpertya.model.MInvoice;
import org.openXpertya.model.MLocator;
import org.openXpertya.model.MOrderLine;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MStorage;
import org.openXpertya.model.MWarehouse;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.DB;
import org.openXpertya.util.Env;

import ar.com.geneos.mrp.plugin.model.MPPOrderBOMLine;
import ar.com.geneos.mrp.plugin.model.MPPOrderCost;

/**
 * Actualizar cantidad reservadas en Storage
 *
 *
 * @author Cooperativa Geneos
 * @version $Id: UpdateStorageReserved.java,v 1.0 2017/03/09
 * 
 * @author José Fantasia
 * 
 * Actualización de cantidades reservadas de artículos
 * 
 * 
 * 
 */

public class UpdateStorageReserved extends SvrProcess {
	
	protected int p_M_Warehouse_ID = 0;
	protected boolean p_FixQty = false;

	
	
	/**
	 * Prepare - e.g., get Parameters.
	 * 
	 * @PP_Order_ID define una OM en concreto, o si se deja en blanco todas las OM.
	 * @date aplica a todas las OM en el rango de fechas
	 * 
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para[i].getParameterAsInt();
			else if (name.equals("FixQty"))
				p_FixQty = ((String)para[i].getParameter() ).equals("Y");
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	} // prepare

	/**
	 * Process
	 *
	 * @return info
	 * @throws Exception
	 */
	protected String doIt() throws Exception {
		
		
		String msg = "";

		if (p_M_Warehouse_ID == 0)
			return "";

		MWarehouse wh = MWarehouse.get(getCtx(), p_M_Warehouse_ID);
		int locatorDefault_ID = wh.getDefaultLocator().getM_Locator_ID();
		int[] allProductIds = MProduct.getAllIDs(MProduct.Table_Name,
				"lowlevel <> 0 and ad_org_id =" + Env.getAD_Org_ID(getCtx()), this.get_TrxName());

		int cStorage = 0;
		int cStorageNegative = 0;
		int cStorageNotDefault = 0;

		for (int M_Product_ID : allProductIds) {
			

			/*
			 * Chequeo/Actulizacion de reservados
			 */

			BigDecimal totalReserved = Env.ZERO;

			PreparedStatement pstmt = null;
			String sql = "SELECT SUM(qtyreserved) FROM PP_Order_BOMLine" 
					+ " WHERE m_product_id = " + M_Product_ID
					//+ " AND qtyreserved > 0"
					+ " AND pp_order_id in (select pp_order_id from pp_order where docstatus = 'CO' AND M_Warehouse_ID = "+p_M_Warehouse_ID+")";
			try {
				pstmt = DB.prepareStatement(sql.toString(), get_TrxName());

				ResultSet rs = pstmt.executeQuery();

				if (rs.next()) {
					totalReserved = rs.getBigDecimal(1) == null ? BigDecimal.ZERO : rs.getBigDecimal(1);
				}

				rs.close();
				pstmt.close();
				pstmt = null;
			} catch (Exception e) {
				log.log(Level.SEVERE, "getSumReserved - " + sql, e);
			} finally {
				try {
					if (pstmt != null) {
						pstmt.close();
					}
				} catch (Exception e) {
				}

				pstmt = null;
			}

			List<MStorage> stList = MStorage.getAll(getCtx(), M_Product_ID, 0, 0, get_TrxName());

			// Default Storage
			MStorage defaultStorage = MStorage.getCreate(getCtx(), locatorDefault_ID, M_Product_ID, 0, get_TrxName());
			BigDecimal actualTotalReserved = defaultStorage.getQtyReserved();
			MProduct aProduct = MProduct.get(getCtx(), M_Product_ID);
			
			if (actualTotalReserved.compareTo(totalReserved.setScale(4, BigDecimal.ROUND_HALF_UP)) != 0) {
				cStorage++;
				log.severe("Cantidad reservada erronea para producto: " + aProduct.getValue() + ". Segun ordenes:"
						+ totalReserved + " , en storage (default): " + actualTotalReserved);
			}
			// Todos los storages que no son el por defecto y tenen partida != 0 deben tener
			// cantidad reservada en 0
			for (MStorage aStorage : stList) {
				if (aStorage.getM_Warehouse_ID() == p_M_Warehouse_ID){
					BigDecimal qtyReserved = BigDecimal.ZERO;
					if (aStorage.getM_Locator_ID() == locatorDefault_ID 
							&& aStorage.getM_AttributeSetInstance_ID() == 0)
						qtyReserved = totalReserved;
					else {
						if (aStorage.getQtyReserved().signum() == 1) {
							log.severe("Cantidad reservada en storage no correspondiente: " + aProduct.getValue() + " - "+aStorage);
							cStorageNotDefault++;
						}
					}

					if (aStorage.getQtyReserved().signum() == -1) {
						log.severe("Cantidad reservada negativa en storage: " + aProduct.getValue() + " - "+aStorage);
						cStorageNegative++;
					}
					if (p_FixQty) {
						aStorage.setQtyReserved(qtyReserved);
						aStorage.save();
					}
					
				}
			}
		}

		msg += "Cantidades reservadas erroneas: "+cStorage+", Storages con reservado negativo: "+cStorageNegative+", Storages no correspondientes con reservados: "+cStorageNotDefault+". Para mas detalles por favor consulte el log.";
		return msg;
	} // doit

	
} // UpdateStorageReserved
