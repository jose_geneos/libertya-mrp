package ar.com.geneos.mrp.plugin.process;

import org.openXpertya.process.DocAction;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;

import ar.com.geneos.mrp.plugin.model.MPPOrder;

public class ReOpenPPOrder extends SvrProcess {

	/* Orden de manufactura*/
	private Integer PP_Order_ID = null;
	
	public ReOpenPPOrder() {}

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();		
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if(para[i].getParameter() != null) {
				if(name.equalsIgnoreCase("PP_Order_ID")) {
					PP_Order_ID = para[i].getParameterAsInt();
	            	continue;
	        	}					
			} 
		}		
	}

	@Override
	protected String doIt() throws Exception {
		MPPOrder order = new MPPOrder(getCtx(), this.PP_Order_ID, get_TrxName());
		/* Chequeo que esté cerrado */
		if(!order.getDocStatus().equals(DocAction.ACTION_Close)) {
			throw new IllegalStateException("El documento no está cerrado.\n (@DocStatus@ = " + order.getDocStatus()+")");
		}		
		/* Cambio el estado de documento */
		order.setDocStatus(DocAction.STATUS_Completed);
		order.setDocAction(DocAction.ACTION_Close);
		order.save();
		return "Ok";
	}

}
