package ar.com.geneos.mrp.plugin.process;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.openXpertya.model.MAttributeSetInstance;
import org.openXpertya.model.MBOM;
import org.openXpertya.model.MBOMProduct;
import org.openXpertya.model.MForecastLine;
import org.openXpertya.model.MProduct;
import org.openXpertya.model.MProductBOM;
import org.openXpertya.model.MQuery;
import org.openXpertya.model.MResource;
import org.openXpertya.model.MStorage;
import org.openXpertya.model.MUOM;
import org.openXpertya.model.MWarehouse;
import org.openXpertya.model.PrintInfo;
import org.openXpertya.model.Query;
import org.openXpertya.model.X_C_Invoice;
import org.openXpertya.print.MPrintFormat;
import org.openXpertya.print.ReportEngine;
import org.openXpertya.print.Viewer;
import org.openXpertya.process.ProcessInfoParameter;
import org.openXpertya.process.SvrProcess;
import org.openXpertya.util.DB;
import org.openXpertya.util.DBException;
import org.openXpertya.util.Env;
import org.openXpertya.util.Language;
import org.openXpertya.util.Trx;
import org.openXpertya.wf.MWorkflow;

import ar.com.geneos.mrp.plugin.model.LP_PP_Cost_Collector;
import ar.com.geneos.mrp.plugin.model.MPPCostCollector;
import ar.com.geneos.mrp.plugin.model.MPPMRP;
import ar.com.geneos.mrp.plugin.model.MPPOrder;
import ar.com.geneos.mrp.plugin.model.MPPProductBOM;
import ar.com.geneos.mrp.plugin.model.MPPProductBOMLine;
import ar.com.geneos.mrp.plugin.model.MPPProductPlanning;
import ar.com.geneos.mrp.plugin.util.MUMBOMProduct;

/**
 * Reporte de trazabilidad
 * 
 * @author Jorg Janke
 * @version $Id: RPT_EstadoPronostico.java,v 1.0 2016/03/30 jjanke Exp $
 */
public class RPT_Trazabilidad extends SvrProcess {

	/** ID Product */
	private int p_M_Product_ID = 0;

	/** ID Attr */
	private int p_M_Attr_ID = 0;

	/** Product */
	private MProduct m_product = null;

	/** Desc Attr */
	private String p_M_Attr_Description = null;

	/** List of Products */
	private Hashtable<Integer, MProduct> m_products_exploded = new Hashtable<Integer, MProduct>();

	/**
	 * Prepare
	 */
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_ID"))
				p_M_Product_ID = para[i].getParameterAsInt();
			else if (name.equals("M_AttributeSetInstance_ID"))
				p_M_Attr_ID = para[i].getParameterAsInt();
			else if (name.equals("Description"))
				p_M_Attr_Description = (String) para[i].getParameter();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}

	} // prepare

	private int getMAttributeSetInstance() {
		// TODO Auto-generated method stub

		final StringBuffer whereClause = new StringBuffer();

		whereClause.append("description = '" + p_M_Attr_Description + "'");

		int ret = new Query(getCtx(), MAttributeSetInstance.Table_Name, whereClause.toString(), get_TrxName())
				.firstId();

		return ret;
	}

	/**
	 * Process
	 * 
	 * @return Info
	 * @throws Exception
	 */
	protected String doIt() throws Exception {

		if (p_M_Attr_Description != null && !p_M_Attr_Description.isEmpty()) {
			p_M_Attr_ID = getMAttributeSetInstance();
			if (p_M_Attr_ID == -1)
				throw new Exception("No se encontro ninguna partida con descripcion: " + p_M_Attr_Description);
		}

		DB.executeUpdate("DELETE FROM T_RPTTRAZABILIDAD");

		List<Integer> pp_orders = MPPCostCollector.getStartOrderTrx(getCtx(), null, p_M_Attr_ID);

		for (int PP_Order_id : pp_orders) {
			ciclo(PP_Order_id, p_M_Attr_ID);
		}

		Trx.getTrx(get_TrxName()).commit();

		Language language = Language.getLoginLanguage(); // Base Language

		// Get Format & Data

		MPrintFormat format = MPrintFormat.get(getCtx(), 1011031, false);
		format.setLanguage(language);
		format.setTranslationLanguage(language);

		MQuery query = new MQuery("PP_RPTTRAZABILIDAD");

		// Engine

		PrintInfo info = new PrintInfo("Informe de Trazabilidad", 1010443, 0);

		ReportEngine re = new ReportEngine(getCtx(), format, query, info);

		new Viewer(re);

		return "";

	} // doIt

	/**
	 * Ciclo de componentes de producto elaborado
	 * 
	 * @param Producto
	 * @param Almacén
	 * @param Recurso
	 * @return Info
	 * @throws Exception
	 */

	private String ciclo(int pp_Order_ID, int attr_ID) throws Exception {

		MPPOrder ord = new MPPOrder(getCtx(), pp_Order_ID, get_TrxName());

		List<Integer> pp_orders_line = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			MProduct prod_line = null;
			MUOM uom_line = null;
			MAttributeSetInstance attr_line = null;
			MWorkflow wf_line = null;
			String is_bom = "";

			// Recepciones
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT m_product_id, m_attributesetinstance_id, SUM(MovementQty), MIN(MovementDate)")
					.append(" FROM PP_Cost_collector").append(" WHERE pp_order_id = " + pp_Order_ID)
					.append(" AND m_product_id = " + ord.getM_Product_ID()).append(" AND costcollectortype = '100'")
					.append(" AND m_attributesetinstance_id = " + attr_ID)
					.append(" GROUP BY m_product_id, m_attributesetinstance_id");

			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = pstmt.executeQuery();

			/*
			 * List<MPPCostCollector> om_colectors_out =
			 * MPPCostCollector.getCostCollectorOM(getCtx(),
			 * ord.getM_Product_ID(), pp_Order_ID, get_TrxName(), "100");
			 */

			while (rs.next()) {
				prod_line = new MProduct(getCtx(), ord.getM_Product_ID(), get_TrxName());
				uom_line = new MUOM(getCtx(), prod_line.getC_UOM_ID(), get_TrxName());
				attr_line = new MAttributeSetInstance(getCtx(), attr_ID, get_TrxName());
				wf_line = new MWorkflow(getCtx(), ord.getAD_Workflow_ID(), get_TrxName());
				if (prod_line.isBOM())
					is_bom = "Y";
				// Solo cargo la partida si no fue cargada antes
				if (m_products_exploded.get(attr_line.getM_AttributeSetInstance_ID()) == null) {
					m_products_exploded.put(attr_line.getM_AttributeSetInstance_ID(), prod_line);
					addLine(prod_line.getM_Product_ID(), prod_line.getName(), attr_line.getM_AttributeSetInstance_ID(),
							attr_line.getDescription() == null ? " - " : attr_line.getDescription(),
							wf_line.getAD_Workflow_ID(), wf_line.getName(), ord.getPP_Order_ID(), ord.getDocumentNo(),
							uom_line.getC_UOM_ID(), uom_line.getName(), is_bom, rs.getBigDecimal(3),
							rs.getTimestamp(4).toString());
				}
			}

			// Recepcion de co-producto
			sql = new StringBuilder();
			sql.append("SELECT m_product_id, m_attributesetinstance_id, SUM(MovementQty), MIN(MovementDate)")
					.append(" FROM PP_Cost_collector").append(" WHERE pp_order_id = " + pp_Order_ID)
					.append(" AND costcollectortype = '105'")
					.append(" GROUP BY m_product_id, m_attributesetinstance_id");

			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				prod_line = new MProduct(getCtx(), rs.getInt(1), get_TrxName());
				uom_line = new MUOM(getCtx(), prod_line.getC_UOM_ID(), get_TrxName());
				attr_line = new MAttributeSetInstance(getCtx(), rs.getInt(2), get_TrxName());
				wf_line = new MWorkflow(getCtx(), ord.getAD_Workflow_ID(), get_TrxName());
				if (prod_line.isBOM())
					is_bom = "Y";

				// Solo cargo la partida si no fue cargada antes
				if (m_products_exploded.get(attr_line.getM_AttributeSetInstance_ID()) == null) {
					m_products_exploded.put(attr_line.getM_AttributeSetInstance_ID(), prod_line);
					addLine(prod_line.getM_Product_ID(), prod_line.getName(), attr_line.getM_AttributeSetInstance_ID(),
							attr_line.getDescription() == null ? " - " : attr_line.getDescription(),
							wf_line.getAD_Workflow_ID(), wf_line.getName(), ord.getPP_Order_ID(), ord.getDocumentNo(),
							uom_line.getC_UOM_ID(), uom_line.getName(), is_bom, rs.getBigDecimal(3),
							rs.getTimestamp(4).toString());
				}

			}

			// Surtimientos
			sql = new StringBuilder();
			sql.append("SELECT m_product_id, m_attributesetinstance_id, SUM(MovementQty), MIN(MovementDate)")
					.append(" FROM PP_Cost_collector").append(" WHERE pp_order_id = " + pp_Order_ID)
					.append(" AND costcollectortype = '110'")
					.append(" GROUP BY m_product_id, m_attributesetinstance_id");

			pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
			rs = pstmt.executeQuery();

			/*
			 * List<MPPCostCollector> om_colectors_in =
			 * MPPCostCollector.getCostCollectorOM(getCtx(), 0, pp_Order_ID,
			 * get_TrxName(), "110");
			 */

			while (rs.next()) {
				prod_line = new MProduct(getCtx(), rs.getInt(1), get_TrxName());
				uom_line = new MUOM(getCtx(), prod_line.getC_UOM_ID(), get_TrxName());
				attr_line = new MAttributeSetInstance(getCtx(), rs.getInt(2), get_TrxName());
				wf_line = new MWorkflow(getCtx(), ord.getAD_Workflow_ID(), get_TrxName());
				if (prod_line.isBOM())
					is_bom = "Y";

				// Solo cargo la partida si no fue cargada antes
				if (m_products_exploded.get(attr_line.getM_AttributeSetInstance_ID()) == null) {
					m_products_exploded.put(attr_line.getM_AttributeSetInstance_ID(), prod_line);
					addLine(prod_line.getM_Product_ID(), prod_line.getName(), attr_line.getM_AttributeSetInstance_ID(),
							attr_line.getDescription() == null ? " - " : attr_line.getDescription(),
							wf_line.getAD_Workflow_ID(), wf_line.getName(), ord.getPP_Order_ID(), ord.getDocumentNo(),
							uom_line.getC_UOM_ID(), uom_line.getName(), is_bom, rs.getBigDecimal(3),
							rs.getTimestamp(4).toString());

					// Solo exploto la partida si es manufacturado
					if (prod_line.isBOM()) {

						pp_orders_line = MPPCostCollector.getStartOrderTrx(getCtx(), null, rs.getInt(2));

						for (int PP_Order_id : pp_orders_line) {
							ciclo(PP_Order_id, rs.getInt(2));
						}
					}
				}
			}

		} // try
		catch (SQLException ex) {
			throw new DBException(ex);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}

		return "#";

	} // validateProduct

	private void addLine(int product_ID, String product_name, int attr_ID, String attr_name, int workflow_ID,
			String workflow_name, int order_ID, String order_nro, int uom_ID, String uom_name, String isBOM,
			BigDecimal movementqty, String movementdate) throws Exception {
		// TODO Auto-generated method stub

		StringBuffer sql = new StringBuffer();
		sql.append(
				" INSERT INTO T_RPTTRAZABILIDAD (ad_pinstance_id, ad_client_id, ad_org_id, m_product_id, product_name,"
						+ "m_attributesetinstance_id, m_attributesetinstance_name, ad_workflow_id, ad_workflow_name,"
						+ "pp_order_id,pp_order_name,c_uom_id,c_uom_name, isbom, movementqty, movementdate)");
		sql.append(" VALUES ( ").append(getAD_PInstance_ID()).append(",").append(getAD_Client_ID()).append(",")
				.append(getCtx().getProperty("#AD_Org_ID")).append(",").append(product_ID).append(",'")
				.append(product_name).append("',").append(attr_ID).append(",'").append(attr_name).append("',")
				.append(workflow_ID).append(",'").append(workflow_name).append("',").append(order_ID).append(",'")
				.append(order_nro).append("',").append(uom_ID).append(",'").append(uom_name).append("','").append(isBOM)
				.append("',").append(movementqty).append(",").append("to_date('" + movementdate + "', 'YYYY-MM-DD')")
				.append(")");

		int no = DB.executeUpdate(sql.toString(), get_TrxName());
		if (no == 0) {
			throw new Exception("Error insertando datos en la tabla temporal");
		}

	}

} // RPT_Componentes
