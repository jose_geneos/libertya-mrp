package ar.com.geneos.mrp.plugin.process;

import java.util.ArrayList;
import java.util.List;

import org.openXpertya.JasperReport.MJasperReport;
import org.openXpertya.model.MComponentVersion;
import org.openXpertya.model.MRole;
import org.openXpertya.model.MTree;
import org.openXpertya.model.MTree_Base;
import org.openXpertya.model.Query;
import org.openXpertya.process.PluginPostInstallProcess;
import org.openXpertya.utils.JarHelper;

public class PostInstallUpgradeFrom10 extends PluginPostInstallProcess {

	/** Reporte de existencia de inventario con saldo de stock */
	protected final static String StockProyectado_REPORT_JASPER_REPORT_UID = "PPA-AD_JasperReport-1010186";
	protected final static String StockProyectad_REPORT_JASPER_REPORT_FILENAME = "PPStockProyectado.jasper";
	
	/** Reporte de requisición de compra */
	protected final static String Requisition_REPORT_JASPER_REPORT_UID = "PPA-AD_JasperReport-1010206";
	protected final static String Requisition_REPORT_JASPER_REPORT_FILENAME = "rpt_requisicion_MateoLogo.jasper";
	
	/** Reporte de movimiento de inventario */
	protected final static String Movement_REPORT_JASPER_REPORT_UID = "PPA-AD_JasperReport-1010207";
	protected final static String Movement_REPORT_JASPER_REPORT_FILENAME = "movimiento_inventario.jasper";
	
	/** Reporte de consolidacion de OC a RR */
	protected final static String Consolidation_REPORT_JASPER_REPORT_UID = "PPA-AD_JasperReport-1010208";
	protected final static String Consolidation_REPORT_JASPER_REPORT_FILENAME = "consolidacion_oc_rr.jasper";
	
	protected String doIt() throws Exception {
		super.doIt();
	
		// Reporte de existencia de inventario con saldo de stock
		MJasperReport
			.updateBinaryData(
					get_TrxName(),
					getCtx(),
					StockProyectado_REPORT_JASPER_REPORT_UID,
					JarHelper
							.readBinaryFromJar(
									jarFileURL,
									getBinaryFileURL(StockProyectad_REPORT_JASPER_REPORT_FILENAME)));
		
		// Informe de impresión de Requisición de compra
		MJasperReport.updateBinaryData(
			get_TrxName(),
			getCtx(),
			Requisition_REPORT_JASPER_REPORT_UID,
			JarHelper.readBinaryFromJar(
				jarFileURL,
				getBinaryFileURL(Requisition_REPORT_JASPER_REPORT_FILENAME)));
		
		// Informe de impresión de Movimiento de Inventario
		MJasperReport.updateBinaryData(
			get_TrxName(),
			getCtx(),
			Movement_REPORT_JASPER_REPORT_UID,
			JarHelper.readBinaryFromJar(
				jarFileURL,
				getBinaryFileURL(Movement_REPORT_JASPER_REPORT_FILENAME)));
		
		// Informe de conolidación de OC en RR
		MJasperReport.updateBinaryData(
			get_TrxName(),
			getCtx(),
			Consolidation_REPORT_JASPER_REPORT_UID,
			JarHelper.readBinaryFromJar(
				jarFileURL,
				getBinaryFileURL(Consolidation_REPORT_JASPER_REPORT_FILENAME)));
		
		/* Creación del perfil manufactura */
//		StringBuffer whereClause = new StringBuffer();
//		whereClause.append("name = ?");
//		List<Object> params = new ArrayList<Object>();
//		params.add("Manufactura");
//		Query q = new Query(this.getCtx(), MRole.Table_Name, whereClause.toString(), this.get_TrxName());
//		q.setParameters(params);
//		MRole role = q.first();
//		if(role == null) {
//			role =  new MRole(getCtx(), 0, get_TrxName());
//			role.setName("Manufactura");
//			role.setUserLevel(" CO");
//			role.setC_Currency_ID(118);
//			role.setIsCanApproveOwnDoc(false);
//			role.setIsAccessAllOrgs(true);
//			role.setviewsalesprice(false);
//			role.setviewpurchaseprice(false);
//			role.setPOSJournalSupervisor(false);
//			role.setIsInfoBPartnerAccess(false);
//			role.setIsInfoScheduleAccess(false);
//			role.setIsInfoOrderAccess(false);
//			role.setIsInfoInvoiceAccess(false);
//			role.setIsInfoInOutAccess(false);
//			role.setIsInfoPaymentAccess(false);
//			role.setIsInfoCashLineAccess(false);
//			//role.setIsInfoAssignmentAccess(false);
//			role.setIsInfoAssetAccess(false);
//			role.setAllow_Info_Product(true);
//			role.setAddSecurityValidation_CreateFromShipment(false);
//			role.setAddSecurityValidation_OPRC_NC(false);
//			role.setAddSecurityValidation_POS_NC(false);	
//			
//			whereClause = new StringBuffer();
//			whereClause.append("AD_componentObjectUID = ?");
//			params.clear();
//			params.add("MRP-AD_Tree-1010163"); //UID del arbol de manufactura
//			q = new Query(this.getCtx(), MTree.Table_Name, whereClause.toString(), this.get_TrxName());
//			q.setParameters(params);
//			MTree_Base tree = q.first(); 			
//			role.setAD_Tree_Menu_ID(tree.getAD_Tree_ID());
//			
//			whereClause = new StringBuffer();
//			whereClause.append("ad_component_id = (select ad_component_id from ad_component where publicname  = 'PPAUXILIAR')");
//			params.clear();
//			q = new Query(this.getCtx(), MComponentVersion.Table_Name, whereClause.toString(), this.get_TrxName());
//			q.setParameters(params);
//			MComponentVersion cv = q.first();
//			role.setAD_ComponentVersion_ID(cv.getAD_ComponentVersion_ID());
//			
//			role.save();
//		}
		
		return "";
	}
}
