package ar.com.geneos.mrp.plugin.tool.engine;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/


import java.util.HashMap;
import java.util.Map;

import org.openXpertya.model.X_M_CostType;

import ar.com.geneos.mrp.plugin.model.LP_M_CostType;

/**
 * @author teo_sarca
 *
 */
public class CostingMethodFactory {
	private static final CostingMethodFactory s_instance = new CostingMethodFactory();

	public static CostingMethodFactory get() {
		return s_instance;
	}

	private static final Map<String, Class<? extends ICostingMethod>> s_map = new HashMap<String, Class<? extends ICostingMethod>>();
	static {
		s_map.put(LP_M_CostType.COSTINGMETHOD_Fifo, FifoLifoCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_Lifo, FifoLifoCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_AverageInvoice, AverageInvoiceCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_AveragePO, AveragePOCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_LastInvoice, LastInvoiceCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_LastPOPrice, LastPOPriceCostingMethod.class);
		s_map.put(LP_M_CostType.COSTINGMETHOD_StandardCosting, StandardCostingMethod.class);
	}

	private CostingMethodFactory() {
	}

	/**
	 * Get Costing method
	 * 
	 * @param ce
	 *            cost element
	 * @param costingMethod
	 *            costing method. Optional. If null, we get the costing method
	 *            from cost element
	 * @return costing method class instance
	 */
	public ICostingMethod getCostingMethod(String costingMethod) {
		Class<? extends ICostingMethod> cl = s_map.get(costingMethod);
		if (cl == null) {
			throw new IllegalStateException("No implementation found for costing method " + costingMethod);
		}
		ICostingMethod cm;
		try {
			cm = cl.newInstance();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		return cm;
	}
}
