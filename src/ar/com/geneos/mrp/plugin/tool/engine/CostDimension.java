package ar.com.geneos.mrp.plugin.tool.engine;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.util.ArrayList;
import java.util.Properties;

import org.openXpertya.model.*;
import org.openXpertya.util.Env;
import org.openXpertya.util.Util;

import ar.com.geneos.mrp.plugin.model.IDocumentLine;
import ar.com.geneos.mrp.plugin.model.LP_C_AcctSchema;
import ar.com.geneos.mrp.plugin.util.MUColumnNames;
import ar.com.geneos.mrp.plugin.util.MUMProduct;

/**
 * Immutable Cost Dimension
 * 
 * @author Teo Sarca
 */
public class CostDimension {
	public static final int ANY = -10;

	private int AD_Client_ID;
	private int AD_Org_ID;
	private int M_Warehouse_ID;
	private int M_Product_ID;
	private int S_Resource_ID;
	private int M_AttributeSetInstance_ID;
	private int M_CostType_ID;
	private int C_AcctSchema_ID;
	private int M_CostElement_ID;

	public CostDimension(MProduct product, MAcctSchema as, int M_CostType_ID, int AD_Org_ID, int M_Warehouse_ID, int M_ASI_ID, int M_CostElement_ID) {
		this.AD_Client_ID = as.getAD_Client_ID();
		this.AD_Org_ID = AD_Org_ID;
		this.M_Warehouse_ID = M_Warehouse_ID;
		this.M_Product_ID = product != null ? product.getID() : ANY;
		this.M_AttributeSetInstance_ID = M_ASI_ID;
		this.M_CostType_ID = M_CostType_ID;
		this.C_AcctSchema_ID = as.getID();
		this.M_CostElement_ID = M_CostElement_ID;
		updateForProduct(product, as);
	}

	public CostDimension(int client_ID, int org_ID, int warehouse_ID, int product_ID, int attributeSetInstance_ID, int costType_ID, int acctSchema_ID,
			int costElement_ID) {
		this.AD_Client_ID = client_ID;
		this.AD_Org_ID = org_ID;
		this.M_Warehouse_ID = warehouse_ID;
		this.M_Product_ID = product_ID;
		this.M_AttributeSetInstance_ID = attributeSetInstance_ID;
		this.M_CostType_ID = costType_ID;
		this.C_AcctSchema_ID = acctSchema_ID;
		this.M_CostElement_ID = costElement_ID;
		//
		updateForProduct(null, null);
	}

	/**
	 * Copy Constructor
	 *
	 * @param costDimension
	 *            a <code>CostDimension</code> object
	 */
	public CostDimension(CostDimension costDimension) {
		this.AD_Client_ID = costDimension.AD_Client_ID;
		this.AD_Org_ID = costDimension.AD_Org_ID;
		this.M_Warehouse_ID = costDimension.M_Warehouse_ID;
		this.M_Product_ID = costDimension.M_Product_ID;
		this.M_AttributeSetInstance_ID = costDimension.M_AttributeSetInstance_ID;
		this.M_CostType_ID = costDimension.M_CostType_ID;
		this.C_AcctSchema_ID = costDimension.C_AcctSchema_ID;
		this.M_CostElement_ID = costDimension.M_CostElement_ID;
	}

	private Properties getCtx() {
		return Env.getCtx(); // TODO
	}

	private void updateForProduct(MProduct product, MAcctSchema as) {
		if (product == null) {
			product = MProduct.get(getCtx(), this.M_Product_ID);
		}
		if (product == null) {
			// incomplete specified dimension [SKIP]
			return;
		}
		if (as == null) {
			as = MAcctSchema.get(getCtx(), this.C_AcctSchema_ID);
		}

		/*
		 * Migración Libero Cambio en parámetros se elimina AD_Org_ID
		 * 
		 * @autor pepo
		 */

		String CostingLevel = MUMProduct.getCostingLevel(product, as);
		//
		if (LP_C_AcctSchema.COSTINGLEVEL_Client.equals(CostingLevel)) {
			AD_Org_ID = 0;
			M_AttributeSetInstance_ID = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Organization.equals(CostingLevel)) {
			M_AttributeSetInstance_ID = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Warehouse.equals(CostingLevel)) {
			M_Warehouse_ID = 0;
			M_AttributeSetInstance_ID = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_BatchLot.equals(CostingLevel)) {
			AD_Org_ID = 0;
		}
		//
		this.S_Resource_ID = product.getS_Resource_ID();
	}

	/**
	 * @return the aD_Client_ID
	 */
	public int getAD_Client_ID() {
		return AD_Client_ID;
	}

	/**
	 * @return the aD_Org_ID
	 */
	public int getAD_Org_ID() {
		return AD_Org_ID;
	}

	/**
	 * @return the aD_Org_ID
	 */
	public int getM_Warehouse_ID() {
		return M_Warehouse_ID;
	}

	/**
	 * @return the m_Product_ID
	 */
	public int getM_Product_ID() {
		return M_Product_ID;
	}

	public int getS_Resource_ID() {
		return S_Resource_ID;
	}

	public CostDimension setM_Product_ID(int M_Product_ID) {
		CostDimension d = new CostDimension(this);
		d.M_Product_ID = M_Product_ID;
		d.updateForProduct(null, null);
		//
		return d;
	}

	public CostDimension setM_Product(MProduct product) {
		CostDimension d = new CostDimension(this);
		d.M_Product_ID = product.getID();
		d.updateForProduct(product, null);
		return d;
	}

	/**
	 * @return the M_AttributeSetInstance_ID
	 */
	public int getM_AttributeSetInstance_ID() {
		return M_AttributeSetInstance_ID;
	}

	/**
	 * @return the m_CostType_ID
	 */
	public int getM_CostType_ID() {
		return M_CostType_ID;
	}

	/**
	 * @return the c_AcctSchema_ID
	 */
	public int getC_AcctSchema_ID() {
		return C_AcctSchema_ID;
	}

	/**
	 * @return the m_CostElement_ID
	 */
	public int getM_CostElement_ID() {
		return M_CostElement_ID;
	}

	public Query toQuery(Class<?> clazz, String trxName) {
		return toQuery(clazz, null, null, trxName);
	}

	public Query toQuery(Class<?> clazz, String whereClause, Object[] params, String trxName) {
		String tableName;
		// Get Table_Name by Class
		// TODO: refactor
		try {
			tableName = (String) clazz.getField("Table_Name").get(null);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		//
		Properties ctx = Env.getCtx();

		M_Table table = M_Table.get(ctx, tableName);

		ArrayList<Object> finalParams = new ArrayList<Object>();
		StringBuffer finalWhereClause = new StringBuffer();

		finalWhereClause.append(MUColumnNames.COLUMNNAME_AD_Client_ID + "=? ");
		finalParams.add(this.AD_Client_ID);
		finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_AD_Org_ID + "=?");
		finalParams.add(this.AD_Org_ID);
		finalWhereClause.append(" AND (").append(MUColumnNames.COLUMNNAME_M_Warehouse_ID).append(" IS NULL OR ")
				.append(MUColumnNames.COLUMNNAME_M_Warehouse_ID + "=? )");
		finalParams.add(this.M_Warehouse_ID);
		finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_M_Product_ID + "=?");
		finalParams.add(this.M_Product_ID);
		finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_M_AttributeSetInstance_ID + "=?");
		finalParams.add(this.M_AttributeSetInstance_ID);
		finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_C_AcctSchema_ID + "=?");
		finalParams.add(this.C_AcctSchema_ID);
		if (this.M_CostElement_ID != ANY) {
			finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_M_CostElement_ID + "=?");
			finalParams.add(this.M_CostElement_ID);
		}

		if (this.M_CostType_ID != ANY && table.getColumn(MUColumnNames.COLUMNNAME_M_CostType_ID) != null) {
			finalWhereClause.append(" AND " + MUColumnNames.COLUMNNAME_M_CostType_ID + "=?");
			finalParams.add(this.M_CostType_ID);
		}
		if (!Util.isEmpty(whereClause, true)) {
			finalWhereClause.append(" AND (").append(whereClause).append(")");
			if (params != null && params.length > 0) {
				for (Object p : params) {
					finalParams.add(p);
				}
			}
		}

		return new Query(ctx, tableName, finalWhereClause.toString(), trxName).setParameters(finalParams);
	}

	@Override
	protected Object clone() {
		return new CostDimension(this);
	}

	@Override
	public String toString() {
		final String TAB = ";";

		String retValue = "";

		retValue = "CostDimension{" + "AD_Client_ID = " + this.AD_Client_ID + TAB + "AD_Org_ID = " + this.AD_Org_ID + TAB + "M_Warehouse_ID = "
				+ this.M_Warehouse_ID + TAB + "M_Product_ID = " + this.M_Product_ID + TAB + "M_AttributeSetInstance_ID = " + this.M_AttributeSetInstance_ID
				+ TAB + "M_CostType_ID = " + this.M_CostType_ID + TAB + "C_AcctSchema_ID = " + this.C_AcctSchema_ID + TAB + "M_CostElement_ID = "
				+ this.M_CostElement_ID + TAB + "}";

		return retValue;
	}

	public static boolean isSameCostDimension(MAcctSchema as, MTransaction trxFrom, MTransaction trxTo) {
		if (trxFrom.getM_Product_ID() != trxTo.getM_Product_ID()) {
			throw new IllegalStateException("Same product is needed - " + trxFrom + ", " + trxTo);
		}
		MProduct product = MProduct.get(trxFrom.getCtx(), trxFrom.getM_Product_ID());

		// String CostingLevel = product.getCostingLevel(as,
		// trxFrom.getAD_Org_ID());
		String CostingLevel = MUMProduct.getCostingLevel(product, as);

		MLocator locatorFrom = MLocator.get(trxFrom.getCtx(), trxFrom.getM_Locator_ID());
		MLocator locatorTo = MLocator.get(trxTo.getCtx(), trxTo.getM_Locator_ID());
		int Org_ID = locatorFrom.getAD_Org_ID();
		int Org_ID_To = locatorTo.getAD_Org_ID();
		int ASI_ID = trxFrom.getM_AttributeSetInstance_ID();
		int ASI_ID_To = trxTo.getM_AttributeSetInstance_ID();
		if (LP_C_AcctSchema.COSTINGLEVEL_Client.equals(CostingLevel)) {
			Org_ID = 0;
			Org_ID_To = 0;
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Organization.equals(CostingLevel)) {
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Warehouse.equals(CostingLevel)) {
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_BatchLot.equals(CostingLevel)) {
			Org_ID = 0;
			Org_ID_To = 0;
		}
		//
		return Org_ID == Org_ID_To && ASI_ID == ASI_ID_To;
	}

	public static boolean isSameCostDimension(MAcctSchema as, IDocumentLine model) {
		MProduct product = MProduct.get(model.getCtx(), model.getM_Product_ID());
		MLocator locator = MLocator.get(model.getCtx(), model.getM_LocatorTo_ID());

		// String CostingLevel = product.getCostingLevel(as,
		// model.getAD_Org_ID());
		String CostingLevel = MUMProduct.getCostingLevel(product, as);

		int Org_ID = model.getAD_Org_ID();
		int Org_ID_To = locator.getAD_Org_ID();
		int ASI_ID = model.getM_AttributeSetInstance_ID();
		int ASI_ID_To = model.getM_AttributeSetInstance_ID();
		if (LP_C_AcctSchema.COSTINGLEVEL_Client.equals(CostingLevel)) {
			Org_ID = 0;
			Org_ID_To = 0;
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Organization.equals(CostingLevel)) {
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_Warehouse.equals(CostingLevel)) {
			ASI_ID = 0;
			ASI_ID_To = 0;
		} else if (LP_C_AcctSchema.COSTINGLEVEL_BatchLot.equals(CostingLevel)) {
			Org_ID = 0;
			Org_ID_To = 0;
		}
		//
		return Org_ID == Org_ID_To && ASI_ID == ASI_ID_To;
	}

	public String getCostingMethod() {
		MCostType ct = new MCostType(Env.getCtx(), getM_CostType_ID(), null);
		return ct.getCostingMethod();
	}
}
