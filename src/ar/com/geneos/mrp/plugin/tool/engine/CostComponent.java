package ar.com.geneos.mrp.plugin.tool.engine;

/********************************************************************************************
** Libertya MRP
** Módulo de Gestión de la producción para Libertya ERP. 

 This program is free software: you can redistribute it and/or modify it under the terms 
 of the GNU General Public License as published by the Free Software Foundation, either 
 version 3 of the License, or (at your option) any later version. This program is 
 distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 General Public License for more details. You should have received a copy of the GNU General 
 Public Licensealong with this program. If not, see <http://www.gnu.org/licenses/>.

** Copyright (C) 2016 Cooperativa de Trabajo Geneos Ltda.
** Contributor(s)
** Pablo Velazquez - pablo.velazquez@geneos.com.ar
** José Maria Fantasia - jose.fantasia@geneos.com.ar
**
********************************************************************************************/

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.openXpertya.util.Env;

/**
 * @author Teo Sarca
 *
 */
public class CostComponent
{
	public BigDecimal qty = Env.ZERO;
	public BigDecimal priceActual = Env.ZERO;
	
	private int scale = 4;
	private BigDecimal percent = Env.ZERO;

	public CostComponent(BigDecimal qty, BigDecimal priceActual)
	{
		this.qty = qty;
		this.priceActual = priceActual;
	}
	
	public BigDecimal getAmount()
	{
		BigDecimal amt = qty.multiply(priceActual);
		if (this.percent.signum() != 0)
		{
			amt = amt.multiply(this.percent);
			amt = amt.divide(Env.ONEHUNDRED, this.scale, RoundingMode.HALF_UP);
		}
		if (amt.scale() > this.scale)
		{
			amt = amt.setScale(this.scale, RoundingMode.HALF_UP);
		}
		return amt;
	}
	
	public BigDecimal getQty()
	{
		return this.qty;
	}

	public int getScale()
	{
		return scale;
	}

	public void setScale(int scale)
	{
		this.scale = scale;
	}

	public BigDecimal getPercent()
	{
		return percent;
	}

	public void setPercent(BigDecimal percent)
	{
		this.percent = percent;
	}
	
	public String toString()
	{
		return "qty=" + qty + ", price=" + priceActual + ", percentage="+percent;
	}
	
	public CostComponent reverseQty()
	{
		CostComponent cc = new CostComponent(qty.negate(), priceActual);
		cc.setPercent(getPercent());
		cc.setScale(getScale());
		return cc;
	}

}

